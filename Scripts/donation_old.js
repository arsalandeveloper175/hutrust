﻿//import { Alert } from "./bootstrap.bundle";

$(function () {
    $('#donationAmount').on('change', function (e) {

        var value = $(this).val();

        if (value) {
            if (value == -1) {
                $('#otherAmount').removeClass('hide');

                $('#otherAmount').addClass('txt-other-case');
                $('#donationAmount').addClass('ddl-other-case');

            } else {
                $('#otherAmount').addClass('hide');

                $('#otherAmount').removeClass('txt-other-case');
                $('#donationAmount').removeClass('ddl-other-case');
            }
        }
    });
     
});
function showLoader() {
    try {
        $("#mask").show();
        $("#loader").show();
    } catch (e) {
    }
}
function hideLoader() {
    try {
        $("#mask").hide();
        $("#loader").hide();
    } catch (e) {
    }
}
function resetFormFields(id) {
    try {
        if (id)
            $('#' + id).trigger('reset');

        return false;
    }
    catch (e) { }
}
// bound input control to only accept digits
jQuery('.numbersOnly').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});

function validateEmail(email) {
    //var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email)) {
        return false;
    } else {
        return true;
    }
}
function PrintElem(elem) {
    Popup($(elem).html());
}
function Popup(data) {
    var mywindow = window.open('', 'divPrint', 'height=400,width=600');
    mywindow.document.write('<html><head><title>Admit Card</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
}
function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}
function htmlDecode(value) {
    return $('<div/>').html(value).text();
}
function addCommas(amount) {
	if(amount){
	amount = amount.toString();
    var result = "";
    amount = amount.split("").reverse().join("");
    for (var i = 0; i < amount.length; i++) {
        result = result + amount[i];
        if (i === 2)
            result = result +',';
        if (i === 5)
            result = result + ',';
        if (i === 8)
            result = result + ',';
		if (i === 11)
            result = result + ',';
		if (i === 14)
            result = result + ',';

       // console.log(result);
    }
	 result = result.split("").reverse().join("");
	 if(result.charAt(0) === ',')
			result = result.slice(1);

    return result;
	}
}
function onTxtChange(e) {
    if (e) {
        var value = e.value;
        if (e.dataset) {
            if (e.dataset.parent) {
                var parentId = e.dataset.parent;
                if (parentId) {
                    if (value) {
                        $('#' + parentId).attr('data-other-amount', value);
                        $('#' + parentId).attr('data-amount', value);
                        console.log(value);
                    } else {
                        e.value = 0;
                        $('#' + parentId).attr('data-amount', 0);
                        $('#' + parentId).attr('data-other-amount', 0);
                    }
                    if (value > 99999999) {
                        alert('Amount should be between 0 - 99,999,999');
                        e.value = 0;
                        $('#' + parentId).attr('data-amount', 0);
                    }
                    //extra check 
                    if (value <= 0) {
                        try {
                            if (e.dataset.error) {
                                alert('Please enter ' + e.dataset.error.trim());
                            } else {
                                alert('Please enter amount');
                            }
                        } catch (ex) { console.debug(ex); }
                        return false;
                    }
                }
            }
        }
    }
}
function onSelectChange(e) {
    if (e) {
        var value = e.value;
        if (e.dataset) {
            if (e.dataset.parent) {
                var parentId = e.dataset.parent;
                if (parentId) {
                    if (value) {
                        $('#' + parentId).attr('data-other-amount', value);
                        $('#' + parentId).attr('data-amount', value);
                        console.log(value);
                    } else {
                        e.value = 0;
                        $('#' + parentId).attr('data-amount', 0);
                        $('#' + parentId).attr('data-other-amount', 0);
                    }
                    //extra check 
                    if (value == 0) {
                        try {
                            $($(e).parent()).find('#txt_' + e.name).remove();
                            alert('Please select ' + $(e).attr('data-error').trim());
                        } catch (ex) { console.debug(ex); }
                        return false;
                    }
                    if (value == -1) {
                        try {
                            $(e).parent().append('<input type="number" name="txt_' + e.name + '" id="txt_' + e.name + '" class="numbersOnly" onchange="onTxtChange(this)" max="999999999999999" min="1" data-parent="' + $(e).attr('data-parent') + '" placeholder="Type an amount" data-error="' + $(e).attr('data-error') + '" class="txt-other">');
                            //add a text box here
                            // alert('Please select ' + $(e).attr('data-error'));
                        } catch (ex) { console.debug(ex); }
                        return false;
                    } else {
                        $($(e).parent()).find('#txt_' + e.name).remove();
                    }
                }
            }
        }
    }
    return false;
}
$(function () {
    try {
        var spn_amounts = $('.spn-amount');
        if (spn_amounts) {
            $.each(spn_amounts, function (index, data) {
                if (data) { //console.log(index);
                    $(data).text(addCommas($(data).text().substring(0, $(data).text().indexOf('.'))));
                }
            });
        }

        $('.chkbx-control').on("change", function (e) {
            if (e) {
                if (e.target) {
                    if (e.target.id) {
                        if (e.target.id.search(/other/i) > -1) {
                            var spnAmount = $('#spnOther_' + e.target.id);
                            if ($('#' + e.target.id).is(':checked')) {
                                console.log('it is other type of check box' + e.target.id);
                                if (spnAmount) {

                                    $(spnAmount).html('<select data-error="' + $(e.target).parent().text() + '" name="otherAmount_' + e.target.name + '" id="otherAmount_' + e.target.name + '" onchange="onSelectChange(this)" data-parent="' + e.target.id + '" placeholder="select an amount"><option value="0">Please select</option><option value="2500">2,500</option><option value="5000">5,000</option><option value="7500">7,500</option><option value="10000">10,000</option><option value="15000">15,000</option><option value="20000">20,000</option><option value="25000">25,000</option><option value="-1">Other</option></select>');
                                    //$(spnAmount).html('<input type="number" name="otherAmount_' + e.target.name + '" id="otherAmount_' + e.target.name + '" onchange="onTxtChange(this)" max="999999999999999" min="1" data-parent="' + e.target.id + '" placeholder="Type an amount">');
                                }
                            } else {
                                $(spnAmount).html('');
                                $('#' + e.target.id).attr('data-other-amount', 0);
                                $('#' + e.target.id).attr('data-amount', 0);
                            }
                        }

                        if ($(e.target).is(':checked')) {
                            if ($(e.target).closest('div.form-group')) {
                                $(e.target).closest('div.form-group').addClass('selected_row');
                            }
                        }
                        else {
                            if ($(e.target).closest('div.form-group')) {
                                $(e.target).closest('div.form-group').removeClass('selected_row');
                            }
                        }
                        /*
                        else {// special case for public lecture series
                            if (e.target.id.search(/Public_Lecture_Series__Fund_contribution/i) > -1) {
                                var spnAmount = $('#spnOther_' + e.target.id);
                                if ($('#' + e.target.id).is(':checked')) {
                                    console.log('it is other type of check box' + e.target.id);
                                    if (spnAmount) {

                                        $(spnAmount).html('<select data-error="' + $(e.target).parent().text() + '" name="otherAmount_' + e.target.name + '" id="otherAmount_' + e.target.name + '" onchange="onSelectChange(this)" data-parent="' + e.target.id + '" placeholder="select an amount"><option value="0">Please select</option><option value="10000">10,000</option><option value="15000">15,000</option><option value="20000">20,000</option><option value="25000">25,000</option><option value="-1">Other</option><select>');
                                        //$(spnAmount).html('<input type="number" name="otherAmount_' + e.target.name + '" id="otherAmount_' + e.target.name + '" onchange="onTxtChange(this)" max="999999999999999" min="1" data-parent="' + e.target.id + '" placeholder="Type an amount">');
                                    }
                                } else {
                                    $(spnAmount).html('');
                                    $('#' + e.target.id).attr('data-other-amount', 0);
                                    $('#' + e.target.id).attr('data-amount', 0);
                                }
                            }
                        }    */
                    }
                }
            }
            e.preventDefault();
        });
        $('#btn-next').on("click", function (e) {
            var flag = true;
            var totalPledged = 0;
            var selectedFunds = $('input[type="checkbox"]:checked');
            if (selectedFunds) {
                $.each(selectedFunds, function (index, data) {
                    if (data) {
                        if (data) {
                            if (data.dataset) {
                                if (data.dataset.amount) {
                                    var value = parseInt(data.dataset.amount);

                                    if (value == 0) {
                                        try {
                                            alert('Please select amount in ' + $(data).parent().text().trim());
                                            document.getElementById(data.id).scrollIntoView(true);
                                        } catch (ex) { console.debug(ex); }
                                        flag = false;
                                        return false;
                                    }
                                    else if (value == -1) {
                                        try {
                                            try {
                                                alert('Please enter numeric value in ' + $(data).parent().text().trim());
                                                document.getElementById(data.id).scrollIntoView(true);
                                            } catch (ex) { console.debug(ex); }
                                            flag = false;
                                            return false;
                                        }
                                        catch (ex) { console.debug(ex); }
                                    }
                                    //else if (value > 50000000) {
                                    //    try {
                                    //        alert('Amount to pledge should be equal to or less than USD 50,000,000 in ' + $(data).parent().text().trim());
                                    //    } catch (ex) { console.debug(ex); }
                                    //    flag = false;
                                    //    return false;
                                    //}

                                    totalPledged += value;
                                }
                            }
                        }
                    }
                });
            }
            if (totalPledged > 0 && flag) {
                var sumContnr = $(".summary_container");
                var accordn = $("#accordion");
                if (sumContnr) {
                    $(sumContnr).show();
                    if (accordn) {
                        $(accordion).hide();
                    }
                    $("#spn-summary").html(addCommas(totalPledged));
                }
                $(body).append('<a id="a_nav" href="' + window.document.location.origin + '/donate#pledgeForm_locator"></a>');
                $('#a_nav').click();
            } else {
                if (flag) {
                    alert('Please select an option to donate.');
                }
            }
            e.preventDefault();
        });
        $('#goBack').on("click", function (e) {
            var sumContnr = $(".summary_container");
            var accordn = $("#accordion");
            if (sumContnr) {
                $(sumContnr).hide();
                if (accordn) {
                    $(accordion).show();
                }
                $("#spn-summary").html(0);
            }
            e.preventDefault();
        });
        $('#rdConfirm').on("change", function (e) {
            if ($('#rdConfirm').is(':checked')) {
                var infoCntnr = $('.pledgeForm');
                var sumContnr = $(".summary_container");
                if (infoCntnr) {
                    $(infoCntnr).show();
                    if (sumContnr) {
                        $(sumContnr).hide();
                    }
                }
            }
            $('.scroll_btn').show();
            e.preventDefault();
        });
        $('input[name=recognition]').on("change", function (e) {
            var recogn = $('input[name=recognition]:checked').val();
            if (recogn == 2) {
                $('#photo_btn').hide();
            } else {
                $('#photo_btn').show();
            }
            return false;
        });
        $('#btnPledge').on("click", function (e) {
            var flag = true;
            var recognition = 0;
            var address = "";
            var totalAmount = 0;

            $('#fullNameError').addClass('text-hide');
            $('#fullNameError').html('');
            $('#recognitionError').addClass('text-hide');
            $('#recognitionError').html('');

            var name = $('#name').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var confirm = $('#rdConfirm').is(':checked');
            recognition = $('input[name=recognition]:checked').val();
            address = $('#address').val();
            totalAmount = $("#spn-summary").text();
            totalAmount = totalAmount.replace(/,/g, '');

            if (name) {
                if (name.trim().length == 0) {
                    $('#fullNameError').html('Please enter your name');
                    $('#fullNameError').removeClass('text-hide');
                    document.getElementById('name').scrollIntoView(true);
                    flag = false;
                } else if (name.trim().length > 120) {
                    $('#fullNameError').html('Name shuold be less than 120 characters');
                    $('#fullNameError').removeClass('text-hide');
                    document.getElementById('name').scrollIntoView(true);
                    flag = false;
                }
            } else {
                $('#fullNameError').html('Please enter your name');
                $('#fullNameError').removeClass('text-hide');
                document.getElementById('name').scrollIntoView(true);
                flag = false;
            }
            if (recognition) {
                recognition = parseInt(recognition);
                if (recognition == 0) {
                    $('#recognitionError').html('Please select the option');
                    $('#recognitionError').removeClass('text-hide');
                    document.getElementById('recognizeByName').scrollIntoView(true);
                    flag = false;
                } else if (recognition == 1) { recognition = true; } else if (recognition == 2) { recognition = false; }
            } else {
                $('#recognitionError').html('Please select the option');
                $('#recognitionError').removeClass('text-hide');
                document.getElementById('recognizeByName').scrollIntoView(true);
                flag = false;
            }
            if (phone) {
                if (phone.trim().length == 0) {
                    $('#contactNoError').html('Please enter contact no');
                    $('#contactNoError').removeClass('text-hide');
                    document.getElementById('phone').scrollIntoView(true);
                    flag = false;
                } else if (phone.trim().length > 50) {
                    $('#contactNoError').html('Contact no length should be less than or equal to 50 numbers');
                    $('#contactNoError').removeClass('text-hide');
                    document.getElementById('phone').scrollIntoView(true);
                    flag = false;
                } else {
                    $('#contactNoError').html('');
                    $('#contactNoError').addClass('text-hide');
                }
            } else {
                $('#contactNoError').html('Please enter contact no.');
                $('#contactNoError').removeClass('text-hide');
                document.getElementById('phone').scrollIntoView(true);
                flag = false;
            }
            if (email) {
                if (email.trim().length > 0) {
                    if (validateEmail(email)) {
                        $('#emailError').html('');
                        $('#emailError').addClass('text-hide');
                        if (email.trim().length > 180) {
                            $('#emailError').html('Email length should be less than or equal to 180 characters');
                            $('#emailError').removeClass('text-hide');
                            document.getElementById('email').scrollIntoView(true);
                            flag = false;
                        }
                    } else {
                        $('#emailError').html('Please enter a valid email');
                        $('#emailError').removeClass('text-hide');
                        document.getElementById('email').scrollIntoView(true);
                        flag = false;
                    }
                } else {
                    $('#emailError').html('Please enter an email');
                    $('#emailError').removeClass('text-hide');
                    document.getElementById('email').scrollIntoView(true);
                    flag = false;
                }
            } else {
                $('#emailError').html('Please enter an email');
                $('#emailError').removeClass('text-hide');
                document.getElementById('email').scrollIntoView(true);
                flag = false;
            }

            var fundData = [];

            // file case
            var photo = $('#PhotoId').val();
            var totalPledgedAmount = $("#spn-summary").html();
            totalPledgedAmount = totalPledgedAmount.replace(/,/g, '');
            //if (!photo) {
            //    $("#progressCounter_photograph").html("Please take a selfy");
            //    flag = false;
            //}
        //
            var selectedFunds = $('input[type="checkbox"]:checked');
            if (selectedFunds) {
                $.each(selectedFunds, function (index, data) {
                    if (data) {
                        if (data) {
                            fundData.push({ 'Id': parseInt(data.name), 'OtherAmount': parseInt(data.dataset.otherAmount) });
                        }
                    }
                });
                var pledge = { 'Funds': fundData, 'FullName': capitalize(name), 'PhoneNo': phone, 'Email': email, 'Confirm': confirm, 'Recognition': recognition, 'Address': address, 'PhotoId': photo, 'TotalPledgeAmount': totalPledgedAmount };
                console.log(pledge);
                if (fundData.length <= 0) {
                    alert('Please select an option to donate.');
                    flag = false;
                }
                if (flag) {
                    showLoader();
                    $.post('/Donate/Pledge', pledge, function (resp) {
                        console.log(resp);
                        if (resp) {
                            if (resp.Success) {
                                if (resp.PledgerId) {
                                    if (resp.PledgerId > 0) {
                                        var link = $('#Pay').val();
                                    }
                                    if (link) {
                                        var url = (document.location.origin + link);
                                        redirect(url, totalAmount, resp.PledgerId, resp.Name, resp.HashKey, resp.TimeStamp, resp.Email);
                                        return false;
                                    }
                                    else {
                                        alert('Faild to save record, please try again.');
                                        document.location.href = document.location.href;
                                    }
                                }
                            }
                            else {
                                alert(resp.Msg);
                                return false;
                            }
                        }
                    });
                }
                return false;
            }
            e.preventDefault();
        });
        $('.btn-link').on("click", function (e) {
            $($(this).data('link-nas')).toggle();
            if ($(this).hasClass('expanded')) {
                $(this).removeClass('expanded').addClass('collapsed');
            } else if($(this).hasClass('collapsed')){
                $(this).removeClass('collapsed').addClass('expanded');
            }
        });
    } catch (e) {
        if (console) {
            console.log('try catch error: ');
            console.log(e);
        }
    }
});
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function capitalize(text) {
    return text.replace(/\b\w/g, function (m) { return m.toUpperCase(); });
}
$(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height()+150 >= $(document).height()) {
        $('.scroll_btn').hide();
    } else {
        $('.scroll_btn').show();
    }
});

function redirect(url, amount, id, name, hashKey, timeStamp) {
    var successURL = document.location.href + "\SuccessfulPayment";
    var failURL = document.location.href + "\FailedPayment";
    //url = "https://demo.globalgatewaye4.firstdata.com/payment";//"https://test.ipg-online.com/connect/gateway/processing";//"https://demo.globalgatewaye4.firstdata.com";//
    url = "https://checkout.globalgatewaye4.firstdata.com/payment";
    var form = $(document.createElement('form'));
    $(form).attr("action", url);
    $(form).attr("method", "POST");

    var input = $("<input>").attr("type", "hidden").attr("name", "x_login").val('WSP-HUFUS-G&8TKgC7gg');
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_fp_timestamp").val(timeStamp);
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_fp_sequence").val(id);
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_fp_hash").val(hashKey);
    $(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "storename").val("10123456789");
    //$(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "mode").val("fullpay");
    //$(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_amount").val(amount);
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "currency").val("");
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_show_form").val("PAYMENT_FORM");
    $(form).append($(input));
    //var input = $("<input>").attr("type", "hidden").attr("name", "x_fee_amount ").val("");
    //$(form).append($(input));

    //optional params
    //var input = $("<input>").attr("type", "hidden").attr("name", "customerid").val(id);
    //$(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "comments").val("Doner Name: " + name);
    //$(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "responseSuccessURL").val(successURL);
    //$(form).append($(input));
    //var input = $("<input>").attr("type", "hidden").attr("name", "responseFailURL").val(failURL);
    //$(form).append($(input));

    //var input1 = $("<input>").attr("type", "hidden").attr("pledgerId", "pledgerId").val(id);
    //$(form).append($(input1));
    //var input2 = $("<input>").attr("type", "hidden").attr("name", "name").val(name);
    //$(form).append($(input2));

    form.appendTo(document.body)
    $(form).submit();
}