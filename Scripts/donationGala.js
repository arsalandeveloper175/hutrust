﻿//import { Alert } from "./bootstrap.bundle";
$(function () {
    $('#donationAmount').on('change', function (e) {

        var value = $(this).val();

        if (value) {
            if (value == -1) {
                $('#otherAmount').removeClass('hide');

                $('#otherAmount').addClass('txt-other-case');
                $('#donationAmount').addClass('ddl-other-case');

            } else {
                $('#otherAmount').addClass('hide');

                $('#otherAmount').removeClass('txt-other-case');
                $('#donationAmount').removeClass('ddl-other-case');
            }
        }
    });
     
});
_popUpMsg = "";
function redirect(url) {
    var url = (document.location.origin + $('#ThankYouLink').val()+'?mode=-1');
    document.location.href = url;
}
function showLoader() {
    try {
        $("#mask").show();
        $("#loader").show();
    } catch (e) {
    }
}
function hideLoader() {
    try {
        $("#mask").hide();
        $("#loader").hide();
    } catch (e) {
    }
}
function resetFormFields(id) {
    try {
        if (id)
            $('#' + id).trigger('reset');

        return false;
    }
    catch (e) { }
}
// bound input control to only accept digits
jQuery('.numbersOnly').keyup(function () {
    this.value = this.value.replace(/[^0-9\.]/g, '');
});

function validateEmail(email) {
    //var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email)) {
        return false;
    } else {
        return true;
    }
}
function PrintElem(elem) {
    Popup($(elem).html());
}
function Popup(data) {
    var mywindow = window.open('', 'divPrint', 'height=400,width=600');
    mywindow.document.write('<html><head><title>Admit Card</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

  //  mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

  //  mywindow.print();
  //  mywindow.close();

    return true;
}
function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}
function htmlDecode(value) {
    return $('<div/>').html(value).text();
}
function addCommas(amount) {
	if(amount){
	amount = amount.toString();
    var result = "";
    amount = amount.split("").reverse().join("");
    for (var i = 0; i < amount.length; i++) {
        result = result + amount[i];
        if (i === 2)
            result = result +',';
        if (i === 5)
            result = result + ',';
        if (i === 8)
            result = result + ',';
		if (i === 11)
            result = result + ',';
		if (i === 14)
            result = result + ',';

       // console.log(result);
    }
	 result = result.split("").reverse().join("");
	 if(result.charAt(0) === ',')
			result = result.slice(1);

    return result;
	}
}
function onTxtChange(e) {
    if (e) {
        var value = e.value;
        if (e.dataset) {
            if (e.dataset.parent) {
                var parentId = e.dataset.parent;
                if (parentId) {
                    if (value) {
                        $('#' + parentId).attr('data-other-amount', value);
                        $('#' + parentId).attr('data-amount', value);
                        console.log(value);
                    } else {
                        e.value = 0;
                        $('#' + parentId).attr('data-amount', 0);
                        $('#' + parentId).attr('data-other-amount', 0);
                    }
                    if (value > 999999) {
                        alert('Amount should be between 0 - 999,999');
                        e.value = 0;
                        $('#' + parentId).attr('data-amount', 0);
                    }
                    //extra check 
                    if (value <= 0) {
                        try {
                            if (e.dataset.error) {
                                alert('Please enter ' + e.dataset.error.trim());
                            } else {
                                alert('Please enter amount');
                            }
                        } catch (ex) { console.debug(ex); }
                        return false;
                    }
                }
            }
        }
    }
}
function onSelectChange(e) {
    if (e) {
        var value = e.value;
        if (e.dataset) {
            if (e.dataset.parent) {
                var parentId = e.dataset.parent;
                if (parentId) {
                    if (value) {
                        $('#' + parentId).attr('data-other-amount', value);
                        $('#' + parentId).attr('data-amount', value);
                        console.log(value);
                    } else {
                        e.value = 0;
                        $('#' + parentId).attr('data-amount', 0);
                        $('#' + parentId).attr('data-other-amount', 0);
                    }
                    //extra check 
                    if (value == 0) {
                        try {
                            $($(e).parent()).find('#txt_' + e.name).remove();
                            alert('Please select ' + $(e).attr('data-error').trim());
                        } catch (ex) { console.debug(ex); }
                        return false;
                    }
                    if (value == -1) {
                        try {
                            $(e).parent().append('<input type="number" name="txt_' + e.name + '" id="txt_' + e.name + '" class="numbersOnly" onchange="onTxtChange(this)" max="999999999999999" min="1" data-parent="' + $(e).attr('data-parent') + '" placeholder="Type an amount" data-error="' + $(e).attr('data-error') + '" class="txt-other">');
                            //add a text box here
                            // alert('Please select ' + $(e).attr('data-error'));
                        } catch (ex) { console.debug(ex); }
                        return false;
                    } else {
                        $($(e).parent()).find('#txt_' + e.name).remove();
                    }
                }
            }
        }
    }
    return false;
}
$(function () {
    try {
        var spn_amounts = $('.spn-amount');
        if (spn_amounts) {
            $.each(spn_amounts, function (index, data) {
                if (data) { //console.log(index);
                    $(data).text(addCommas($(data).text().substring(0, $(data).text().indexOf('.'))));
                }
            });
        }

        //donation and pledge chks
        $('#donate').on("click", function (e) {
            $('.mode-btns').removeClass('load-case');
            $('#donate').addClass('btn-selected');
            $('#pledge').removeClass('btn-selected');
            $('.pledge-case').hide();
            $('.donate-case').show();
            $('.default-case').show();
            $('#TypeId').val(1);

            $('#photo_btn').hide();
            $('#recognizeByAnonymous').removeClass('btn-selected');
            $('#recognizeByName').removeClass('btn-selected');
            $('#Recognition').val(false);

            $('.div-confirm').show();
            $("#amountError").hide();
        });
        $('#pledge').on("click", function (e) {
            $('.mode-btns').removeClass('load-case');
            $('#donate').removeClass('btn-selected');
            $('#pledge').addClass('btn-selected');
            $('.pledge-case').show();
            $('.donate-case').hide();
            $('.default-case').show();
            $('#TypeId').val(0);

            $('#photo_btn').hide();
            $('#recognizeByAnonymous').removeClass('btn-selected');
            $('#recognizeByName').removeClass('btn-selected');
            $('#Recognition').val(false);

            // reset siblings
            $('#OneTimeDonation').removeClass('btn-selected');
            $('#Recurring').removeClass('btn-selected');
            $('#rdMonthly').removeClass('btn-selected');
            $('#rdQuaterly').removeClass('btn-selected');
            $('#rdAnnually').removeClass('btn-selected');
            $('.div-confirm').show();
            $("#amountError").hide();
        });
        $('#OneTimeDonation').on("click", function (e) {
            $('#OneTimeDonation').addClass('btn-selected');
            $('#Recurring').removeClass('btn-selected');
            $('#RecurringDonation_btn').hide();
            $('#IsRecurringPayment').val(0);

            // reset siblings
            $('#rdMonthly').removeClass('btn-selected');
            $('#rdQuaterly').removeClass('btn-selected');
            $('#rdSemiAnnually').removeClass('btn-selected');
            $('#rdAnnually').removeClass('btn-selected');
            $('#FrequencyLimit').val(0);
            $('#startDate').val('');
            $('#endDate').val('');
        });
        $('#Recurring').on("click", function (e) {
            $('#OneTimeDonation').removeClass('btn-selected');
            $('#Recurring').addClass('btn-selected');
            $('#RecurringDonation_btn').show();
            $('#IsRecurringPayment').val(1);
        });
        $('#rdMonthly').on("click", function (e) {
            $('#rdQuaterly').removeClass('btn-selected');
            $('#rdSemiAnnually').removeClass('btn-selected');
            $('#rdAnnually').removeClass('btn-selected');
            $('#rdMonthly').addClass('btn-selected');
            $('#Frequency').val('Monthly');
            $('#FrequencyLimit').val(1);
        });
        $('#rdQuaterly').on("click", function (e) {
            $('#rdMonthly').removeClass('btn-selected');
            $('#rdSemiAnnually').removeClass('btn-selected');
            $('#rdAnnually').removeClass('btn-selected');
            $('#rdQuaterly').addClass('btn-selected');
            $('#Frequency').val('Quarterly');
            $('#FrequencyLimit').val(4);
        });
        $('#rdSemiAnnually').on("click", function (e) {
            $('#rdQuaterly').removeClass('btn-selected');
            $('#rdMonthly').removeClass('btn-selected');
            $('#rdAnnually').removeClass('btn-selected');
            $('#rdSemiAnnually').addClass('btn-selected');
            $('#Frequency').val('semi-annually');
            $('#FrequencyLimit').val(6);
        });
        $('#rdAnnually').on("click", function (e) {
            $('#rdQuaterly').removeClass('btn-selected');
            $('#rdMonthly').removeClass('btn-selected');
            $('#rdSemiAnnually').removeClass('btn-selected');
            $('#rdAnnually').addClass('btn-selected');
            $('#Frequency').val('Annually');
            $('#FrequencyLimit').val(12);
        });
        $('#recognizeByName').on("click", function (e) {
           $('#photo_btn').show();
            $('#recognizeByName').addClass('btn-selected');
            $('#recognizeByAnonymous').removeClass('btn-selected');
            $('#Recognition').val(true);
        });
        $('#recognizeByAnonymous').on("click", function (e) {
           $('#photo_btn').hide();
            $('#recognizeByAnonymous').addClass('btn-selected');
            $('#recognizeByName').removeClass('btn-selected');
            $('#Recognition').val(false);
        });
        $('#endDate').on("change", function (e) { searchOnDate();  });
        $('#startDate').on("change", function (e) { searchOnDate(); });

        $('.chkbx-control').on("change", function (e) {
            if (e) {
                if (e.target) {
                    if (e.target.id) {
                        if (e.target.id.search(/other/i) > -1) {
                            var spnAmount = $('#spnOther_' + e.target.id);
                            if ($('#' + e.target.id).is(':checked')) {
                                console.log('it is other type of check box' + e.target.id);
                                if (spnAmount) {

                                    $(spnAmount).html('<select data-error="' + $(e.target).parent().text() + '" name="otherAmount_' + e.target.name + '" id="otherAmount_' + e.target.name + '" onchange="onSelectChange(this)" data-parent="' + e.target.id + '" placeholder="select an amount"><option value="0">Please select</option><option value="2500">2,500</option><option value="5000">5,000</option><option value="7500">7,500</option><option value="10000">10,000</option><option value="15000">15,000</option><option value="20000">20,000</option><option value="25000">25,000</option><option value="-1">Other</option></select>');
                                    //$(spnAmount).html('<input type="number" name="otherAmount_' + e.target.name + '" id="otherAmount_' + e.target.name + '" onchange="onTxtChange(this)" max="999999999999999" min="1" data-parent="' + e.target.id + '" placeholder="Type an amount">');
                                }
                            } else {
                                $(spnAmount).html('');
                                $('#' + e.target.id).attr('data-other-amount', 0);
                                $('#' + e.target.id).attr('data-amount', 0);
                            }
                        }

                        if ($(e.target).is(':checked'))
                            $(e.target).closest('div.form-group').addClass('selected_row');
                        else
                            $(e.target).closest('div.form-group').removeClass('selected_row');
                        /*
                        else {// special case for public lecture series
                            if (e.target.id.search(/Public_Lecture_Series__Fund_contribution/i) > -1) {
                                var spnAmount = $('#spnOther_' + e.target.id);
                                if ($('#' + e.target.id).is(':checked')) {
                                    console.log('it is other type of check box' + e.target.id);
                                    if (spnAmount) {

                                        $(spnAmount).html('<select data-error="' + $(e.target).parent().text() + '" name="otherAmount_' + e.target.name + '" id="otherAmount_' + e.target.name + '" onchange="onSelectChange(this)" data-parent="' + e.target.id + '" placeholder="select an amount"><option value="0">Please select</option><option value="10000">10,000</option><option value="15000">15,000</option><option value="20000">20,000</option><option value="25000">25,000</option><option value="-1">Other</option><select>');
                                        //$(spnAmount).html('<input type="number" name="otherAmount_' + e.target.name + '" id="otherAmount_' + e.target.name + '" onchange="onTxtChange(this)" max="999999999999999" min="1" data-parent="' + e.target.id + '" placeholder="Type an amount">');
                                    }
                                } else {
                                    $(spnAmount).html('');
                                    $('#' + e.target.id).attr('data-other-amount', 0);
                                    $('#' + e.target.id).attr('data-amount', 0);
                                }
                            }
                        }    */
                    }
                }
            }
            e.preventDefault();
        });
        $('#btn-next').on("click", function (e) {
            _popUpMsg = "";
            var totalPledged = 0;
            totalPledged = getTotalAmount();
            if (totalPledged > 0) {
                var sumContnr = $(".summary_container");
               // var pledgeForm = $('.pledgeForm');
                var accordn = $("#accordion");
                if (sumContnr) {
                    $(sumContnr).show();
                    $('.div-btns').show();
                    //if (pledgeForm) {
                    //    $(pledgeForm).show()[0].focus();
                    //}
                    if (accordn) {
                        $(accordion).hide();
                    }
                    $("#spn-summary").html(addCommas(totalPledged));

                    //if (totalPledged > 999999) {
                    //    $(".amount-confirm-gourp").hide();
                    //    $("#amountError").show();
                    //} else {
                    //    $(".amount-confirm-gourp").show();
                    //    $("#amountError").hide();
                    //}
                }
            } else {
                    if (_popUpMsg)
                        alert(_popUpMsg);
                    else
                        alert('Please select an option to donate.');
            }
            e.preventDefault();
        });
        $('#goBack').on("click", function (e) {
            var sumContnr = $(".summary_container");
            //var pledgeForm = $('.pledgeForm');
            var accordn = $("#accordion");
            if (sumContnr) {
                //if (pledgeForm) {
                //    $(pledgeForm).hide();
                //}
                $(sumContnr).hide();
                if (accordn) {
                    $(accordion).show();
                }
                $("#spn-summary").html(0);
            }
            e.preventDefault();
        });
        $('#rdConfirm').on("click", function (e) {
            if ($('#rdConfirm').is(':checked')) {
                var typeId = $('#TypeId').val();
                if (typeId == 1 || typeId == "1") {
                    var totalPledged = getTotalAmount();
                       if (totalPledged > 999999) {
                        $(".div-confirm").hide();
                           $("#amountError").show();
                           return;
                    } else {
                           $(".div-confirm").show();
                        $("#amountError").hide();
                    }
                }
                var infoCntnr = $('.pledgeForm');
                var sumContnr = $(".summary_container");
                if (infoCntnr) {
                    $(infoCntnr).show();
                    if (sumContnr) {
                        $(sumContnr).hide();
                    }
                }
            }
            $('.scroll_btn').show();
            e.preventDefault();
        });
        $('.saveData').on("click", function (e) {
            var flag = true;
            var recognition = '';
            var address = "";
            var typeId = $('#TypeId').val(); //e.target.dataset.typeid;
            var isRecuringDonation = $('#IsRecurringPayment').val();
            var frequency = $('#Frequency').val();
            var startDate = null;
            var endDate = null;

            $('#fullNameError').addClass('text-hide');
            $('#fullNameError').html('');
            $('#recognitionError').addClass('text-hide');
            $('#recognitionError').html('');

            var name = $('#name').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var confirm = $('#rdConfirm').is(':checked');
            recognition = $('#Recognition').val();
            address = $('#address').val();
            if (isRecuringDonation == "1" || isRecuringDonation == 1) {
                flag = searchOnDate();
                if (flag) {
                    startDate = $('#startDate').val();
                    endDate = $('#endDate').val();
                }
            }
            if (name) {
                if (name.trim().length == 0) {
                    $('#fullNameError').html('Please enter your name');
                    $('#fullNameError').removeClass('text-hide');
                    document.getElementById('name').scrollIntoView(true);
                    flag = false;
                } else if (name.trim().length > 30) {
                    $('#fullNameError').html('Name shuold be less than 31 characters');
                    $('#fullNameError').removeClass('text-hide');
                    document.getElementById('name').scrollIntoView(true);
                    flag = false;
                }
            } else {
                $('#fullNameError').html('Please enter your name');
                $('#fullNameError').removeClass('text-hide');
                document.getElementById('name').scrollIntoView(true);
                flag = false;
            }
            if (address) {
                if (address.trim().length == 0) {
                    $('#addressError').html('Please enter address');
                    $('#addressError').removeClass('text-hide');
                    document.getElementById('address').scrollIntoView(true);
                    flag = false;
                } else if (address.trim().length > 30) {
                    $('#addressError').html('Address shuold be less than 31 characters');
                    $('#addressError').removeClass('text-hide');
                    document.getElementById('address').scrollIntoView(true);
                    flag = false;
                } else {
                // hide error msg
                    $('#addressError').html('').removeClass('text-hide');
                }
            } else {
                $('#addressError').html('Please enter address');
                $('#addressError').removeClass('text-hide');
                document.getElementById('address').scrollIntoView(true);
                flag = false;
            }

            if (recognition) {
                $('#recognitionError').html('').addClass('text-hide');
                //recognition = parseInt(recognition);
                //if (recognition == 0) {
                //    $('#recognitionError').html('Please select the option');
                //    $('#recognitionError').removeClass('text-hide');
                //    document.getElementById('recognizeByName').scrollIntoView(true);
                //    flag = false;
                //} else if (recognition == 1) { recognition = true; } else if (recognition == 2) { recognition = false; }
            } else {
                $('#recognitionError').html('Please select the option');
                $('#recognitionError').removeClass('text-hide');
                document.getElementById('recognizeByName').scrollIntoView(true);
                flag = false;
            }
              /*
            if (isRecuringDonation) {
                isRecuringDonation = parseInt(isRecuringDonation);
                if (isRecuringDonation == -1) {
                    $('#isRecuringDonationError').html('Please select the option');
                    $('#isRecuringDonationError').removeClass('text-hide');
                    document.getElementById('OneTimeDonation').scrollIntoView(true);
                    flag = false;
                } else if (isRecuringDonation == 1) {
                    $('#isRecuringDonationError').html('').addClass('text-hide');
                    isRecuringDonation = true;
                    // get frequency in case of recuring
                    if (frequency) {
                        if (frequency.trim().length == 0) {
                            $('#frequencyError').html('Please select frequency');
                            $('#frequencyError').removeClass('text-hide');
                            document.getElementById('rdMonthly').scrollIntoView(true);
                            flag = false;
                        } else {
                            $('#frequencyError').html('').removeClass('text-hide');
                        }
                    } else {
                        $('#frequencyError').html('Please select frequency');
                        $('#frequencyError').removeClass('text-hide');
                        document.getElementById('rdMonthly').scrollIntoView(true);
                        flag = false;
                    }
                    // end of requency
                } else if (isRecuringDonation == 0) {
                    $('#isRecuringDonationError').html('').addClass('text-hide');
                    isRecuringDonation = false;
                }
            } else {
                $('#isRecuringDonationError').html('Please select the option');
                $('#isRecuringDonationError').removeClass('text-hide');
                document.getElementById('isRecuringDonation').scrollIntoView(true);
                flag = false;
            }
            */
            if (phone) {
                if (phone.trim().length == 0) {
                    $('#contactNoError').html('Please enter contact no');
                    $('#contactNoError').removeClass('text-hide');
                    document.getElementById('phone').scrollIntoView(true);
                    flag = false;
                } else if (phone.trim().length > 14) {
                    $('#contactNoError').html('Contact no length should be less than or equal to 14 digits only.');
                    $('#contactNoError').removeClass('text-hide');
                    document.getElementById('phone').scrollIntoView(true);
                    flag = false;
                } else {
                    $('#contactNoError').html('');
                    $('#contactNoError').addClass('text-hide');
                }
            } else {
                $('#contactNoError').html('Please enter contact no.');
                $('#contactNoError').removeClass('text-hide');
                document.getElementById('phone').scrollIntoView(true);
                flag = false;
            }
            if (email) {
                if (email.trim().length > 0) {
                    if (validateEmail(email)) {
                        $('#emailError').html('');
                        $('#emailError').addClass('text-hide');
                        if (email.trim().length > 64) {
                            $('#emailError').html('Email length should be less than or equal to 64 characters');
                            $('#emailError').removeClass('text-hide');
                            document.getElementById('email').scrollIntoView(true);
                            flag = false;
                        }
                    } else {
                        $('#emailError').html('Please enter a valid email');
                        $('#emailError').removeClass('text-hide');
                        document.getElementById('email').scrollIntoView(true);
                        flag = false;
                    }
                } else {
                    $('#emailError').html('Please enter an email');
                    $('#emailError').removeClass('text-hide');
                    document.getElementById('email').scrollIntoView(true);
                    flag = false;
                }
            } else {
                $('#emailError').html('Please enter an email');
                $('#emailError').removeClass('text-hide');
                document.getElementById('email').scrollIntoView(true);
                flag = false;
            }

            var fundData = [];

            // file case
            var photo = $('#PhotoId').val();
            var totalPledgedAmount = $("#spn-summary").html();
            totalPledgedAmount = totalPledgedAmount.replace(/,/g, '');
            //if (!photo) {
            //    $("#progressCounter_photograph").html("Please take a selfy");
            //    flag = false;
            //}
        //
            var selectedFunds = $('input[type="checkbox"]:checked');
            if (selectedFunds) {
                $.each(selectedFunds, function (index, data) {
                    if (data) {
                        if (data) {
                            fundData.push({ 'Id': parseInt(data.name), 'OtherAmount': parseInt(data.dataset.otherAmount) });
                        }
                    }
                });
                var pledge = {
                    'Funds': fundData, 'FullName': capitalize(name), 'PhoneNo': phone, 'Email': email, 'Confirm': confirm, 'Recognition': recognition, 'Address': address, 'PhotoId': photo, 'TotalPledgeAmount': totalPledgedAmount, 'TypeId': typeId, 'IsRecuring': isRecuringDonation, 'Frequency': frequency, 'TotalRecurringAmount': totalPledgedAmount, 'StartDate': startDate, 'EndDate': endDate
                };
                console.log(pledge);
                if (fundData.length <= 0) {
                    alert('Please select an option to donate.');
                    flag = false;
                }
                if (flag) {
                    showLoader();
                    $.post('/Campaign/Pledge', pledge, function (resp) {
                        console.log(resp);
                        if (resp) {
                            if (resp.Success) {
                                if (resp.TypeId == 0) {
                                    var link = $('#ThankYouLink');
                                    if (link) {
                                        var url = (document.location.origin + $('#ThankYouLink').val());
                                        redirect(url);
                                        return false;
                                    }
                                    else
                                        alert('Thank you for your contribution');
                                } else
                                {     // donor case
                                    if (resp.PledgerId) {
                                        if (resp.TypeId == 1) {
                                            if (resp.PledgerId > 0) {
                                                var link = $('#Pay').val();
                                            }
                                            if (link) {
                                                var url = (document.location.origin + link);
                                                redirectToGateway(url, totalPledgedAmount, resp.PledgerId, resp.Name, resp.HashKey, resp.TimeStamp, resp.X_login, resp.Email, address, phone, resp.IsRecuring, resp.Frequency, resp.StartDate, resp.EndDate)//new Date(parseInt(resp.StartDate.substr(6))), new Date(parseInt(resp.EndDate.substr(6))));
                                                return false;
                                            }
                                            else {
                                                alert('Faild to save record, please try again.');
                                                document.location.href = document.location.href;
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                hideLoader();
                                alert(resp.Msg);
                                return false;
                            }
                        }
                    });
                }
                return false;
            }
            e.preventDefault();
        });
        $('.btn-link').on("click", function (e) {
            $($(this).data('link-nas')).toggle();
            if ($(this).hasClass('expanded')) {
                $(this).removeClass('expanded').addClass('collapsed');
            } else if($(this).hasClass('collapsed')){
                $(this).removeClass('collapsed').addClass('expanded');
            }
        });
    } catch (e) {
        if (console) {
            console.log('try catch error: ');
            console.log(e);
        }
    }
});

function getTotalAmount() {       
    var totalPledged = 0;
    var selectedFunds = $('input[type="checkbox"]:checked');
    if (selectedFunds) {
        $.each(selectedFunds, function (index, data) {
            if (data) {
                if (data) {
                    if (data.dataset) {
                        if (data.dataset.amount) {
                            var value = parseInt(data.dataset.amount);

                            if (value == 0) {
                                try {
                                    //alert('Please select amount in ' + $(data).parent().text().trim());
                                    _popUpMsg = 'Please select amount in ' + $(data).parent().text().trim();
                                    document.getElementById(data.id).scrollIntoView(true);
                                } catch (ex) { console.debug(ex); }
                                flag = false;
                                return false;
                            }
                            else if (value == -1) {
                                try {
                                    try {
                                    //    alert('Please enter numeric value in ' + $(data).parent().text().trim());
                                        _popUpMsg = 'Please enter numeric value in ' + $(data).parent().text().trim();
                                        document.getElementById(data.id).scrollIntoView(true);
                                    } catch (ex) { console.debug(ex); }
                                    flag = false;
                                    return false;
                                }
                                catch (ex) { console.debug(ex); }
                            }
                            //else if (value > 50000000) {
                            //    try {
                            //        alert('Amount to pledge should be equal to or less than USD 50,000,000 in ' + $(data).parent().text().trim());
                            //    } catch (ex) { console.debug(ex); }
                            //    flag = false;
                            //    return false;
                            //}

                            totalPledged += value;
                        }
                    }
                }
            }
        });
    }
    return totalPledged;
}

function searchOnDate() {
    var frequencyLimit = $('#FrequencyLimit').val();
    var strStartDate = $('#startDate').val();
    var strEndDate = $('#endDate').val();

    if (frequencyLimit) {
        var intFreqLimit = parseInt(frequencyLimit);
        if (intFreqLimit) { 
        $('#endDateError').text('').addClass('text-hide');
        $('#startDateError').text('').addClass('text-hide');

            var currentDate = new Date('2019-08-17');
            var dateLimit = new Date(currentDate.getFullYear(), currentDate.getMonth() + intFreqLimit, currentDate.getDate()+1);
            currentDate = currentDate;//.setHours(0, 0, 0, 0);

        if (strStartDate) {

            // var dateStartLimit = new Date(startTime);
            // dateStartLimit.setHours(0, 0, 0, 0);
            // console.log(dateStartLimit);
            var _startDate = new Date(strStartDate);
            var _endDate = new Date(strEndDate);
            _startDate;//.setHours(0, 0, 0, 0);
            _endDate;//.setHours(0, 0, 0, 0);
            console.log(_startDate);

            if (currentDate > _startDate) {
                //alert('Date shouldn\'t be older than current Date.');
                $('#startDateError').text('Date shouldn\'t be older than current Date.').removeClass('text-hide');;
                return false;
            }
            if (_endDate) {
                if (_endDate == "Invalid Date") {
                    $('#endDateError').text('Please provdie End Date').removeClass('text-hide');
                    return false;
                }

                if (_endDate < currentDate) {
                    //alert('End Date shouldn\'t be greater than current start date.');
                    $('#endDateError').text('End Date shouldn\'t be older than current date.').removeClass('text-hide');
                    return false;
                }

                if (_endDate < _startDate) {
                    //alert('Start Date shouldn\'t be greater than End Date.');
                    $('#startDateError').text('Start Date shouldn\'t be greater than End Date.').removeClass('text-hide');;
                    return false;
                }
                if (_endDate < dateLimit) {
                    //alert('Start Date shouldn\'t be greater than End Date.');
                    $('#endDateError').text('Invalid End Date, please set according to frequency.').removeClass('text-hide');;
                    return false;
                }
            } else {
                $('#endDateError').text('Please provdie End Date').removeClass('text-hide');
                return false;
            }
        } else {
            $('#startDateError').text('Please provdie Start Date').removeClass('text-hide');
            return false;
        }
    } else {
        // add frequency error msg
    }
}
    return true;
}
function redirectToGateway(url, amount, id, name, hashKey, timeStamp, x_login,email, address, phoneNo, isRecuring, frequency, startDate, endDate) {
    var successURL = document.location.href + "\SuccessfulPayment";
    var failURL = document.location.href + "\FailedPayment";
    //url = "https://demo.globalgatewaye4.firstdata.com/payment";//"https://test.ipg-online.com/connect/gateway/processing";//"https://demo.globalgatewaye4.firstdata.com";//
    url = "https://checkout.globalgatewaye4.firstdata.com/payment";
    var form = $(document.createElement('form'));
    $(form).attr("action", url);
    $(form).attr("method", "POST");

    var input = $("<input>").attr("type", "hidden").attr("name", "x_login").val('WSP-HUFUS-erSN2ADDrA'); 
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_fp_timestamp").val(timeStamp);
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_fp_sequence").val(id);
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_fp_hash").val(hashKey);
    $(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "storename").val("10123456789");
    //$(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_amount").val(amount);
    $(form).append(input);

    //var input = $("<input>").attr("type", "hidden").attr("name", "mode").val("fullpay");
    //$(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "currency").val("");
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_address").val(address);//"karachi pakistan my home.");
    $(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_email").val(email);//"karachi pakistan my home.");
    $(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "x_city").val("Shikarpur.");
    //$(form).append($(input));

    var input = $("<input>").attr("type", "hidden").attr("name", "x_first_name").val(name);
    $(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "x_last_name").val(name);
    //$(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "x_country").val("Pakistan.");
    //$(form).append($(input));

  //  $(form).append('<input name="enable_level3_processing" type="hidden" value="TRUE" />');

    var input = $("<input>").attr("type", "hidden").attr("name", "x_show_form").val("PAYMENT_FORM");
    $(form).append($(input));
    //var input = $("<input>").attr("type", "hidden").attr("name", "x_fee_amount ").val("");
    //$(form).append($(input));

    //optional params
    var input = $("<input>").attr("type", "hidden").attr("name", "x_cust_id").val(email);
    $(form).append($(input));

    // should be remove later on
    var input = $("<input>").attr("type", "hidden").attr("name", "x_phone").val(phoneNo);
    $(form).append($(input));

    //Truncated to the first 20 characters and becomes part of the transaction. It appears in column “Customer Reference Number” under Transaction Search. It does not appear in the receipt.
    var input = $("<input>").attr("type", "hidden").attr("name", "x_po_num").val(phoneNo);
    $(form).append($(input));
    //Truncated to the first 20 characters and becomes part of the transaction. It appears in column “Ref Num” under Transaction Search and “TRANS. REF” in the CTR (customer transaction record).
    var input = $("<input>").attr("type", "hidden").attr("name", "x_invoice_num").val(email);
    $(form).append($(input));
    //Maximum length 30 and becomes part of the transaction. It appears in column "Reference Number 3" under Transaction Search. It does not appear in the receipt.
    var input = $("<input>").attr("type", "hidden").attr("name", "x_reference_3").val(address);
    $(form).append($(input));

    //var input = $("<input>").attr("type", "hidden").attr("name", "responseSuccessURL").val(successURL);
    //$(form).append($(input));
    //var input = $("<input>").attr("type", "hidden").attr("name", "responseFailURL").val(failURL);
    //$(form).append($(input));

    //var input1 = $("<input>").attr("type", "hidden").attr("pledgerId", "pledgerId").val(id);
    //$(form).append($(input1));
    //var input2 = $("<input>").attr("type", "hidden").attr("name", "name").val(name);
    //$(form).append($(input2));

    
    // recurring options
    if (isRecuring) {
        var input2 = $("<input>").attr("type", "hidden").attr("name", "x_recurring_billing").val('TRUE');
        $(form).append($(input2));

        var input2 = $("<input>").attr("type", "hidden").attr("name", "x_recurring_billing_id").val(frequency);    //.val('MB-HABIB-7-7464');//
        $(form).append($(input2)); 

        var input2 = $("<input>").attr("type", "hidden").attr("name", "x_recurring_billing_amount").val(amount);
        $(form).append($(input2));

        var input2 = $("<input>").attr("type", "hidden").attr("name", "Ecommerce_flag").val('2');
        $(form).append($(input2));
        //YYYY-MM-DD
        var input2 = $("<input>").attr("type", "hidden").attr("name", "x_recurring_billing_start_date").val(startDate);
        $(form).append($(input2));
        var input2 = $("<input>").attr("type", "hidden").attr("name", "x_recurring_billing_end_date").val(endDate);
        $(form).append($(input2));
    } 

    form.appendTo(document.body)
    $(form).submit();

}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function capitalize(text) {
    return text.replace(/\b\w/g, function (m) { return m.toUpperCase(); });
}
$(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height()+150 >= $(document).height()) {
        $('.scroll_btn').hide();
    } else {
        $('.scroll_btn').show();
    }
});