﻿var id = "";
function uploadContent(e) {
    try {
        var appId = 0;
        var filedata = new FormData();
        var file = e.files[0];
        id = e.id;
        if (file) {
            filedata.append(e.id, file);

            $.ajax({
                url: ('/FileUploader.ashx'),  //Server script to process data
                type: 'POST',
                xhr: function () {  // Custom XMLHttpRequest
                    var myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) { // Check if upload property exists
                        myXhr.upload.addEventListener('progress', progress, false); // For handling the progress of the upload  function (e) { if (console) { console.log(e); } else { console.debug(e); } }
                    }
                    return myXhr;
                },
                //Ajax events
                success: function (e) {
                    if (e) {
                        if (typeof (e) == "string")
                            var result = JSON.parse(e);
                        else
                            var result = e;

                        if (result.IsFail == false) {
                            $("#progressCounter_" + id).html("File Uploaded");
                            if ($("#PhotoId")) {
                                $("#PhotoId").val(result.DocId)
                            }
                            setTimeout(function () {
                                $("#progressPanel_" + id).width('0%');
                                $("#progressCounter_" + id).html('');
                                if ($('button[data-item="' + id + '"]'))
                                    $('button[data-item="' + id + '"]').show();
                            }, 1000);
                        } else {
                            if (result.IsFail == true) {
                                if (result.Message) {
                                    $("#progressCounter_" + id).html(result.Message);
                                }
                                else
                                    $("#progressCounter_" + id).html("Upload fail, please try again");
                            }
                        }
                    }
                },
                error: function (e) {
                    $("#progressCounter_" + id).html("Uploading failed, please try again.");
                },
                // Form data
                data: filedata,
                //Options to tell jQuery not to process data or worry about content-type.
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }
    catch (e) {
        if (console) {
            console.log(e);
        }
    }
}
function progress(progressEvent) {
    try {
        if (progressEvent.lengthComputable) {
            var percent = Math.round(progressEvent.loaded * 100 / progressEvent.total) + '%';
            $("#progressCounter_" + id).html(percent);
            $("#progressPanel_" + id).width(percent);
        }
    }
    catch (e) {
        if (console) {
            console.log(e);
        }
    }
}
function beforeSendHandler(e) {//check_file() {
    try {

        var filedata = new FormData();
        var file = e.files[0];

        var fileType = file.type;

        switch (fileType) {
            case 'jpg':
            case 'image/jpg':
            case 'jpeg':
            case 'image/jpeg':
                console.log(fileType);
                // do OK stuff
                // end check file size
                if (file.size >= 1000000) {//3120000) {
                    alert("Filename: " + file.name + " is too big!" + '\n' + "Your file must be no larger than 1000KB.");
                    $(fileUpload).reset();
                    return false;
                }
                return true;
                break;
            default:

                // do error stuff
                alert("File extension: " + fileType + " is not allowed." + '\n' + "Try only following file extensions:" + '\n' + "jpeg,jpg");
                $(fileUpload).reset();
                return false;
                break;
        }
    }
    catch (e) {
        if (console) {
            console.log(e);
        }
    }
}