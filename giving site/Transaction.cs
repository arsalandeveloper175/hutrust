//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DonorManagementApp
{
    using System;
    using System.Collections.Generic;
    
    public partial class Transaction
    {
        public int Id { get; set; }
        public int DonorId { get; set; }
        public string TransactionId { get; set; }
        public string ReferenceNumber { get; set; }
        public string AuthorizationNumber { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public int Recognition { get; set; }
        public string ReasonCode { get; set; }
        public string TransactionStatus { get; set; }
        public string SubscriptionId { get; set; }
        public string Description { get; set; }
        public string Receipt { get; set; }
        public System.DateTime DateTime { get; set; }
        public int NotificationEmailStatus { get; set; }
        public Nullable<System.DateTime> ApprovalDateTime { get; set; }
        public string ReceiptPath { get; set; }
        public string CardType { get; set; }
        public string TreatAsZakat { get; set; }
        public string TransactionType { get; set; }
        public string FullDescription { get; set; }
    }
}
