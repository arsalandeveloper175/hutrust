﻿using DonorManagementApp.Models;
using iText.Html2pdf;
using iTextSharp.text;
using iTextSharp.text.pdf;
using PdfSharp;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using static DonorManagementApp.Models.Util;

namespace DonorManagementApp.Controllers
{
    public class HomeController : Controller
    {
        string MonthlyRecurringPlan = ConfigurationManager.AppSettings["MonthlyPlan"].ToString();
        string YearlyRecurringPlan = ConfigurationManager.AppSettings["YearlyPlan"].ToString();
        string MonthlyRecurringEndDate = ConfigurationManager.AppSettings["MonthlyEndDate"].ToString();
        string YearlyRecurringEndDate = ConfigurationManager.AppSettings["YearlyEndDate"].ToString();
        public ActionResult Index(string msg = "")
        {
            List<QuickAmount> quickAmounts = null;
            using (var db = new DMSEntitiesNew())
            {
                quickAmounts = db.QuickAmounts.OrderBy(i => i.Indexing).ToList();
            }
            ViewBag.QuickAmounts = quickAmounts;
            ViewBag.Msg = msg;
            return View();
        }

        #region UBL Gateway integration
        //public async Task<ActionResult> Create([Bind(Include = "Id,FirstName,LastName,CellNumber,Email,Password,Organization,Type,TypeId,Currency,Recognition,RecurringDonationStatus,RecurringDonationAmount,RecurringDonationFrequency,RecurringTransactionEnabledDateTime,RecurringDonationStartDate,RecurringDonationEndDate,LastRecurringTransactionDateTime,NextRecurringTransactionDateTime,CreatedOn,UpdatedOn")] Donor donor)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Donate([Bind(Include = "Id,FirstName,LastName,CellNumber,Email,Organization,TreatAsZakat,Amount,Plan,Recognition,InTheNameOf,Address,HearFrom,HearFromField")] Donation donation)
        {
            string msg = "";
            try
            {
                if (string.IsNullOrEmpty(donation.Plan))
                {
                    return RedirectToAction("Index", "Home", new { msg = "Please select plan" });
                }
                if (string.IsNullOrEmpty(donation.FirstName))
                {
                    return RedirectToAction("Index", "Home", new { msg = "Please enter first name" });
                }
                if (string.IsNullOrEmpty(donation.LastName))
                {
                    return RedirectToAction("Index", "Home", new { msg = "Please enter last name" });
                }
                if (string.IsNullOrEmpty(donation.CellNumber))
                {
                    return RedirectToAction("Index", "Home", new { msg = "Please enter mobile" });
                }
                if (string.IsNullOrEmpty(donation.Email))
                {
                    return RedirectToAction("Index", "Home", new { msg = "Please enter email" });
                }
                if (string.IsNullOrEmpty(donation.HearFrom))
                {
                    return RedirectToAction("Index", "Home", new { msg = "Please enter how did you hear about us" });
                }
                else
                {
                    if (donation.HearFrom == "0")
                    {
                        return RedirectToAction("Index", "Home", new { msg = "Please enter how did you hear about us" });
                    }
                    else if (donation.HearFrom.ToLowerInvariant() == "other" || donation.HearFrom.ToLowerInvariant() == "friend/colleague")
                    {
                        if (string.IsNullOrEmpty(donation.HearFromField))
                        {
                            return RedirectToAction("Index", "Home", new { msg = "Please mention, how did you hear about us" });
                        }
                    }
                }
                if (ModelState.IsValid && donation.Amount > 0)
                {
                    /*
                    try
                    {
                        donation.IP = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                        if (string.IsNullOrEmpty(donation.IP))
                        {
                            donation.IP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                        }
                    }
                    catch (Exception ex)
                    {
                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate", "Donate",
                            string.Format("Unable to get IP address of client, Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                            CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                            donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));

                    }
                    */
                    if (donation.Plan.Replace(" ", "").ToLowerInvariant() == "onetime")
                    {
                        return RedirectToAction("OneTimeDonate", "Home", donation);
                    }
                    else if (donation.Plan.ToLowerInvariant() == "weekly" || donation.Plan.ToLowerInvariant() == "monthly" || donation.Plan.ToLowerInvariant() == "yearly")
                    {
                        return RedirectToAction("CreateRecurringTokenAndDonate", "Home", donation);
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Home", new { msg = "Please select amount" });

                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            //return Json(new { Success = flag, Msg = msg, Id = pledgerId, FirstName = donation.FirstName, LastName = donation.LastName, Amount = donation.Amount }, JsonRequestBehavior.AllowGet);
            return RedirectToAction("Index", new { Msg = msg });
        }


        public ActionResult OneTimeDonate([Bind(Include = "Id,FirstName,LastName,CellNumber,Email,Organization,TreatAsZakat,Amount,Plan,Recognition,InTheNameOf,Address,HearFrom,HearFromField")] Donation donation)
        {
            int pledgerId = 0;
            bool flag = false;
            string msg = "";

            FirstdataCreateTokenTransactionRequest firstdata = new FirstdataCreateTokenTransactionRequest();
            firstdata.x_first_name = donation.FirstName;
            firstdata.x_last_name = donation.LastName;
            firstdata.x_phone = donation.CellNumber;
            firstdata.x_email = donation.Email;
            firstdata.x_amount = donation.Amount.ToString();
            firstdata.x_recurring_billing = "FALSE";
            firstdata.x_currency_code = "USD";
            firstdata.x_type = "AUTH_CAPTURE";
            if (donation.Address.Length > 27)
            { firstdata.x_address = donation.Address.Substring(0, 27).ToString(); }
            else
            { firstdata.x_address = donation.Address; }

            AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate", "Donate",
                string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));

            if (donation.Id == 0)
            {
                Donor firstdata_Donors = new Donor();
                firstdata_Donors.Currency = "USD";
                firstdata_Donors.FirstName = donation.FirstName;
                firstdata_Donors.LastName = donation.LastName;
                firstdata_Donors.CellNumber = donation.CellNumber;
                firstdata_Donors.Email = donation.Email;
                firstdata_Donors.RecurringDonationFrequency = donation.Plan;
                firstdata_Donors.Organization = donation.Organization;
                firstdata_Donors.TreatAsZakat = string.IsNullOrEmpty(donation.TreatAsZakat) ? "Donation" : donation.TreatAsZakat;
                firstdata_Donors.TotalAmount = donation.Amount;
                firstdata_Donors.Address = donation.Address;
                firstdata_Donors.HearFrom = string.IsNullOrEmpty(donation.HearFromField) ? donation.HearFrom : donation.HearFromField;
                // firstdata_Donors.RecognitionInNameOf = donation.InTheNameOf;
                firstdata_Donors.Recognition = donation.Recognition;

                firstdata_Donors.UpdatedOn = DateTime.UtcNow.AddHours(5);
                firstdata_Donors.CreatedOn = DateTime.UtcNow.AddHours(5);


                firstdata_Donors.NextRecurringTransactionDateTime = DateTime.UtcNow.AddHours(5);
                firstdata_Donors.RecurringDonationAmount = 0;
                firstdata_Donors.RecurringDonationStatus = false;
                //hbl_Donors.RecurringTransactionEnabledDateTime = DateTime.UtcNow.AddHours(5);



                //  var s = hBLTransactionRequest.getSignature();
                //fields with default values


                using (var db = new DMSEntitiesNew())
                {
                    db.Donors.Add(firstdata_Donors);
                    if (db.SaveChanges() > 0)
                    {
                        if (firstdata_Donors.Id > 0)
                        {
                            pledgerId = firstdata_Donors.Id;
                            flag = true;

                            AuditLogger.Log("Successfully added pledger with selected funds in DB", "Donate", "Donate",
                             string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                                CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                                donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                        }
                        else
                        {
                            msg = "Unable to save record, please try again.";
                            AuditLogger.Log("Failed to save donor in DB, as donor id not found on code", "Donate", "Donate",
                            string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                               CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                               donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                        }
                    }
                    else
                    {
                        msg = "Unable to save record, please try again.";
                        AuditLogger.Log("Failed to save donor in DB, as EF is unable to save donor info", "Donate", "Donate",
                        string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                           CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                           donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                    }
                }

                #region Filling Firstdata gateway transaction info

                if (pledgerId > 0)
                {
                    try
                    {
                        //System.Net.SecurityProtocolType.Tls12.ToString();
                        firstdata.x_invoice_num = DateTime.Now.Ticks.ToString();//pledgerId.ToString();// just to pass on gateway request
                                                                                //return View(pay);

                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate-Pay", "Donate-Pay", string.Format("Redirecting to Payment Gateway, totalAmount:{0}, PledgerId:{1}, IP:{2}, Name:{3}",
                            donation.Amount, pledgerId, this.Request.ServerVariables["REMOTE_ADDR"], $"{donation.FirstName} {donation.LastName}"));

                        Transaction transaction = new Transaction();
                        transaction.DonorId = pledgerId;
                        transaction.TransactionStatus = "Pending";
                        transaction.Amount = donation.Amount;
                        transaction.DateTime = DateTime.UtcNow.AddHours(5);
                        transaction.TransactionId = "";
                        transaction.ReferenceNumber = firstdata.x_invoice_num;
                        transaction.Currency = "USD";
                        transaction.Recognition = donation.Recognition;
                        transaction.TreatAsZakat = donation.TreatAsZakat;
                        transaction.RecognitionInNameOf = donation.InTheNameOf;
                        transaction.TransactionType = donation.Plan.ToLowerInvariant();
                        transaction.SubscriptionId = "Single Transaction";


                        using (var db = new DMSEntitiesNew())
                        {
                            db.Transactions.Add(transaction);
                            if (db.SaveChanges() > 0)
                            {
                                flag = true;
                            }
                            else
                            {
                                flag = false;
                                msg = "unable to save transaction, please try again.";
                            }
                        }


                    }
                    catch (Exception ex)
                    {
                        msg = ex.Message;
                        ExceptionsLog transactionError = new ExceptionsLog();
                        transactionError.ErrorCode = (int)ErrorCode.ErrorInEpaymenAPI;
                        transactionError.ResponseCode = "0";//Convert.ToString(pay.Properties["ResponseCode"]);
                        transactionError.ResponseDescription = ""; //Convert.ToString(pay.Properties["ResponseDescription"]);
                        transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                        transactionError.ErrorMessage = ex.Message;
                        transactionError.ErrorDescription = ErrorCode.GeneralException.ToString();
                        transactionError.ErrorDescription = "PledgerId : " + pledgerId + "Error Code from API : " + ErrorCode.GeneralException.ToString();
                        using (var db = new DMSEntitiesNew())
                        {
                            db.ExceptionsLogs.Add(transactionError);
                            db.SaveChanges();
                        }
                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Doante-Pay", "Doante-Pay", string.Format("Main Exception Block, Error in IPG loading, totalAmount:{0}, PledgerId:{1}, IP:{2}, Name:{3}, TransactionId:{4}, ErrorDesc: {5}, ResponseDesc: {6}, ErrorCode: {7}, ResponseCode: {8}",
                          donation.Amount, pledgerId, this.Request.ServerVariables["REMOTE_ADDR"], $"{donation.FirstName} {donation.LastName}", firstdata.x_invoice_num, ErrorCode.GeneralException.ToString(), "", (int)ErrorCode.ErrorInEpaymenAPI, ""));
                        return RedirectToAction("Index", new { msg = ex.Message });
                    }
                }

                #endregion
                if (flag)
                {
                    return View("CreateRecurringTokenAndDonate", firstdata);
                }
                else
                {
                    return RedirectToAction("Index", new { msg = msg });
                }
            }
            return RedirectToAction("Index", new { msg = msg });
        }
        public ActionResult CreateRecurringTokenAndDonate([Bind(Include = "Id,FirstName,LastName,CellNumber,Email,Organization,TreatAsZakat,Amount,Plan,Recognition,InTheNameOf,Address,HearFrom,HearFromField")] Donation donation)
        {
            int pledgerId = 0;
            bool flag = false;
            string msg = "";

            FirstdataCreateTokenTransactionRequest firstdata = new FirstdataCreateTokenTransactionRequest();
            firstdata.x_first_name = donation.FirstName;
            firstdata.x_last_name = donation.LastName;
            firstdata.x_phone = donation.CellNumber;
            firstdata.x_email = donation.Email;
            firstdata.x_amount = donation.Amount.ToString();
            firstdata.x_recurring_billing_amount = donation.Amount.ToString();
            firstdata.x_recurring_billing = "TRUE";
            firstdata.x_currency_code = "USD";
            firstdata.x_type = "AUTH_CAPTURE";
            if (donation.Address.Length > 27)
            { firstdata.x_address = donation.Address.Substring(0, 27).ToString(); }
            else
            { firstdata.x_address = donation.Address; }

            AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate", "Donate",
                string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));

            if (donation.Id == 0)
            {
                Donor firstdata_Donors = new Donor();
                firstdata_Donors.Currency = "USD";
                firstdata_Donors.FirstName = donation.FirstName;
                firstdata_Donors.LastName = donation.LastName;
                firstdata_Donors.CellNumber = donation.CellNumber;
                firstdata_Donors.Email = donation.Email;
                firstdata_Donors.RecurringDonationFrequency = donation.Plan;
                firstdata_Donors.Organization = donation.Organization;
                firstdata_Donors.TreatAsZakat = string.IsNullOrEmpty(donation.TreatAsZakat) ? "Donation" : donation.TreatAsZakat;
                firstdata_Donors.TotalAmount = donation.Amount;
                firstdata_Donors.Address = donation.Address;
                firstdata_Donors.HearFrom = string.IsNullOrEmpty(donation.HearFromField) ? donation.HearFrom : donation.HearFromField;
                //firstdata_Donors.RecognitionInNameOf = donation.InTheNameOf;
                firstdata_Donors.Recognition = donation.Recognition;
                //hbl_Donors.TypeId = 0;
                //hbl_Donors.Type = "Regular Donor";
                firstdata_Donors.UpdatedOn = DateTime.UtcNow.AddHours(5);
                firstdata_Donors.CreatedOn = DateTime.UtcNow.AddHours(5);

                if (donation.Plan.ToLowerInvariant() == "weekly")
                {
                }
                if (donation.Plan.ToLowerInvariant() == "monthly")
                {
                    firstdata.x_recurring_billing_amount = donation.Amount.ToString();
                    firstdata.x_recurring_billing_start_date = DateTime.UtcNow.AddMonths(1).AddHours(5).ToString("yyyy-MM-dd");
                    firstdata.x_recurring_billing_end_date = MonthlyRecurringEndDate;
                    firstdata.x_recurring_billing_id = MonthlyRecurringPlan;

                    firstdata_Donors.RecurringDonationStartDate = DateTime.UtcNow.AddHours(5);
                    firstdata_Donors.RecurringDonationEndDate = Convert.ToDateTime(MonthlyRecurringEndDate);
                    firstdata_Donors.NextRecurringTransactionDateTime = DateTime.UtcNow.AddMonths(1).AddHours(5);
                    firstdata_Donors.RecurringDonationPlan = firstdata.x_recurring_billing_id;
                    firstdata_Donors.RecurringDonationAmount = donation.Amount;
                    firstdata_Donors.RecurringDonationStatus = true;
                    // hbl_Donors.RecurringTransactionEnabledDateTime = DateTime.UtcNow.AddHours(5);
                }
                if (donation.Plan.ToLowerInvariant() == "yearly")
                {

                    firstdata.x_recurring_billing_amount = donation.Amount.ToString();
                    firstdata.x_recurring_billing_start_date = DateTime.UtcNow.AddYears(1).AddHours(5).ToString("yyyy-MM-dd");
                    firstdata.x_recurring_billing_end_date = YearlyRecurringEndDate;
                    firstdata.x_recurring_billing_id = YearlyRecurringPlan;

                    firstdata_Donors.RecurringDonationStartDate = DateTime.UtcNow.AddHours(5);
                    firstdata_Donors.RecurringDonationEndDate = Convert.ToDateTime(YearlyRecurringEndDate);
                    firstdata_Donors.NextRecurringTransactionDateTime = DateTime.UtcNow.AddYears(1).AddHours(5);
                    firstdata_Donors.RecurringDonationPlan = firstdata.x_recurring_billing_id;
                    firstdata_Donors.RecurringDonationAmount = donation.Amount;
                    firstdata_Donors.RecurringDonationStatus = true;

                }


                //  var s = hBLTransactionRequest.getSignature();
                //fields with default values


                using (var db = new DMSEntitiesNew())
                {
                    db.Donors.Add(firstdata_Donors);
                    if (db.SaveChanges() > 0)
                    {
                        if (firstdata_Donors.Id > 0)
                        {
                            pledgerId = firstdata_Donors.Id;
                            flag = true;

                            AuditLogger.Log("Successfully added pledger with selected funds in DB", "Donate", "Donate",
                             string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                                CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                                donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                        }
                        else
                        {
                            msg = "Unable to save record, please try again.";
                            AuditLogger.Log("Failed to save donor in DB, as donor id not found on code", "Donate", "Donate",
                            string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                               CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                               donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                        }
                    }
                    else
                    {
                        msg = "Unable to save record, please try again.";
                        AuditLogger.Log("Failed to save donor in DB, as EF is unable to save donor info", "Donate", "Donate",
                        string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                           CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                           donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                    }
                }

                #region Filling Firstdata gateway transaction info

                if (pledgerId > 0)
                {
                    firstdata.x_invoice_num = DateTime.Now.Ticks.ToString();// just to pass on gateway request
                                                                            //return View(pay);
                    try
                    {
                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate-Pay", "Donate-Pay", string.Format("Redirecting to Payment Gateway, totalAmount:{0}, PledgerId:{1}, IP:{2}, Name:{3}",
                            donation.Amount, pledgerId, this.Request.ServerVariables["REMOTE_ADDR"], $"{donation.FirstName} {donation.LastName}"));

                        Transaction transaction = new Transaction();
                        transaction.DonorId = pledgerId;
                        transaction.TransactionStatus = "Pending";
                        transaction.Amount = donation.Amount;
                        transaction.DateTime = DateTime.UtcNow.AddHours(5);
                        transaction.TransactionId = "";
                        transaction.ReferenceNumber = firstdata.x_invoice_num;
                        transaction.Currency = "USD";
                        transaction.Recognition = donation.Recognition;
                        transaction.TreatAsZakat = donation.TreatAsZakat;
                        transaction.TransactionType = donation.Plan.ToLowerInvariant();
                        transaction.SubscriptionId = firstdata.x_recurring_billing_id;
                        transaction.RecognitionInNameOf = donation.InTheNameOf;
                        using (var db = new DMSEntitiesNew())
                        {
                            db.Transactions.Add(transaction);
                            db.SaveChanges();
                        }


                    }
                    catch (Exception ex)
                    {
                        msg = ex.Message;
                        ExceptionsLog transactionError = new ExceptionsLog();
                        transactionError.ErrorCode = (int)ErrorCode.ErrorInEpaymenAPI;
                        transactionError.ResponseCode = "0";//Convert.ToString(pay.Properties["ResponseCode"]);
                        transactionError.ResponseDescription = ""; //Convert.ToString(pay.Properties["ResponseDescription"]);
                        transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                        transactionError.ErrorMessage = ex.Message;
                        transactionError.ErrorDescription = ErrorCode.GeneralException.ToString();
                        transactionError.ErrorDescription = "PledgerId : " + pledgerId + "Error Code from API : " + ErrorCode.GeneralException.ToString();
                        using (var db = new DMSEntitiesNew())
                        {
                            db.ExceptionsLogs.Add(transactionError);
                            db.SaveChanges();
                        }
                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Doante-Pay", "Doante-Pay", string.Format("Main Exception Block, Error in IPG loading, totalAmount:{0}, PledgerId:{1}, IP:{2}, Name:{3}, TransactionId:{4}, ErrorDesc: {5}, ResponseDesc: {6}, ErrorCode: {7}, ResponseCode: {8}",
                          donation.Amount, pledgerId, this.Request.ServerVariables["REMOTE_ADDR"], $"{donation.FirstName} {donation.LastName}", firstdata.x_invoice_num, ErrorCode.GeneralException.ToString(), "", (int)ErrorCode.ErrorInEpaymenAPI, ""));
                        return RedirectToAction("Index", new { msg = ex.Message });
                    }
                }

                #endregion
                return View(firstdata);
            }



            return View();
        }


        public ActionResult originalOneTimeDonate([Bind(Include = "Id,FirstName,LastName,CellNumber,Email,Organization,TreatAsZakat,Amount,Plan")] Donation donation)
        {
            int pledgerId = 0;
            bool flag = false;
            string msg = "";

            HBLTransactionRequest hBLTransactionRequest = new HBLTransactionRequest();
            hBLTransactionRequest.bill_to_phone = donation.CellNumber;
            hBLTransactionRequest.bill_to_forename = donation.FirstName;
            hBLTransactionRequest.bill_to_surname = donation.LastName;
            hBLTransactionRequest.bill_to_email = donation.Email;
            hBLTransactionRequest.amount = donation.Amount.ToString();
            //hBLTransactionRequest.customer_ip_address = donation.IP;

            AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate", "Donate",
                string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));

            if (donation.Id == 0)
            {
                Donor hbl_Donors = new Donor();
                hbl_Donors.FirstName = donation.FirstName;
                hbl_Donors.LastName = donation.LastName;
                hbl_Donors.CellNumber = donation.CellNumber;
                hbl_Donors.Email = donation.Email;
                hbl_Donors.RecurringDonationFrequency = donation.Plan;
                hbl_Donors.Organization = donation.Organization;
                hbl_Donors.TreatAsZakat = string.IsNullOrEmpty(donation.TreatAsZakat) ? "Donation" : donation.TreatAsZakat;
                hbl_Donors.TotalAmount = donation.Amount;
                hbl_Donors.UpdatedOn = DateTime.UtcNow.AddHours(5);
                hbl_Donors.CreatedOn = DateTime.UtcNow.AddHours(5);


                hBLTransactionRequest.transaction_type = "sale";
                hBLTransactionRequest.merchant_defined_data2 = string.IsNullOrEmpty(donation.TreatAsZakat) ? "Donation" : "Zakat";
                hBLTransactionRequest.consumer_id = donation.Id.ToString();

                hbl_Donors.RecurringDonationStartDate = DateTime.UtcNow.AddHours(5);
                hbl_Donors.NextRecurringTransactionDateTime = DateTime.UtcNow.AddMonths(1).AddHours(5);
                hbl_Donors.RecurringDonationAmount = donation.Amount;
                hbl_Donors.RecurringDonationStatus = true;
                //hbl_Donors.RecurringTransactionEnabledDateTime = DateTime.UtcNow.AddHours(5);



                //  var s = hBLTransactionRequest.getSignature();
                //fields with default values


                using (var db = new DMSEntitiesNew())
                {
                    db.Donors.Add(hbl_Donors);
                    if (db.SaveChanges() > 0)
                    {
                        if (hbl_Donors.Id > 0)
                        {
                            pledgerId = hbl_Donors.Id;
                            flag = true;

                            AuditLogger.Log("Successfully added pledger with selected funds in DB", "Donate", "Donate",
                             string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                                CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                                donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                        }
                        else
                        {
                            msg = "Unable to save record, please try again.";
                            AuditLogger.Log("Failed to save donor in DB, as donor id not found on code", "Donate", "Donate",
                            string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                               CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                               donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                        }
                    }
                    else
                    {
                        msg = "Unable to save record, please try again.";
                        AuditLogger.Log("Failed to save donor in DB, as EF is unable to save donor info", "Donate", "Donate",
                        string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                           CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                           donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                    }
                }

                #region Filling HBL gateway transaction info

                if (pledgerId > 0)
                {
                    try
                    {
                        //System.Net.SecurityProtocolType.Tls12.ToString();


                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate-Pay", "Donate-Pay", string.Format("Redirecting to Payment Gateway, totalAmount:{0}, PledgerId:{1}, IP:{2}, Name:{3}",
                            donation.Amount, pledgerId, this.Request.ServerVariables["REMOTE_ADDR"], $"{donation.FirstName} {donation.LastName}"));

                        Transaction transaction = new Transaction();
                        transaction.DonorId = pledgerId;
                        transaction.TransactionStatus = "Pending";
                        transaction.Amount = donation.Amount;
                        transaction.DateTime = DateTime.UtcNow.AddHours(5);
                        transaction.TransactionId = "";
                        transaction.ReferenceNumber = hBLTransactionRequest.reference_number;
                        //transaction.TransactionUUId = hBLTransactionRequest.transaction_uuid;//Convert.ToString(pay.GetProperty("TransactionID"));
                        transaction.Currency = "PKR";
                        transaction.TreatAsZakat = donation.TreatAsZakat;
                        transaction.TransactionType = donation.Plan.ToLowerInvariant();
                        //transaction.TransactionRequestType = hBLTransactionRequest.transaction_type;

                        using (var db = new DMSEntitiesNew())
                        {
                            db.Transactions.Add(transaction);
                            if (db.SaveChanges() > 0)
                            {
                                flag = true;
                            }
                            else
                            {
                                flag = false;
                                msg = "unable to save transaction, please try again.";
                            }
                        }

                        hBLTransactionRequest.consumer_id = pledgerId.ToString();// just to pass on gateway request
                                                                                 //return View(pay);
                    }
                    catch (Exception ex)
                    {
                        msg = ex.Message;
                        ExceptionsLog transactionError = new ExceptionsLog();
                        transactionError.ErrorCode = (int)ErrorCode.ErrorInEpaymenAPI;
                        transactionError.ResponseCode = "0";//Convert.ToString(pay.Properties["ResponseCode"]);
                        transactionError.ResponseDescription = ""; //Convert.ToString(pay.Properties["ResponseDescription"]);
                        transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                        transactionError.ErrorMessage = ex.Message;
                        transactionError.ErrorDescription = ErrorCode.GeneralException.ToString();
                        transactionError.ErrorDescription = "PledgerId : " + pledgerId + "Error Code from API : " + ErrorCode.GeneralException.ToString();
                        using (var db = new DMSEntitiesNew())
                        {
                            db.ExceptionsLogs.Add(transactionError);
                            db.SaveChanges();
                        }
                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Doante-Pay", "Doante-Pay", string.Format("Main Exception Block, Error in IPG loading, totalAmount:{0}, PledgerId:{1}, IP:{2}, Name:{3}, TransactionId:{4}, ErrorDesc: {5}, ResponseDesc: {6}, ErrorCode: {7}, ResponseCode: {8}",
                          donation.Amount, pledgerId, this.Request.ServerVariables["REMOTE_ADDR"], $"{donation.FirstName} {donation.LastName}", hBLTransactionRequest.transaction_uuid, ErrorCode.GeneralException.ToString(), "", (int)ErrorCode.ErrorInEpaymenAPI, ""));
                        return RedirectToAction("Index", new { msg = ex.Message });
                    }
                }

                #endregion
                if (flag)
                {
                    return View("Donate", hBLTransactionRequest);
                }
                else
                {
                    return RedirectToAction("Index", new { msg = msg });
                }
            }
            return RedirectToAction("Index", new { msg = msg });
        }

        public ActionResult originalCreateRecurringTokenAndDonate([Bind(Include = "Id,FirstName,LastName,CellNumber,Email,Organization,TreatAsZakat,Amount,Plan")] Donation donation)
        {
            int pledgerId = 0;
            bool flag = false;
            string msg = "";

            HBLCreateTokenTransactionRequest hBLTransactionRequest = new HBLCreateTokenTransactionRequest();
            hBLTransactionRequest.bill_to_phone = donation.CellNumber;
            hBLTransactionRequest.bill_to_forename = donation.FirstName;
            hBLTransactionRequest.bill_to_surname = donation.LastName;
            hBLTransactionRequest.bill_to_email = donation.Email;
            hBLTransactionRequest.amount = donation.Amount.ToString();
            //hBLTransactionRequest.customer_ip_address = donation.IP;

            AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate", "Donate",
                string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));

            if (donation.Id == 0)
            {
                Donor hbl_Donors = new Donor();
                hbl_Donors.FirstName = donation.FirstName;
                hbl_Donors.LastName = donation.LastName;
                hbl_Donors.CellNumber = donation.CellNumber;
                hbl_Donors.Email = donation.Email;
                hbl_Donors.RecurringDonationFrequency = donation.Plan;
                hbl_Donors.Organization = donation.Organization;
                hbl_Donors.TreatAsZakat = string.IsNullOrEmpty(donation.TreatAsZakat) ? "Donation" : donation.TreatAsZakat;
                hbl_Donors.TotalAmount = donation.Amount;
                //hbl_Donors.TypeId = 0;
                //hbl_Donors.Type = "Regular Donor";
                hbl_Donors.UpdatedOn = DateTime.UtcNow.AddHours(5);
                hbl_Donors.CreatedOn = DateTime.UtcNow.AddHours(5);

                if (donation.Plan.ToLowerInvariant() == "weekly")
                {
                    hBLTransactionRequest.transaction_type = "sale,create_payment_token";
                    hBLTransactionRequest.recurring_frequency = "weekly";
                    hBLTransactionRequest.recurring_amount = donation.Amount.ToString();
                    hBLTransactionRequest.recurring_start_date = DateTime.UtcNow.AddHours(5).ToString("yyyyMMdd");
                    hBLTransactionRequest.consumer_id = pledgerId.ToString();
                    hBLTransactionRequest.recurring_number_of_installments = "261";
                    hBLTransactionRequest.merchant_defined_data2 = string.IsNullOrEmpty(donation.TreatAsZakat) ? "Donation" : "Zakat";

                    hbl_Donors.RecurringDonationStartDate = DateTime.UtcNow.AddHours(5);
                    hbl_Donors.NextRecurringTransactionDateTime = DateTime.UtcNow.AddMonths(1).AddHours(5);
                    hbl_Donors.RecurringDonationAmount = donation.Amount;
                    hbl_Donors.RecurringDonationStatus = true;
                    // hbl_Donors.RecurringTransactionEnabledDateTime = DateTime.UtcNow.AddHours(5);
                }
                if (donation.Plan.ToLowerInvariant() == "monthly")
                {
                    hBLTransactionRequest.transaction_type = "sale,create_payment_token";
                    hBLTransactionRequest.recurring_frequency = "monthly";
                    hBLTransactionRequest.recurring_amount = donation.Amount.ToString();
                    hBLTransactionRequest.recurring_start_date = DateTime.UtcNow.AddMonths(1).AddHours(5).ToString("yyyyMMdd");
                    hBLTransactionRequest.consumer_id = pledgerId.ToString();
                    hBLTransactionRequest.recurring_number_of_installments = "60";
                    hBLTransactionRequest.merchant_defined_data2 = string.IsNullOrEmpty(donation.TreatAsZakat) ? "Donation" : "Zakat";


                    hbl_Donors.RecurringDonationStartDate = DateTime.UtcNow.AddHours(5);
                    hbl_Donors.NextRecurringTransactionDateTime = DateTime.UtcNow.AddMonths(1).AddHours(5);
                    hbl_Donors.RecurringDonationAmount = donation.Amount;
                    hbl_Donors.RecurringDonationStatus = true;
                    // hbl_Donors.RecurringTransactionEnabledDateTime = DateTime.UtcNow.AddHours(5);
                }
                if (donation.Plan.ToLowerInvariant() == "yearly")
                {
                    //hBLTransactionRequest.transaction_type = "sale,create_payment_token";
                    hBLTransactionRequest.recurring_frequency = "annually";
                    hBLTransactionRequest.recurring_amount = donation.Amount.ToString();
                    hBLTransactionRequest.recurring_start_date = DateTime.UtcNow.AddYears(1).AddHours(5).ToString("yyyyMMdd");
                    hBLTransactionRequest.consumer_id = pledgerId.ToString();
                    hBLTransactionRequest.recurring_number_of_installments = "5";
                    hBLTransactionRequest.merchant_defined_data2 = string.IsNullOrEmpty(donation.TreatAsZakat) ? "Donation" : "Zakat";

                    hbl_Donors.RecurringDonationStartDate = DateTime.UtcNow.AddHours(5);
                    hbl_Donors.NextRecurringTransactionDateTime = DateTime.UtcNow.AddYears(1).AddHours(5);
                    hbl_Donors.RecurringDonationAmount = donation.Amount;
                    hbl_Donors.RecurringDonationStatus = true;
                    //hbl_Donors.RecurringTransactionEnabledDateTime = DateTime.UtcNow.AddHours(5);
                }


                //  var s = hBLTransactionRequest.getSignature();
                //fields with default values


                using (var db = new DMSEntitiesNew())
                {
                    db.Donors.Add(hbl_Donors);
                    if (db.SaveChanges() > 0)
                    {
                        if (hbl_Donors.Id > 0)
                        {
                            pledgerId = hbl_Donors.Id;
                            flag = true;

                            AuditLogger.Log("Successfully added pledger with selected funds in DB", "Donate", "Donate",
                             string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                                CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                                donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                        }
                        else
                        {
                            msg = "Unable to save record, please try again.";
                            AuditLogger.Log("Failed to save donor in DB, as donor id not found on code", "Donate", "Donate",
                            string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                               CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                               donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                        }
                    }
                    else
                    {
                        msg = "Unable to save record, please try again.";
                        AuditLogger.Log("Failed to save donor in DB, as EF is unable to save donor info", "Donate", "Donate",
                        string.Format(" Name:{0}, Email:{1}, CellNumber{2}, TreatAsZakat:{3}, Amount:{4}, AmountUSD{5}, Rate{6}, IP{7}, DonorId{8}",
                           CamelCase(donation.FirstName + " " + donation.LastName), donation.Email, donation.CellNumber, donation.TreatAsZakat,
                           donation.Amount, 0, 0, this.Request.ServerVariables["REMOTE_ADDR"], donation.Id));
                    }
                }

                #region Filling HBL gateway transaction info

                if (pledgerId > 0)
                {
                    try
                    {
                        //System.Net.SecurityProtocolType.Tls12.ToString();


                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Donate-Pay", "Donate-Pay", string.Format("Redirecting to Payment Gateway, totalAmount:{0}, PledgerId:{1}, IP:{2}, Name:{3}",
                            donation.Amount, pledgerId, this.Request.ServerVariables["REMOTE_ADDR"], $"{donation.FirstName} {donation.LastName}"));

                        Transaction transaction = new Transaction();
                        transaction.DonorId = pledgerId;
                        transaction.TransactionStatus = "Pending";
                        transaction.Amount = donation.Amount;
                        transaction.DateTime = DateTime.UtcNow.AddHours(5);
                        transaction.TransactionId = "";
                        transaction.ReferenceNumber = hBLTransactionRequest.reference_number;
                        //transaction.TransactionUUId = hBLTransactionRequest.transaction_uuid;//Convert.ToString(pay.GetProperty("TransactionID"));
                        transaction.Currency = "PKR";
                        transaction.TreatAsZakat = donation.TreatAsZakat;
                        transaction.TransactionType = donation.Plan.ToLowerInvariant();
                        //transaction.TransactionRequestType = hBLTransactionRequest.transaction_type;

                        using (var db = new DMSEntitiesNew())
                        {
                            db.Transactions.Add(transaction);
                            db.SaveChanges();
                        }

                        hBLTransactionRequest.consumer_id = pledgerId.ToString();// just to pass on gateway request
                                                                                 //return View(pay);
                    }
                    catch (Exception ex)
                    {
                        msg = ex.Message;
                        ExceptionsLog transactionError = new ExceptionsLog();
                        transactionError.ErrorCode = (int)ErrorCode.ErrorInEpaymenAPI;
                        transactionError.ResponseCode = "0";//Convert.ToString(pay.Properties["ResponseCode"]);
                        transactionError.ResponseDescription = ""; //Convert.ToString(pay.Properties["ResponseDescription"]);
                        transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                        transactionError.ErrorMessage = ex.Message;
                        transactionError.ErrorDescription = ErrorCode.GeneralException.ToString();
                        transactionError.ErrorDescription = "PledgerId : " + pledgerId + "Error Code from API : " + ErrorCode.GeneralException.ToString();
                        using (var db = new DMSEntitiesNew())
                        {
                            db.ExceptionsLogs.Add(transactionError);
                            db.SaveChanges();
                        }
                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "Doante-Pay", "Doante-Pay", string.Format("Main Exception Block, Error in IPG loading, totalAmount:{0}, PledgerId:{1}, IP:{2}, Name:{3}, TransactionId:{4}, ErrorDesc: {5}, ResponseDesc: {6}, ErrorCode: {7}, ResponseCode: {8}",
                          donation.Amount, pledgerId, this.Request.ServerVariables["REMOTE_ADDR"], $"{donation.FirstName} {donation.LastName}", hBLTransactionRequest.transaction_uuid, ErrorCode.GeneralException.ToString(), "", (int)ErrorCode.ErrorInEpaymenAPI, ""));
                        return RedirectToAction("Index", new { msg = ex.Message });
                    }
                }

                #endregion
                return View(hBLTransactionRequest);
            }



            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> PaymentStatus()
        {
            int sessionPledgerId = 0;
            int donorId = 0;
            string reference_number = "";
            Transaction transaction = null;
            ExceptionsLog transactionError;
            Donor donor = null;
            string receiptPath = "";
            //string signature = "";
            string payment_token = "";
            string _type = "";
            string certificatePath = "";
            int flag = 0;
            //bool isCallingFromCyberSourceScheduler = false;
            StringBuilder stringBuilder = new StringBuilder();
            List<string> CCAdresses = new List<string>();
            try
            {
                IDictionary<string, string> parameter = new Dictionary<string, string>();

                foreach (string property in Request.Form.AllKeys)
                {
                    stringBuilder.AppendLine($"{property}:{Request.Params[property]}");
                    parameter.Add(property, Request.Form[property]);
                }
                string x_trans_id = parameter["x_trans_id"].ToString();
                string x_invoice_num = parameter["x_invoice_num"].ToString();
                string x_response_reason_text = parameter["x_response_reason_text"].ToString();
                string x_auth_code = parameter["x_auth_code"].ToString();
                string x_amount = parameter["x_amount"].ToString();
                if (Request.Form.AllKeys.Contains("x_recurring_billing_id"))
                { string x_recurring_billing_id = parameter["x_recurring_billing_id"].ToString(); }
                string x_currency_code = parameter["x_currency_code"].ToString();
                string TransactionCardType = parameter["TransactionCardType"].ToString();
                string Retrieval_Ref_No = parameter["Authorization_Num"].ToString();
                string Bank_Resp_Code = parameter["Bank_Resp_Code"].ToString();
                string Bank_Message = parameter["Bank_Message"].ToString();
                string Reference_No = parameter["Reference_No"].ToString();
                string exact_ctr = parameter["exact_ctr"].ToString();

                #region cc address loader
                string strCCList = ConfigurationManager.AppSettings.Get("bccEmail");
                if (!string.IsNullOrEmpty(strCCList))
                {
                    var ccList = strCCList.Split(';');
                    if (ccList != null)
                    {
                        foreach (var ccItem in ccList)
                        {
                            CCAdresses.Add(ccItem);
                        }
                    }
                }
                #endregion

                reference_number = Reference_No;


                //foreach (var key in Request.Form.AllKeys)
                //{
                //    stringBuilder.AppendLine($"{key}:{Request.Params[key]}");
                //    //Response.Write("<span>" + key + "</span><input type=\"text\" name=\"" + key + "\" size=\"50\" value=\"" + Request.Params[key] + "\" readonly=\"true\"/><br/>");
                //    parameters.Add(key, Request.Params[key]);
                //}

                //signature = Request.Params["signature"];

                if (string.IsNullOrEmpty(x_trans_id))
                {
                    return RedirectToAction("Index", "Home", new { msg = string.Format("Error occured, transaction details not found, while processing transaction ID: {0}, please try again.", x_trans_id) });
                }
                //if (!string.IsNullOrEmpty(Request.Params["payment_token"]))
                //{
                //    payment_token = Request.Params["payment_token"].ToString();
                //}
                #region add record in DB
                DMSEntitiesNew context = new DMSEntitiesNew();
                transaction = (from t in context.Transactions
                               where t.ReferenceNumber == x_invoice_num
                               select t).SingleOrDefault();
                donor = (from v in context.Donors
                         where v.Id == transaction.DonorId
                         select v).SingleOrDefault();

                //bool validation = Request.Params["signature"].Equals(Security.sign(parameters));
                if (Bank_Resp_Code == "100")
                {
                    donorId = donor.Id;
                    flag = 1;

                    if (donorId > 0)
                    {
                        try
                        {
                            #region generate PDF receipt
                            string nextTransactionDate = "";
                            if (transaction.TransactionType == "monthly")
                            {
                                nextTransactionDate = transaction.DateTime.AddMonths(1).ToString("dd/MMMM/yyyy");
                            }
                            else if (transaction.TransactionType == "yearly")
                            {
                                nextTransactionDate = transaction.DateTime.AddYears(1).ToString("dd/MMMM/yyyy");
                            }
                            TransactionReceipt transactionReceipt = new TransactionReceipt();

                            transactionReceipt.Id = donorId;
                            transactionReceipt.ReceiptNo = String.Format("{0:D6}", transaction.Id);
                            transactionReceipt.Amount = String.Format("{0:n0}", transaction.Amount);
                            transactionReceipt.AmountInFigure = NumberToWords(Convert.ToInt32(transaction.Amount), true);
                            transactionReceipt.Date = transaction.DateTime.ToString("dd/MMMM/yyyy");
                            transactionReceipt.FullName = $"{donor.FirstName} {donor.LastName}";
                            transactionReceipt.NatureOfDoation = string.IsNullOrEmpty(transaction.TreatAsZakat) ? "Donation" : "Zakat";
                            transactionReceipt.Plan = Util.CamelCase(transaction.TransactionType);
                            transactionReceipt.NationalTexNo = "3017275-6";
                            transactionReceipt.TransactionId = x_trans_id;
                            transactionReceipt.NextTransactionDate = nextTransactionDate;
                            transactionReceipt.Organization = donor.Organization;
                            transactionReceipt.Address = donor.Address;
                            if (donor.Recognition == 1)
                            { transactionReceipt.ContributionRecognition = "Anonymous"; }
                            else if (donor.Recognition == 2)
                            { transactionReceipt.ContributionRecognition = "In the name/memory of : " + transaction.RecognitionInNameOf; }
                            else
                            { transactionReceipt.ContributionRecognition = "Self"; }
                            var viewString = RenderViewToString(ControllerContext, "ViewToPDF", transactionReceipt);
                            var path = Server.MapPath("/Receipts" + "//" + x_trans_id + "_Receipt.pdf");
                            var cssFilePath = Server.MapPath("/content/viewToPDF.css");
                            string cssFile = System.IO.File.ReadAllText(cssFilePath);
                            if (SaveAdmitCardPDF(viewString, path, cssFile))
                            {
                                receiptPath = path;
                                transaction.ReceiptPath = receiptPath;
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            transactionError = new ExceptionsLog();
                            transactionError.ErrorCode = (int)ErrorCode.TransactionFailed;
                            transactionError.Amount = Convert.ToInt32(Request.Params["auth_amount="]);
                            // transactionError.InvoiceId = Convert.ToInt64(invoice.INVOICE_NO);
                            transactionError.ResponseCode = Convert.ToString(Request.Params["reason_code"]);
                            transactionError.ResponseDescription = $"{Convert.ToString(Request.Params["message"])}, details: { stringBuilder.ToString()}";
                            transactionError.DonorId = donorId;
                            transactionError.FullName = "Find name from donor table";
                            transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                            transactionError.TransactionId = Convert.ToString(Request.Params["transaction_id"]);

                            using (var db = new DMSEntitiesNew())
                            {
                                db.ExceptionsLogs.Add(transactionError);
                                db.SaveChanges();
                            }
                            AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "PaymentStatus", "",
                                     string.Format("Failed payment for DonorId:{0}, IP{1}, transactionId:{2}, ResponseDesc:{3}, Name: {4}, Amount: {5}",
                                     sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], x_trans_id, Bank_Message, "Find donor name from table", x_amount));

                            AuditLogger.LogToDB(donor.Email, 0,
                                     string.Format("Failed payment for DonorId:{0}, IP{1}, transactionId:{2}, ResponseDesc:{3}, Name: {4}, Amount: {5}",
                                     sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], x_trans_id, Bank_Message, "Find donor name from table", x_amount), "");



                        }
                        //using (var db = new DMSEntitiesNew())
                        //{
                        //    var transactions = db.Transactions.Where(p => p.ReferenceNumber == reference_number).ToList();

                        //    if (transactions != null && transactions.Count > 0)
                        //    {
                        //        transaction = transactions.LastOrDefault();

                        //        transaction.TransactionStatus = "Approved";
                        //        transaction.ReasonCode = Convert.ToString(Request.Params["reason_code"]);
                        //        transaction.TransactionStatus = Convert.ToString(Request.Params["message"]);
                        //        transaction.ApprovalDateTime = string.IsNullOrEmpty(Convert.ToString(Request.Params["signed_date_time"])) ? DateTime.UtcNow.AddHours(5) : Convert.ToDateTime(Convert.ToString(Request.Params["signed_date_time"]));
                        //        transaction.TransactionId = Convert.ToString(Request.Params["transaction_id"]);
                        //        transaction.SubscriptionId = payment_token;
                        //        transaction.ReasonCode = Convert.ToString(Request.Params["reason_code"]);
                        //        transaction.ReferenceNumber = reference_number;
                        //        transaction.Receipt = stringBuilder.ToString();
                        //        transaction.ReceiptPath = receiptPath;
                        //        db.SaveChanges();
                        //    }
                        //    else if (isCallingFromCyberSourceScheduler)// new transaction from CyberSource Scheduler
                        //    {
                        //        transaction = new Transaction();
                        //        transaction.TransactionStatus = "Approved";
                        //        transaction.ReasonCode = Convert.ToString(Request.Params["reason_code"]);
                        //        transaction.TransactionStatus = Convert.ToString(Request.Params["message"]);
                        //        transaction.ApprovalDateTime = string.IsNullOrEmpty(Convert.ToString(Request.Params["signed_date_time"])) ? DateTime.UtcNow.AddHours(5) : Convert.ToDateTime(Convert.ToString(Request.Params["signed_date_time"]));
                        //        transaction.TransactionId = Convert.ToString(Request.Params["transaction_id"]);
                        //        transaction.SubscriptionId = payment_token;
                        //        transaction.ReasonCode = Convert.ToString(Request.Params["reason_code"]);
                        //        transaction.ReferenceNumber = reference_number;
                        //        transaction.Receipt = stringBuilder.ToString();
                        //        transaction.ReceiptPath = receiptPath;
                        //        db.Transactions.Add(transaction);
                        //        db.SaveChanges();

                        //    }
                        transaction.TransactionId = x_trans_id;
                        transaction.ReferenceNumber = Reference_No;
                        transaction.AuthorizationNumber = Retrieval_Ref_No;
                        transaction.ReasonCode = Bank_Resp_Code;
                        transaction.TransactionStatus = Bank_Message;
                        transaction.Description = x_response_reason_text;
                        transaction.Receipt = exact_ctr;
                        transaction.CardType = TransactionCardType;
                        transaction.FullDescription = stringBuilder.ToString();
                        context.SaveChanges();

                        #region generate PDF certificate
                        try
                        {
                            if (transaction.Recognition != Recognition.Anonymous.GetHashCode())
                            {
                                var checkIsNameOf = transaction.RecognitionInNameOf;
                                if (string.IsNullOrEmpty(checkIsNameOf))
                                {

                                    try
                                    {
                                        donor = DonorManagementApp.Controllers.HomeController.GetDonorCategory(donor.FirstName, donor.LastName, donor.CellNumber, donor.Email);

                                    }
                                    catch (Exception ex)
                                    {
                                        transactionError = new ExceptionsLog();
                                        transactionError.ErrorCode = (int)ErrorCode.TransactionFailed;
                                        transactionError.Amount = Convert.ToInt32(Request.Params["auth_amount="]);
                                        // transactionError.InvoiceId = Convert.ToInt64(invoice.INVOICE_NO);
                                        transactionError.ResponseCode = Convert.ToString(Request.Params["reason_code"]);
                                        transactionError.ResponseDescription = $"Unable to get donor category to create certificate, {Convert.ToString(Request.Params["message"])}, Message:{ex.Message}, details: { stringBuilder.ToString()}";
                                        transactionError.DonorId = donorId;
                                        transactionError.FullName = "No Donor Category";
                                        transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                                        transactionError.TransactionId = Convert.ToString(Request.Params["transaction_id"]);

                                        using (var db = new DMSEntitiesNew())
                                        {
                                            db.ExceptionsLogs.Add(transactionError);
                                            db.SaveChanges();
                                        }
                                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "PaymentStatus", "",
                                                 string.Format("Unable to create certificate for PledgerId:{0}, IP{1}, transactionId:{2}, ResponseDesc:{3}, Name: {4}, Amount: {5}",
                                                 sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], Request.Params["transaction_id"], Request.Params["message"], "Find donor name from table", Request.Params["auth_amount="]));
                                    }
                                    _type = donor.Type;
                                    if (string.IsNullOrEmpty(_type))
                                    {
                                        _type = _type.ToLowerInvariant();
                                    }
                                    string name = $"{donor.FirstName} {donor.LastName}";
                                    if (!string.IsNullOrEmpty(transaction.RecognitionInNameOf))
                                        name = transaction.RecognitionInNameOf;
                                    certificatePath = GenerateCertificate(name, donor.Type);
                                    if (!string.IsNullOrEmpty(certificatePath))
                                    {
                                        using (var db = new DMSEntitiesNew())
                                        {
                                            var transactions = db.Transactions.Where(p => p.ReferenceNumber == reference_number).ToList();
                                            if (transactions != null && transactions.Count > 0)
                                            {
                                                transaction = transactions.LastOrDefault();
                                                transaction.CertificatePath = certificatePath;
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    string name = transaction.RecognitionInNameOf.ToString();
                                    if (!string.IsNullOrEmpty(transaction.RecognitionInNameOf))
                                        name = transaction.RecognitionInNameOf;
                                    certificatePath = GenerateCertificate(name, "inNameOf");
                                    if (!string.IsNullOrEmpty(certificatePath))
                                    {
                                        using (var db = new DMSEntitiesNew())
                                        {
                                            var transactions = db.Transactions.Where(p => p.ReferenceNumber == reference_number).ToList();
                                            if (transactions != null && transactions.Count > 0)
                                            {
                                                transaction = transactions.LastOrDefault();
                                                transaction.CertificatePath = certificatePath;
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            transactionError = new ExceptionsLog();
                            transactionError.ErrorCode = (int)ErrorCode.TransactionFailed;
                            transactionError.Amount = Convert.ToInt32(Request.Params["auth_amount="]);
                            // transactionError.InvoiceId = Convert.ToInt64(invoice.INVOICE_NO);
                            transactionError.ResponseCode = Convert.ToString(Request.Params["reason_code"]);
                            transactionError.ResponseDescription = $"Unable to create certificate , {Convert.ToString(Request.Params["message"])}, Message:{ex.Message}, details: { stringBuilder.ToString()}";
                            transactionError.DonorId = donorId;
                            transactionError.FullName = "Find name from donor table";
                            transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                            transactionError.TransactionId = Convert.ToString(Request.Params["transaction_id"]);

                            using (var db = new DMSEntitiesNew())
                            {
                                db.ExceptionsLogs.Add(transactionError);
                                db.SaveChanges();
                            }
                            AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "PaymentStatus", "",
                                     string.Format("Unable to create certificate for PledgerId:{0}, IP{1}, transactionId:{2}, Message:{6}, ResponseDesc:{3}, Name: {4}, Amount: {5}",
                                     sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], Request.Params["transaction_id"], Request.Params["message"], "Find donor name from table", Request.Params["auth_amount="], ex.Message));
                        }
                        #endregion


                        AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "PaymentStatus", "",
                              string.Format("Successfully made payment for DonorId: {0}, IP: {1}, transactionId: {2}, ApprovalCode: {3}",
                              donorId, this.Request.ServerVariables["REMOTE_ADDR"], x_trans_id, Bank_Resp_Code));
                        AuditLogger.LogToDB(donor.Email, 0,
                              string.Format("Successfully made payment for DonorId: {0}, IP: {1}, transactionId: {2}, ApprovalCode: {3}",
                              donorId, this.Request.ServerVariables["REMOTE_ADDR"], x_trans_id, Bank_Resp_Code), "");
                        await SendEmailAsync(transaction);
                        //SendEmailAsync(transaction, ControllerContext);
                    }
                }

                else
                {
                    transaction.TransactionId = x_trans_id;
                    transaction.ReferenceNumber = Reference_No;
                    transaction.AuthorizationNumber = Retrieval_Ref_No;
                    transaction.ReasonCode = Bank_Resp_Code;
                    transaction.TransactionStatus = Bank_Message;
                    transaction.Description = x_response_reason_text;
                    transaction.Receipt = exact_ctr;
                    transaction.CardType = TransactionCardType;

                    context.SaveChanges();

                    transactionError = new ExceptionsLog();
                    transactionError.ErrorCode = (int)ErrorCode.TransactionFailed;
                    transactionError.Amount = Convert.ToInt32(Request.Params["x_amount"]);
                    // transactionError.InvoiceId = Convert.ToInt64(invoice.INVOICE_NO);
                    transactionError.ResponseCode = Convert.ToString(Request.Params["Bank_Resp_Code"]);
                    transactionError.ResponseDescription = $"{Convert.ToString(Request.Params["message"])}, details: { stringBuilder.ToString()}";
                    transactionError.DonorId = donorId;
                    transactionError.FullName = "Find name from donor table";
                    transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                    transactionError.TransactionId = Convert.ToString(Request.Params["x_trans_id"]);

                    using (var db = new DMSEntitiesNew())
                    {
                        db.ExceptionsLogs.Add(transactionError);
                        db.SaveChanges();
                    }
                    AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "PaymentStatus", "",
                             string.Format("Failed payment for DonorId:{0}, IP{1}, transactionId:{2}, ResponseDesc:{3}, Name: {4}, Amount: {5}",
                             sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], x_trans_id, Bank_Message, "Find donor name from table", x_amount));
                    AuditLogger.LogToDB(donor.Email, 0,
                             string.Format("Failed payment for DonorId:{0}, IP{1}, transactionId:{2}, ResponseDesc:{3}, Name: {4}, Amount: {5}",
                             sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], x_trans_id, Bank_Message, "Find donor name from table", x_amount), "");

                    CPD.Framework.Core.EmailService.SendEmailHUFUS("giving@habib.edu.pk", CCAdresses, null, "HUFUS Error-2 on payment status page ", $" Transaction Ref: {Reference_No} Transaction  Recipt: { exact_ctr }", null, "giving@hufus.org");
                }
                #endregion

                // to log page access.
                AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "PaymentStatus", "",
                    string.Format("Last block of page, DonorId:{0}, IP{1}, transactionId:{2}, ApprovalCode{3}, Name: {4}, Amount: {5}, DebugMode: {6}",
                    sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], x_trans_id, Bank_Message, "Find donor name from table", x_amount, ConfigurationManager.AppSettings.Get("isDebugMode")).ToLowerInvariant());

                AuditLogger.LogToDB(donor.Email, 0,
                    string.Format("Last block of page, DonorId:{0}, IP{1}, transactionId:{2}, ApprovalCode{3}, Name: {4}, Amount: {5}, DebugMode: {6}",
                    sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], x_trans_id, Bank_Message, "Find donor name from table", x_amount, ConfigurationManager.AppSettings.Get("isDebugMode")).ToLowerInvariant(), "");
                //Logger.Log (Logger.Aplication.HUGiving.GetHashCode(), pInfo.Email, this.Request.ServerVariables["REMOTE_ADDR"], TermFeePages.PaymentStatus.ToString(), "PaymentStatus", "", Logger.GetJsonString(string.Format("InvoiceId={0},AppId={1},EmpId={2},TotalBill={3}", invoice.INVOICE_NO, invoice.APPID, invoice.EMPLID, invoice.TOTAL_BILL)), "");
            }
            catch (Exception ex)
            {
                transactionError = new ExceptionsLog();
                transactionError.ErrorCode = (int)ErrorCode.TransactionFailed;
                transactionError.Amount = Convert.ToInt32(Request.Params["auth_amount="]);
                // transactionError.InvoiceId = Convert.ToInt64(invoice.INVOICE_NO);
                transactionError.ResponseCode = Convert.ToString(Request.Params["reason_code"]);
                transactionError.ResponseDescription = $"{Convert.ToString(Request.Params["message"])}, details: { stringBuilder.ToString()}";
                transactionError.DonorId = donorId;
                transactionError.FullName = "Find name from donor table";
                transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                transactionError.TransactionId = Convert.ToString(Request.Params["transaction_id"]);

                using (var db = new DMSEntitiesNew())
                {
                    db.ExceptionsLogs.Add(transactionError);
                    db.SaveChanges();
                }
                AuditLogger.Log(this.Request.ServerVariables["REMOTE_ADDR"], "PaymentStatus", "",
                         string.Format("Failed payment for DonorId:{0}, IP{1}, transactionId:{2}, ResponseDesc:{3}, Name: {4}, Amount: {5}",
                         sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], Request.Params["transaction_id"], Request.Params["message"], "Find donor name from table", Request.Params["auth_amount="]));

                AuditLogger.LogToDB(donor.Email, 0,
                         string.Format("Failed payment for DonorId:{0}, IP{1}, transactionId:{2}, ResponseDesc:{3}, Name: {4}, Amount: {5}",
                         sessionPledgerId, this.Request.ServerVariables["REMOTE_ADDR"], Request.Params["transaction_id"], Request.Params["message"], "Find donor name from table", Request.Params["auth_amount="]), "");

                string errorMsg = $" Transaction Ref: {reference_number} Transaction  Recipt: { stringBuilder.ToString() }";
                //resourcedevelopment@habib.edu.pk
                CPD.Framework.Core.EmailService.SendEmailHUFUS("giving@habib.edu.pk", CCAdresses, null, "HUFUS Error-1 on payment status page ", errorMsg, null, "giving@hufus.org");
            }
            ViewBag.Flag = flag;
            ViewBag.ReasonCode = Request.Form["Bank_Resp_Code"];
            ViewBag.FirstName = Request.Form["x_first_name"];
            ViewBag.LastName = Request.Form["x_last_name"];
            ViewBag.CellPhone = Request.Form["x_phone"];
            ViewBag.Email = Request.Form["x_email"];
            ViewBag.Message = Request.Form["Bank_Message"];
            ViewBag.Name = $"{Request.Form["x_first_name"]} {Request.Form["x_last_name"]}";
            ViewBag.Amount = Request.Form["x_amount"];

            return View("ThanksYou", null);
        }
        public enum Recognition
        {
            Self = 0,
            Anonymous = 1,
            InNameOf = 2
        }
        public async System.Threading.Tasks.Task SendEmailAsync(Transaction transaction)//(Donor donor, decimal totalAmount)
        {
            try
            {
                await System.Threading.Tasks.Task.Factory.StartNew(() => SendEmail());
            }
            catch (Exception ex)
            {
                AuditLogger.Log(transaction.DonorId.ToString(), "Index", "SendEmail", string.Format(" DonorId:{0}, TransactionId:{1}, Error:{2}, InnerEx:{3}, fail to send email.",
                   transaction.DonorId, transaction.TransactionId, ex.Message, ex.InnerException != null ? ex.InnerException.Message : ""));
                AuditLogger.LogToDB(transaction.DonorId.ToString(), 0, string.Format(" DonorId:{0}, TransactionId:{1}, Error:{2}, InnerEx:{3}, fail to send email.",
                   transaction.DonorId, transaction.TransactionId, ex.Message, ex.InnerException != null ? ex.InnerException.Message : ""), "");
            }
            //return flag;
        }

        static void SendEmail()//(ControllerContext controllerContext)
        {
            //try
            //{
            //sending confirmation email
            string Msgbody = "";
            List<string> CCAdresses = new List<string>();
            List<string> BCCAdresses = new List<string>();
            string strCCList = ConfigurationManager.AppSettings.Get("ccEmail");
            if (!string.IsNullOrEmpty(strCCList))
            {
                var ccList = strCCList.Split(';');
                if (ccList != null)
                {
                    foreach (var ccItem in ccList)
                    {
                        CCAdresses.Add(ccItem);
                    }
                }
            }
            string strBCCList = ConfigurationManager.AppSettings.Get("bccEmail");
            if (!string.IsNullOrEmpty(strBCCList))
            {
                var bccList = strBCCList.Split(';');
                if (bccList != null)
                {
                    foreach (var bccItem in bccList)
                    {
                        BCCAdresses.Add(bccItem);
                    }
                }
            }

            Donor donor = new Donor();
            List<Transaction> transactions = new List<Transaction>();
            using (var db = new DMSEntitiesNew())
            {
                transactions = db.Transactions.Where(t => t.NotificationEmailStatus == 0 && t.ReasonCode == "100").ToList();
            }
            if (transactions != null && transactions.Count() > 0)
            {
                foreach (var transaction in transactions)
                {
                    using (var db = new DMSEntitiesNew())
                    {
                        donor = db.Donors.Where(d => d.Id == transaction.DonorId).ToList().LastOrDefault();
                    }
                    if (donor != null && !string.IsNullOrEmpty(donor.Email))
                    {
                        string Name = $"{ donor.FirstName } {donor.LastName}";
                        string Address = donor.Address;
                        string to = donor.Email;
                        string Plan = "";
                        string TransactionID = transaction.TransactionId;
                        string DateofTransactionID = transaction.DateTime.ToString("dd/MMMM/yyyy");
                        string NatureOfDoation = string.IsNullOrEmpty(transaction.TreatAsZakat) ? "Donation" : "Zakat";
                        if (!string.IsNullOrEmpty(transaction.TransactionType) && transaction.TransactionType.ToLowerInvariant().Replace(" ", "") != "onetime")
                        {
                            Plan = $" {Util.CamelCase(transaction.TransactionType)}";
                        }
                        string Amount = String.Format("{0:n0}", transaction.Amount);

                        if ((ConfigurationManager.AppSettings.Get("isDebugMode")).ToLowerInvariant() == "true")
                        {
                            to = "taha.habib@habib.edu.pk";
                            BCCAdresses = new List<string>();
                        }
                        if (string.IsNullOrEmpty(to))
                        {
                            to = ConfigurationManager.AppSettings.Get("bccEmail");
                        }

                        Msgbody = "<!doctype html> <html>     <head>     <meta charset='utf-8'>     <!-- utf-8 works for most cases -->     <meta name='viewport' content='width=device-width, initial-scale=1.0'>     <!-- Forcing initial-scale shouldn't be necessary -->     <meta http-equiv='X-UA-Compatible' content='IE=edge'>     <!-- Use the latest (edge) version of IE rendering engine -->     <title>EmailTemplate-Responsive</title>     <!-- The title tag shows in email notifications, like Android 4.4. -->     <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->     <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->      <!-- CSS Reset -->     <style type='text/css'> /* What it does: Remove spaces around the email design added by some email clients. */       /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */ html,  body {              margin: 0 !important;         padding: 0 !important;   height: 100% !important;            width: 100% !important; } /* What it does: Stops email clients resizing small text. */ * {           -ms-text-size-adjust: 100%;          -webkit-text-size-adjust: 100%; } /* What it does: Forces Outlook.com to display emails full width. */ .ExternalClass { width: 100%; } /* What is does: Centers email on Android 4.4 */ div[style*='margin: 16px 0'] {             margin: 0 !important; } /* What it does: Stops Outlook from adding extra spacing to tables. */ table,  td {       mso-table-lspace: 0pt !important;         mso-table-rspace: 0pt !important; } /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */ table {            border-spacing: 0 !important;    border-collapse: collapse !important;         table-layout: fixed !important;                margin: 0 auto !important; } table table table {   table-layout: auto; } /* What it does: Uses a better rendering method when resizing images in IE. */ img {                -ms-interpolation-mode: bicubic; } /* What it does: Overrides styles added when Yahoo's auto-senses a link. */ .yshortcuts a {                border-bottom: none !important; } /* What it does: Another work-around for iOS meddling in triggered links. */ a[x-apple-data-detectors] {                color: inherit !important; } </style>      <!-- Progressive Enhancements -->     <style type='text/css'>                 /* What it does: Hover styles for buttons */         .button-td,         .button-a {             transition: all 100ms ease-in;         }         .button-td:hover,         .button-a:hover {             background: #555555 !important;             border-color: #555555 !important;         }          /* Media Queries */         @media screen and (max-width: 600px) {              .email-container {                 width: 100% !important;             }              /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */             .fluid,             .fluid-centered {                 max-width: 100% !important;                 height: auto !important;                 margin-left: auto !important;                 margin-right: auto !important;             }             /* And center justify these ones. */             .fluid-centered {                 margin-left: auto !important;                 margin-right: auto !important;             }              /* What it does: Forces table cells into full-width rows. */             .stack-column,             .stack-column-center {                 display: block !important;                 width: 100% !important;                 max-width: 100% !important;                 direction: ltr !important;             }             /* And center justify these ones. */             .stack-column-center {                 text-align: center !important;             }                     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */             .center-on-narrow {                 text-align: center !important;                 display: block !important;                 margin-left: auto !important;                 margin-right: auto !important;                 float: none !important;             }             table.center-on-narrow {                 display: inline-block !important;             }                         }      </style>     </head>     <body bgcolor='#e0e0e0' width='100%' style='margin: 0;'>     <table bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0' height='100%' width='100%' style='border-collapse:collapse;'>       <tr>         <td><center style='width: 100%;'>                         <!-- Email Header : BEGIN -->             <table align='center' width='600' class='email-container' bgcolor='#ffffff'>             <tr>                 <td style='padding: 20px 0; text-align: center'><img src='http://habib.edu.pk/docs/HUFUSlogo.png' alt='Habib University' border='0'></td>               </tr>           </table>             <!-- Email Header : END -->                         <!-- Email Body : BEGIN -->             <table cellspacing='0' cellpadding='0' border='0' align='center' bgcolor='#ffffff' width='600' class='email-container'>                         <!-- Hero Image, Flush : BEGIN -->             <tr>                 <td class='full-width-image'><img src='https://habib.edu.pk/donate-webapp/thankyou/images/banner.png' width='600' alt='alt_text' border='0' align='center' style='width: 100%; max-width: 600px; height: auto;'></td>               </tr>             <!-- Hero Image, Flush : END -->                         <!-- 1 Column Text : BEGIN -->             <tr>                 <td style='padding: 40px; text-align: left; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;'><strong>Dear " + Name + ",</strong><br> <br>  On behalf of Habib University Foundation U.S. (HUFUS), we would like to extend our deepest gratitude to you for your <strong> " + Plan + " </strong> " + NatureOfDoation + " of USD <strong> " + Amount + "</strong> received on " + DateofTransactionID + " through Transaction ID " + TransactionID + ". Please find enclosed receipt for your donation.<br> <br> Habib University Foundation U.S. is a 501(c)(3) nonprofit organization registered with the Internal Revenue Service. Your contribution is tax - deductible to the extent allowed by law. HUFUS provided no goods or services in exchange for your contribution.<br><br> Best Wishes,<br> Office of Resource Development <br> Habib University Foundation U.S.Inc.<br> Greeley Square Station #20150, 4 East 27th Street, N.Y. 10001-7720<br><!-- WhatsApp:  +1 (203) 444-2696<br>--> <!--T: <a href='tel:+1 (646) 685-3977'>+1 (646) 685-3977 </a> <br>--><br>For more information or queries please write to: <a href = 'mailto:giving@hufus.org'> giving@hufus.org</a><br> <webversion style = 'color:#880D0F'><br>If you wish to unsubscribe from your donation plan please reach out to the <a href = 'mailto:giving@hufus.org'>office of resource development</a></webversion>               </td>               </tr>          <tr>           <td style = 'padding: 20px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888; border-bottom:0px'><webversion style = 'color:#cccccc; text-decoration:none; font-weight: bold;'> Follow Habib University<br> <a href = 'https://www.facebook.com/HabibUniversity'  style = 'text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/004-facebook.png' alt = 'facebook'> </a> &nbsp; <a href = 'https://www.linkedin.com/company/habib-university' style = 'text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/006-linkedin.png' alt = 'linkedIn'> </a> &nbsp;<a href = 'https://instagram.com/habibuniversity' style = 'text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/005-instagram.png' alt = 'instagram'> </a> &nbsp;<a href = 'https://twitter.com/habibuniversity' style = 'text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/003-twitter.png' alt = 'twitter'> </a> &nbsp;<a href = 'https://www.youtube.com/user/HabibUni' style = 'text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/001-youtube.png' alt = 'youtube'> </a><br> <webversion style = 'font-size:10px;line-height:normal;font-weight:normal; color:#666'><!--Habib University Foundation(HUF) is a not -for-profit organization under section 42 of the Companies Act 2017.HUF is also an approved non - profit organization u / s 2(36) of the Income Tax Ordinance, 2001 and any donation to the Foundation qualifies as a charitable donation in terms of section 61 of the Income Tax Ordinance.--></webversion></td></tr>                                                     <!--1 Column Text : BEGIN-->                                                 <!--Two Even Columns : END-->                         <!--Three Even Columns : BEGIN-->                         <!--Three Even Columns : END-->                         <!--Thumbnail Left, Text Right : BEGIN-->                         <!--Thumbnail Left, Text Right : END-->                         <!--Thumbnail Right, Text Left : BEGIN--> </table></center></td></tr></table> </body> </html>";







                        //Msgbody= "<!doctype html> <html>     <head>     <meta charset='utf-8'>     <!-- utf-8 works for most cases -->     <meta name='viewport' content='width=device-width, initial-scale=1.0'>     <!-- Forcing initial-scale shouldn't be necessary -->     <meta http-equiv='X-UA-Compatible' content='IE=edge'>     <!-- Use the latest (edge) version of IE rendering engine -->     <title>EmailTemplate-Responsive</title>     <!-- The title tag shows in email notifications, like Android 4.4. -->     <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->     <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->      <!-- CSS Reset -->     <style type='text/css'> /* What it does: Remove spaces around the email design added by some email clients. */       /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */ html,  body {              margin: 0 !important;         padding: 0 !important;   height: 100% !important;            width: 100% !important; } /* What it does: Stops email clients resizing small text. */ * {           -ms-text-size-adjust: 100%;          -webkit-text-size-adjust: 100%; } /* What it does: Forces Outlook.com to display emails full width. */ .ExternalClass { width: 100%; } /* What is does: Centers email on Android 4.4 */ div[style*='margin: 16px 0'] {             margin: 0 !important; } /* What it does: Stops Outlook from adding extra spacing to tables. */ table,  td {       mso-table-lspace: 0pt !important;         mso-table-rspace: 0pt !important; } /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */ table {            border-spacing: 0 !important;    border-collapse: collapse !important;         table-layout: fixed !important;                margin: 0 auto !important; } table table table {   table-layout: auto; } /* What it does: Uses a better rendering method when resizing images in IE. */ img {                -ms-interpolation-mode: bicubic; } /* What it does: Overrides styles added when Yahoo's auto-senses a link. */ .yshortcuts a {                border-bottom: none !important; } /* What it does: Another work-around for iOS meddling in triggered links. */ a[x-apple-data-detectors] {                color: inherit !important; } </style>      <!-- Progressive Enhancements -->     <style type='text/css'>                 /* What it does: Hover styles for buttons */         .button-td,         .button-a {             transition: all 100ms ease-in;         }         .button-td:hover,         .button-a:hover {             background: #555555 !important;             border-color: #555555 !important;         }          /* Media Queries */         @media screen and (max-width: 600px) {              .email-container {                 width: 100% !important;             }              /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */             .fluid,             .fluid-centered {                 max-width: 100% !important;                 height: auto !important;                 margin-left: auto !important;                 margin-right: auto !important;             }             /* And center justify these ones. */             .fluid-centered {                 margin-left: auto !important;                 margin-right: auto !important;             }              /* What it does: Forces table cells into full-width rows. */             .stack-column,             .stack-column-center {                 display: block !important;                 width: 100% !important;                 max-width: 100% !important;                 direction: ltr !important;             }             /* And center justify these ones. */             .stack-column-center {                 text-align: center !important;             }                     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */             .center-on-narrow {                 text-align: center !important;                 display: block !important;                 margin-left: auto !important;                 margin-right: auto !important;                 float: none !important;             }             table.center-on-narrow {                 display: inline-block !important;             }                         }      </style>     </head>     <body bgcolor='#e0e0e0' width='100%' style='margin: 0;'>     <table bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0' height='100%' width='100%' style='border-collapse:collapse;'>       <tr>         <td><center style='width: 100%;'>                         <!-- Email Header : BEGIN -->             <table align='center' width='600' class='email-container' bgcolor='#ffffff'>             <tr>                 <td style='padding: 20px 0; text-align: center'><img src='http://habib.edu.pk/docs/HUFUSlogo.png' alt='Habib University' border='0'></td>               </tr>           </table>             <!-- Email Header : END -->                         <!-- Email Body : BEGIN -->             <table cellspacing='0' cellpadding='0' border='0' align='center' bgcolor='#ffffff' width='600' class='email-container'>                         <!-- Hero Image, Flush : BEGIN -->             <tr>                 <td class='full-width-image'><img src='https://habib.edu.pk/donate-webapp/thankyou/images/banner.png' width='600' alt='alt_text' border='0' align='center' style='width: 100%; max-width: 600px; height: auto;'></td>               </tr>             <!-- Hero Image, Flush : END -->                         <!-- 1 Column Text : BEGIN -->             <tr>                 <td style='padding: 40px; text-align: justify; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;'>                                                                       <strong>Dear " + Name + ",</strong><br><webversion style = 'color:#3b3b3b; font-size:11px;'>" + Address + "</span><br> <br>  On behalf of Habib University Foundation U.S. (HUFUS), we would like to extend our deepest gratitude to you for your <strong> " + Plan + " </strong> " + NatureOfDoation + " of USD <strong> " + Amount + ".</strong> It is support from community members like yourself that will brighten up the future of deserving and meritorious students to acquire quality higher education at Habib University. <br> <br> Please note that Habib University Foundation U.S. is recognized by the Internal Revenue Service as a Section 501(c)(3) tax - exempt public charity. Donors in United States are eligible to receive an income tax deduction for charitable contributions to the extent permitted by law. HUFUS provides no goods or services in exchange for the contributions. In compliance with Internal Revenue Service regulations, the Board of Directors of Habib University Foundation U.S.maintains control and discretion over the allocation of gifts to Habib University. <br>                 <br>  Thank you once again for your encouragement and support. We will keep you informed about the latest news and activities on campus through your email. You can also visit us online at <a href='https://hufus.org'>www.hufus.org</a> to get more information about the development projects that the university is undertaking. <br><br>We look forward to a deep and lasting relationship with you.<br><br>Best Wishes,<br> Office of Resource Development<br> Habib University Foundation U.S.Inc.<br> Greeley Square Station #20150, 4 East 27th Street, N.Y. 10001-7720<br> WhatsApp:  +1 (203) 444-2696<br> <!--T: <a href='tel:+1 (646) 685-3977'>+1 (646) 685-3977 </a> <br>--><br>                 For more information or queries please write to: <a href = 'mailto:giving@hufus.org'> giving@hufus.org</a><br> <webversion style = 'color:#880D0F'><br>If you wish to unsubscribe from your donation plan please reach out to the <a href = 'mailto:giving@hufus.org'>office of resource development</a></webversion>               </td>               </tr>             <!-- 1 Column Text : BEGIN -->                                                 <!-- Two Even Columns : END -->                         <!-- Three Even Columns : BEGIN -->                         <!-- Three Even Columns : END -->                         <!-- Thumbnail Left, Text Right : BEGIN -->                         <!-- Thumbnail Left, Text Right : END -->                         <!-- Thumbnail Right, Text Left : BEGIN -->                         <!-- Thumbnail Right, Text Left : END -->                       </table>             <!-- Email Body : END -->                         <!-- Email Footer : BEGIN -->             <table align = 'center' width= '600' class='email-container' bgcolor='#ffffff'>             <tr>                 <td style = 'padding: 20px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;'><webversion style='color:#cccccc; text-decoration:none; font-weight: bold;'>                                 Follow Habib University<br>                                                 <a href = 'https://www.facebook.com/HabibUniversity'  style='text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/004-facebook.png' alt='facebook'> </a> &nbsp;                 <a href = 'https://www.linkedin.com/company/habib-university' style='text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/006-linkedin.png' alt='linkedIn'> </a> &nbsp;                 <a href = 'https://instagram.com/habibuniversity' style='text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/005-instagram.png' alt='instagram'> </a> &nbsp;                 <a href = 'https://twitter.com/habibuniversity' style='text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/003-twitter.png' alt='twitter'> </a> &nbsp;                 <a href = 'https://www.youtube.com/user/HabibUni' style='text-decoration: none;'> <img src = 'https://habib.edu.pk/donate-webapp/thankyou/images/001-youtube.png' alt='youtube'> </a><br> <webversion style = 'font-size:10px;line-height:normal;font-weight:normal; color:#666'><!--Habib University Foundation(HUF) is a not-for-profit organization under section 42 of the Companies Act 2017.  HUF is also an approved non-profit organization u/s 2(36) of the Income Tax Ordinance, 2001 and any donation to the Foundation qualifies as a charitable donation in terms of section 61 of the Income Tax Ordinance.--></webversion></td>               </tr>           </table>             <!-- Email Footer : END -->                       </center></td>       </tr>     </table> </body> </html>";
                        //OLD BODY Msgbody = "<!doctype html> <html>     <head>     <meta charset='utf-8'>     <!-- utf-8 works for most cases -->     <meta name='viewport' content='width=device-width, initial-scale=1.0'>     <!-- Forcing initial-scale shouldn't be necessary -->     <meta http-equiv='X-UA-Compatible' content='IE=edge'>     <!-- Use the latest (edge) version of IE rendering engine -->     <title>EmailTemplate-Responsive</title>     <!-- The title tag shows in email notifications, like Android 4.4. -->     <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->     <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->      <!-- CSS Reset -->     <style type='text/css'> /* What it does: Remove spaces around the email design added by some email clients. */       /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */ html,  body { 	margin: 0 !important; 	padding: 0 !important; 	height: 100% !important; 	width: 100% !important; } /* What it does: Stops email clients resizing small text. */ * { 	-ms-text-size-adjust: 100%; 	-webkit-text-size-adjust: 100%; } /* What it does: Forces Outlook.com to display emails full width. */ .ExternalClass { 	width: 100%; } /* What is does: Centers email on Android 4.4 */ div[style*='margin: 16px 0'] { 	margin: 0 !important; } /* What it does: Stops Outlook from adding extra spacing to tables. */ table,  td { 	mso-table-lspace: 0pt !important; 	mso-table-rspace: 0pt !important; } /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */ table { 	border-spacing: 0 !important; 	border-collapse: collapse !important; 	table-layout: fixed !important; 	margin: 0 auto !important; } table table table { 	table-layout: auto; } /* What it does: Uses a better rendering method when resizing images in IE. */ img { 	-ms-interpolation-mode: bicubic; } /* What it does: Overrides styles added when Yahoo's auto-senses a link. */ .yshortcuts a { 	border-bottom: none !important; } /* What it does: Another work-around for iOS meddling in triggered links. */ a[x-apple-data-detectors] { 	color: inherit !important; } </style>      <!-- Progressive Enhancements -->     <style type='text/css'>                 /* What it does: Hover styles for buttons */         .button-td,         .button-a {             transition: all 100ms ease-in;         }         .button-td:hover,         .button-a:hover {             background: #555555 !important;             border-color: #555555 !important;         }          /* Media Queries */         @media screen and (max-width: 600px) {              .email-container {                 width: 100% !important;             }              /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */             .fluid,             .fluid-centered {                 max-width: 100% !important;                 height: auto !important;                 margin-left: auto !important;                 margin-right: auto !important;             }             /* And center justify these ones. */             .fluid-centered {                 margin-left: auto !important;                 margin-right: auto !important;             }              /* What it does: Forces table cells into full-width rows. */             .stack-column,             .stack-column-center {                 display: block !important;                 width: 100% !important;                 max-width: 100% !important;                 direction: ltr !important;             }             /* And center justify these ones. */             .stack-column-center {                 text-align: center !important;             }                     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */             .center-on-narrow {                 text-align: center !important;                 display: block !important;                 margin-left: auto !important;                 margin-right: auto !important;                 float: none !important;             }             table.center-on-narrow {                 display: inline-block !important;             }                         }      </style>     </head>     <body bgcolor='#e0e0e0' width='100%' style='margin: 0;' >     <table bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0' height='100%' width='100%' style='border-collapse:collapse;'>       <tr>         <td><center style='width: 100%;'>                         <!-- Email Header : BEGIN -->             <table align='center' width='600' class='email-container' bgcolor='#ffffff'>             <tr>                 <td style='padding: 20px 0; text-align: center'><img src='https://habib.edu.pk/donate-webapp/thankyou/images/hu_logo.png'  alt='Habib University' border='0'></td>               </tr>           </table>             <!-- Email Header : END -->                         <!-- Email Body : BEGIN -->             <table cellspacing='0' cellpadding='0' border='0' align='center' bgcolor='#ffffff' width='600' class='email-container'>                         <!-- Hero Image, Flush : BEGIN -->             <tr>                 <td class='full-width-image'><img src='https://habib.edu.pk/donate-webapp/thankyou/images/banner.png' width='600' alt='alt_text' border='0' align='center' style='width: 100%; max-width: 600px; height: auto;'></td>               </tr>             <!-- Hero Image, Flush : END -->                         <!-- 1 Column Text : BEGIN -->             <tr>                 <td style='padding: 40px; text-align: justify; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;'> 					<strong>Dear " + Name + ",</strong><br> <br>   On behalf of Habib University Foundation (HUF), we would like to extend our deepest gratitude to you for your<strong>" + Plan + "</strong> " + NatureOfDoation + " of PKR <strong>" + Amount + "</strong>. It is support from community members like yourself that will brighten up the future of deserving and meritorious students to acquire quality higher education at Habib University. Please find enclosed receipt for your donation. <br> <br> Thank you once again for your encouragement and support. Please visit us online at <a href='https://habib.edu.pk'>www.habib.edu.pk</a> to get the latest news on the academic and other development projects that the university is undertaking. <br> <br>   We look forward to a deep and lasting relationship with you. <br>                 <br>                 					Best Wishes,<br> Resource Development Team<br> Habib University<br> Block 18 | University Avenue | Gulistan-e-Jauhar<br> Karachi, Pakistan<br> T: <a href='tel:+92 21 11 10 42242'>+92 21 11 10 42242 (HABIB)</a> | Ext 4444 | <a href='tel:+92 312 2338343'>+92 312 2338343</a><br> For more information or queries please write to: <a href='mailto:giving@habib.edu.pk'>giving@habib.edu.pk</a><br> <webversion style='color:#880D0F'>If you wish to unsubscribe from your donation plan please reach out to the office of resource development</webversion>               </td>               </tr>             <!-- 1 Column Text : BEGIN -->                                                 <!-- Two Even Columns : END -->                         <!-- Three Even Columns : BEGIN -->                         <!-- Three Even Columns : END -->                         <!-- Thumbnail Left, Text Right : BEGIN -->                         <!-- Thumbnail Left, Text Right : END -->                         <!-- Thumbnail Right, Text Left : BEGIN -->                         <!-- Thumbnail Right, Text Left : END -->                       </table>             <!-- Email Body : END -->                         <!-- Email Footer : BEGIN -->             <table align='center' width='600' class='email-container' bgcolor='#ffffff'>             <tr>                 <td style='padding: 20px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;'><webversion style='color:#cccccc; text-decoration:none; font-weight: bold;'>                                 Follow Habib University<br>                                                 <a href='https://www.facebook.com/HabibUniversity'  style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/004-facebook.png' alt='facebook'> </a> &nbsp;                 <a href='https://www.linkedin.com/company/habib-university' style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/006-linkedin.png' alt='linkedIn'> </a> &nbsp;                 <a href='https://instagram.com/habibuniversity' style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/005-instagram.png' alt='instagram'> </a> &nbsp;                 <a href='https://twitter.com/habibuniversity' style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/003-twitter.png' alt='twitter'> </a> &nbsp;                 <a href='https://www.youtube.com/user/HabibUni' style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/001-youtube.png' alt='youtube'> </a><br> <webversion style='font-size:10px;line-height:normal;font-weight:normal; color:#666'>Habib University Foundation (HUF) is a not-for-profit organization under section 42 of the Companies Act 2017.  HUF is also an approved non-profit organization u/s 2(36) of the Income Tax Ordinance, 2001 and any donation to the Foundation qualifies as a charitable donation in terms of section 61 of the Income Tax Ordinance.</webversion></td>               </tr>           </table>             <!-- Email Footer : END -->                       </center></td>       </tr>     </table> </body> </html>";

                        //"<!doctype html> <html>     <head>     <meta charset='utf-8'>     <!-- utf-8 works for most cases -->     <meta name='viewport' content='width=device-width, initial-scale=1.0'>     <!-- Forcing initial-scale shouldn't be necessary -->     <meta http-equiv='X-UA-Compatible' content='IE=edge'>     <!-- Use the latest (edge) version of IE rendering engine -->     <title>EmailTemplate-Responsive</title>     <!-- The title tag shows in email notifications, like Android 4.4. -->     <!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->     <!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->      <!-- CSS Reset -->     <style type='text/css'> /* What it does: Remove spaces around the email design added by some email clients. */       /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */ html,  body { 	margin: 0 !important; 	padding: 0 !important; 	height: 100% !important; 	width: 100% !important; } /* What it does: Stops email clients resizing small text. */ * { 	-ms-text-size-adjust: 100%; 	-webkit-text-size-adjust: 100%; } /* What it does: Forces Outlook.com to display emails full width. */ .ExternalClass { 	width: 100%; } /* What is does: Centers email on Android 4.4 */ div[style*='margin: 16px 0'] { 	margin: 0 !important; } /* What it does: Stops Outlook from adding extra spacing to tables. */ table,  td { 	mso-table-lspace: 0pt !important; 	mso-table-rspace: 0pt !important; } /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */ table { 	border-spacing: 0 !important; 	border-collapse: collapse !important; 	table-layout: fixed !important; 	margin: 0 auto !important; } table table table { 	table-layout: auto; } /* What it does: Uses a better rendering method when resizing images in IE. */ img { 	-ms-interpolation-mode: bicubic; } /* What it does: Overrides styles added when Yahoo's auto-senses a link. */ .yshortcuts a { 	border-bottom: none !important; } /* What it does: Another work-around for iOS meddling in triggered links. */ a[x-apple-data-detectors] { 	color: inherit !important; } </style>      <!-- Progressive Enhancements -->     <style type='text/css'>                 /* What it does: Hover styles for buttons */         .button-td,         .button-a {             transition: all 100ms ease-in;         }         .button-td:hover,         .button-a:hover {             background: #555555 !important;             border-color: #555555 !important;         }          /* Media Queries */         @media screen and (max-width: 600px) {              .email-container {                 width: 100% !important;             }              /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */             .fluid,             .fluid-centered {                 max-width: 100% !important;                 height: auto !important;                 margin-left: auto !important;                 margin-right: auto !important;             }             /* And center justify these ones. */             .fluid-centered {                 margin-left: auto !important;                 margin-right: auto !important;             }              /* What it does: Forces table cells into full-width rows. */             .stack-column,             .stack-column-center {                 display: block !important;                 width: 100% !important;                 max-width: 100% !important;                 direction: ltr !important;             }             /* And center justify these ones. */             .stack-column-center {                 text-align: center !important;             }                     /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */             .center-on-narrow {                 text-align: center !important;                 display: block !important;                 margin-left: auto !important;                 margin-right: auto !important;                 float: none !important;             }             table.center-on-narrow {                 display: inline-block !important;             }                         }      </style>     </head>     <body bgcolor='#e0e0e0' width='100%' style='margin: 0;' >     <table bgcolor='#e0e0e0' cellpadding='0' cellspacing='0' border='0' height='100%' width='100%' style='border-collapse:collapse;'>       <tr>         <td><center style='width: 100%;'>     <!-- Email Header : BEGIN -->             <table align='center' width='600' class='email-container' bgcolor='#ffffff'>             <tr>                 <td style='padding: 20px 0; text-align: center'><img src='https://habib.edu.pk/donate-webapp/thankyou/images/hu_logo.png'  alt='Habib University' border='0'></td>               </tr>           </table>             <!-- Email Header : END -->                         <!-- Email Body : BEGIN -->             <table cellspacing='0' cellpadding='0' border='0' align='center' bgcolor='#ffffff' width='600' class='email-container'>                         <!-- Hero Image, Flush : BEGIN -->             <tr>                 <td class='full-width-image'><img src='https://habib.edu.pk/donate-webapp/thankyou/images/banner.png' width='600' alt='alt_text' border='0' align='center' style='width: 100%; max-width: 600px; height: auto;'></td>               </tr>             <!-- Hero Image, Flush : END -->                         <!-- 1 Column Text : BEGIN -->             <tr>                 <td style='padding: 40px; text-align: justify; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;'> 					<strong>Dear " + Name + ",</strong><br> <br>   On behalf of Habib University Foundation (HUF), we would like to extend our deepest gratitude to you for your contribution. Please find enclosed e-receipt of your online transaction. It is support from community members like yourself that will brighten up the future of deserving and meritorious students to acquire quality higher education at Habib University.<br/> Thank you once again for your encouragement and support. Please visit us online at www.habib.edu.pk to get the latest news on the academic and other development projects that the university is undertaking. We look forward to a deep and lasting relationship with you.<br>                 <br>                              </td>               </tr>                                  </table>             <!-- Email Body : END -->                         <!-- Email Footer : BEGIN -->             <table align='center' width='600' class='email-container' bgcolor='#ffffff'>             <tr>                 <td style='padding: 20px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;'><webversion style='color:#cccccc; text-decoration:none; font-weight: bold;'><a style='color:black; text-decoration: none;' href='tel:+9221111042242'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/001-phone-call.png' alt='phone'> 92 21 1110 42242 (Ext. 4444) </a></webversion><br> <webversion style='color:#cccccc; text-decoration:none; font-weight: bold;'><a style='color:black; text-decoration: none;' href='mailto:giving@habib.edu.pk '> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/002-envelope.png' alt='email'> giving@habib.edu.pk  </a></webversion>                                 <br>                 Follow Habib University<br>                                 <br>                 <a href='https://www.facebook.com/HabibUniversity'  style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/004-facebook.png' alt='facebook'> </a> &nbsp;                 <a href='https://www.linkedin.com/company/habib-university' style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/006-linkedin.png' alt='linkedIn'> </a> &nbsp;                 <a href='https://instagram.com/habibuniversity' style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/005-instagram.png' alt='instagram'> </a> &nbsp;                 <a href='https://twitter.com/habibuniversity' style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/003-twitter.png' alt='twitter'> </a> &nbsp;                 <a href='https://www.youtube.com/user/HabibUni' style='text-decoration: none;'> <img src='https://habib.edu.pk/donate-webapp/thankyou/images/001-youtube.png' alt='youtube'> </a></td>               </tr>           </table>             <!-- Email Footer : END -->                       </center></td>       </tr>     </table> </body> </html>";
                        try
                        {
                            CPD.Framework.Core.EmailService.SendEmailHUFUS(to, CCAdresses, BCCAdresses, "Habib University Foundation U.S. Inc. | Online Payment Confirmation!", Msgbody, transaction.ReceiptPath, "giving@hufus.org", transaction.CertificatePath);

                            using (var db = new DMSEntitiesNew())
                            {
                                var _transactions = db.Transactions.Where(p => p.TransactionId == transaction.TransactionId).ToList();
                                if (_transactions != null && _transactions.Count > 0)
                                {
                                    var _transaction = _transactions.LastOrDefault();
                                    if (_transaction != null)
                                    {
                                        _transaction.NotificationEmailStatus = 1;
                                        db.SaveChanges();
                                    }
                                }
                            }
                            AuditLogger.Log(to, "Home", "SendEmail", string.Format(" Name:{0}, Email:{1}, File{2}, Email sent successfully.",
                                        CamelCase(Name), to, transaction.ReceiptPath));
                        }
                        catch (Exception ex)
                        {
                            ExceptionsLog transactionError = new ExceptionsLog();
                            transactionError.ErrorCode = (int)ErrorCode.TransactionFailed;
                            transactionError.Amount = Convert.ToInt32(donor.TotalAmount);
                            //transactionError.InvoiceId = Convert.ToInt64(invoice.INVOICE_NO);
                            transactionError.ResponseCode = Convert.ToString(transaction.ReasonCode);
                            transactionError.ResponseDescription = $"Unable to send email, edails:{ex.Message}, innerEx: { (ex.InnerException == null ? "" : ex.InnerException.Message)} transaction Ref: {transaction.ReferenceNumber}";
                            transactionError.DonorId = donor.Id;
                            transactionError.FullName = $"{donor.FirstName} {donor.LastName}";
                            transactionError.DateTime = DateTime.UtcNow.AddHours(5);
                            transactionError.TransactionId = transaction.TransactionId;

                            using (var db = new DMSEntitiesNew())
                            {
                                db.ExceptionsLogs.Add(transactionError);
                                db.SaveChanges();
                            }
                            CPD.Framework.Core.EmailService.SendEmailHUFUS("giving@habib.edu.pk", null, BCCAdresses, "HUFUS Error Email Not Sent : Habib University Foundation U.S. Inc. | Online Payment Confirmation!", $"Exception{ ex.Message} Email Body:{ Msgbody}", null, "giving@hufus.org");
                        }
                    }
                }
            }
            //}
            //catch (Exception ex)
            //{
            //    AuditLogger.Log("", "Home", "SendEmail", string.Format(" Name:{0}, DateTime:{1}, Unable to send emails details: {2}, innerEx:{3}.",
            //                       CamelCase(Name), DateTime.UtcNow.AddHours(5), ex.Message, ex.InnerException != null ? ex.InnerException.Message : ""));
            //}
        }

        public static string NumberToWords(int number, bool addOnly = true)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            string words = "";
            if ((number / 1000000000) > 0)
            {
                words += NumberToWords(number / 1000000000, false) + " Billion ";
                number %= 1000000000;
            }

            if ((number / 10000000) > 0)
            {
                words += NumberToWords(number / 10000000, false) + " Crore ";
                number %= 10000000;
            }

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000, false) + " Million ";
                number %= 1000000;
            }


            if ((number / 100000) > 0)
            {
                words += NumberToWords(number / 100000, false) + " Lakh ";
                number %= 100000;
            }


            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000, false) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100, false) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return addOnly ? (words + " only") : words;
        }

        /// <summary>
        /// not in use to be deleted later on
        /// </summary>
        /// <param name="donor"></param>
        /// <param name="totalAmount"></param>
        static void SendEmail(Donor donor, decimal totalAmount)
        {

            //sending confirmation email
            string Msgbody = "";
            List<string> CCAdresses = new List<string>();
            List<string> BCCAdresses = new List<string>();
            string strCCList = ConfigurationManager.AppSettings.Get("ccEmail");
            if (!string.IsNullOrEmpty(strCCList))
            {
                var ccList = strCCList.Split(';');
                if (ccList != null)
                {
                    foreach (var ccItem in ccList)
                    {
                        CCAdresses.Add(ccItem);
                    }
                }
            }
            string strBCCList = ConfigurationManager.AppSettings.Get("bccEmail");
            if (!string.IsNullOrEmpty(strBCCList))
            {
                var bccList = strBCCList.Split(';');
                if (bccList != null)
                {
                    foreach (var bccItem in bccList)
                    {
                        BCCAdresses.Add(bccItem);
                    }
                }
            }
            string Name = $"{ donor.FirstName } {donor.LastName}";
            string to = donor.Email;

            if ((ConfigurationManager.AppSettings.Get("isDebugMode")).ToLowerInvariant() == "true")
            {
                to = "taha.habib@habib.edu.pk";
                CCAdresses = new List<string>();
            }
            if (string.IsNullOrEmpty(to))
            {
                to = ConfigurationManager.AppSettings.Get("bccEmail");
            }


            Msgbody = "Hello World";//"<!DOCTYPE html> <html lang='en' xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'> <head>     <meta charset='utf-8'> <!-- utf-8 works for most cases -->     <meta name='viewport' content='width=device-width'> <!-- Forcing initial-scale shouldn't be necessary -->     <meta http-equiv='X-UA-Compatible' content='IE=edge'> <!-- Use the latest (edge) version of IE rendering engine -->     <meta name='x-apple-disable-message-reformatting'>     <title>Habib University - Pledge</title>       <style>          /* What it does: Remove spaces around the email design added by some email clients. */         /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */         html,         body {             margin: 0 auto !important;             padding: 0 !important;             height: 100% !important;             width: 100% !important;         }          /* What it does: Stops email clients resizing small text. */         * {             -ms-text-size-adjust: 100%;             -webkit-text-size-adjust: 100%;         }          /* What it does: Centers email on Android 4.4 */         div[style*='margin: 16px 0'] {             margin: 0 !important;         }          /* What it does: Stops Outlook from adding extra spacing to tables. */         table,         td {             mso-table-lspace: 0pt !important;             mso-table-rspace: 0pt !important;         }          /* What it does: Fixes webkit padding issue. */         table {             border-spacing: 0 !important;             border-collapse: collapse !important;             table-layout: fixed !important;             margin: 0 auto !important;         }          /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */         a {             text-decoration: none;         }          /* What it does: Uses a better rendering method when resizing images in IE. */         img {             -ms-interpolation-mode:bicubic;         }          /* What it does: A work-around for email clients meddling in triggered links. */         a[x-apple-data-detectors],  /* iOS */         .unstyle-auto-detected-links a,         .aBn {             border-bottom: 0 !important;             cursor: default !important;             color: inherit !important;             text-decoration: none !important;             font-size: inherit !important;             font-family: inherit !important;             font-weight: inherit !important;             line-height: inherit !important;         }          /* What it does: Prevents Gmail from changing the text color in conversation threads. */         .im {             color: inherit !important;         }          /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */         .a6S {            display: none !important;            opacity: 0.01 !important; 		} 		/* If the above doesn't work, add a .g-img class to any image in question. */ 		img.g-img + div { 		   display: none !important; 		}          /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */         /* Create one of these media queries for each additional viewport size you'd like to fix */          /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */         @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {             u ~ div .email-container {                 min-width: 320px !important;             }         }         /* iPhone 6, 6S, 7, 8, and X */         @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {             u ~ div .email-container {                 min-width: 375px !important;             }         }         /* iPhone 6+, 7+, and 8+ */         @media only screen and (min-device-width: 414px) {             u ~ div .email-container {                 min-width: 414px !important;             }         }      </style>     <style>          /* What it does: Hover styles for buttons */         .button-td,         .button-a {             transition: all 100ms ease-in;         } 	    .button-td-primary:hover, 	    .button-a-primary:hover { 	        background: #555555 !important; 	        border-color: #555555 !important; 	    }          /* Media Queries */         @media screen and (max-width: 600px) {              .email-container {                 width: 100% !important;                 margin: auto !important;             }              /* What it does: Forces table cells into full-width rows. */             .stack-column,             .stack-column-center {                 display: block !important;                 width: 100% !important;                 max-width: 100% !important;                 direction: ltr !important;             }             /* And center justify these ones. */             .stack-column-center {                 text-align: center !important;             }              /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */             .center-on-narrow {                 text-align: center !important;                 display: block !important;                 margin-left: auto !important;                 margin-right: auto !important;                 float: none !important;             }             table.center-on-narrow {                 display: inline-block !important;             }              /* What it does: Adjust typography on small screens to improve readability */             .email-container p {                 font-size: 17px !important;             }         }   .t-Details{border: solid thin #5c2568!important;    padding: 10px!important; } .t-header{ margin: 0px!important;     font-weight: bold;                text-align: center!important;                background-color: #5c2568!important;     color: #fff!important;         }  </style> </head> <body width='100%' style='margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f5f5f5;'> 	<center style='width: 100%; background-color: #f5f5f5;'> <table align='center' role='presentation' cellspacing='0' cellpadding='0' border='0' width='600' style='margin: auto;' class='email-container'> 	        <!-- Email Header : BEGIN -->             <tr>                 <td style='padding: 20px 0; text-align: center'>                     <img src='https://donate.habib.edu.pk/Content/HU-HUF.png' width='280' height='83' alt='alt_text' border='0' style='height: auto; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;'>                 </td>             </tr>             <tr>                 <td style='background-color: #ffffff;'>                     <table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>                         <tr>                             <td style='padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;'>                                 <h1 style='margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #5c2568; font-weight: normal;'><strong>Dear " + CamelCase(Name) + ",</strong></h1>                                 <p>Thank you so much for making a contribution to Habib University.<p>We truly appreciate your generosity towards supporting education of talented young women and men of Pakistan.</p><div style='border: solid thin #5c2568!important; padding: 10px!important;'><p style='margin: 0px!important; font-weight: bold; text-align: center!important; background-color: #5c2568!important; color: #fff!important;'>Online Transaction Details</p><p style='margin-bottom:2px;'><span class='t-field' >Amount: </span><span class='t-value'> PKR " + (donor.Amount.HasValue ? donor.Amount.Value : 0) + "</span></p><p style='margin:2px;'><span class='t-field'>Transaction Date/Time: </span><span class='t-value'>" + (donor.TransactionDateTime.HasValue ? donor.TransactionDateTime.Value.ToString("dd/MM/yyyy HH:mm") : "") + "</span></p><p style='margin:2px;'><span class='t-field'>Bank Transaction Id: </span><span class='t-value'>" + donor.TransactionId + "</span></p><p style='margin:2px;'><span class='t-field'>Bank Approval Code: </span><span class='t-value'>" + donor.ApprovalNo + "</span></p></div><br/><p>If you have any questions, please contact Ms. Tatheer Hamdani, President's Chief of Staff and Director of Global Engagement on <a href='mailto:tatheer.hamdani@habib.edu.pk'>tatheer.hamdani@habib.edu.pk</a>. You can also contact Ms. Hamdani via cell number +92-300-3372325 (Pakistan) on WhatsApp.</p> <p>We welcome you as a growing member of our community of Mohsineen.</p><p> Regards, <br>Wasif Rizvi<br/>President, Habib University<br/><a href='mailto:wasif.rizvi@habib.edu.pk'>wasif.rizvi@habib.edu.pk</a></p></td>                         </tr>                                            </table>                 </td>             </tr>             <tr>                 <td valign='middle' > 	                <div> 	                    <table role='presentation' border='0' cellpadding='0' cellspacing='0' width='100%'> 	                        <tr> 	                            <td valign='middle' style='text-align: left; padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #666;'> <p style='margin: 0;'>HUF is a not-for-profit organization under section 42 of the Companies Act 2017. HUF is also an approved non-profit organization u/s 2(36) of the Income Tax Ordinance, 2001 and any donation to the Foundation qualifies as a charitable donation in terms of section 61 of the Income Tax Ordinance.</p> 	                            </td> 	                        </tr> 	                    </table> 	                </div> 	            </td> 	        </tr> 	        <tr> 	            <td style='background-color: #ffffff;'> 	                <table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'> 	                    <tr> 	                        <td style='padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: center;'> 	                            Stay Connected<p> <a href='https://twitter.com/habibuniversity' target='_blank'><img src='http://eapp.azurewebsites.net/Images/twitter.png' width='25' height='20' alt='Twitter' /></a> 	 	<a href='https://www.linkedin.com/company/habib-university' target='_blank'> <img src='http://eapp.azurewebsites.net/Images/linkedin.png' width='25' height='20' alt='Linkedin' /></a> 	 	<a href='https://www.facebook.com/HabibUniversity'><img src='http://eapp.azurewebsites.net/Images/fb.png' width='25' height='20' alt='Facebook' /> </a></p></td> 	                    </tr>  </table> 	            </td> 	        </tr> 	       	    </table>     </center> </body> </html>";
            //"<!DOCTYPE html> <html lang='en' xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'> <head>     <meta charset='utf-8'> <!-- utf-8 works for most cases -->     <meta name='viewport' content='width=device-width'> <!-- Forcing initial-scale shouldn't be necessary -->     <meta http-equiv='X-UA-Compatible' content='IE=edge'> <!-- Use the latest (edge) version of IE rendering engine -->     <meta name='x-apple-disable-message-reformatting'>     <title>Habib University - Pledge</title>       <style>          /* What it does: Remove spaces around the email design added by some email clients. */         /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */         html,         body {             margin: 0 auto !important;             padding: 0 !important;             height: 100% !important;             width: 100% !important;         }          /* What it does: Stops email clients resizing small text. */         * {             -ms-text-size-adjust: 100%;             -webkit-text-size-adjust: 100%;         }          /* What it does: Centers email on Android 4.4 */         div[style*='margin: 16px 0'] {             margin: 0 !important;         }          /* What it does: Stops Outlook from adding extra spacing to tables. */         table,         td {             mso-table-lspace: 0pt !important;             mso-table-rspace: 0pt !important;         }          /* What it does: Fixes webkit padding issue. */         table {             border-spacing: 0 !important;             border-collapse: collapse !important;             table-layout: fixed !important;             margin: 0 auto !important;         }          /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */         a {             text-decoration: none;         }          /* What it does: Uses a better rendering method when resizing images in IE. */         img {             -ms-interpolation-mode:bicubic;         }          /* What it does: A work-around for email clients meddling in triggered links. */         a[x-apple-data-detectors],  /* iOS */         .unstyle-auto-detected-links a,         .aBn {             border-bottom: 0 !important;             cursor: default !important;             color: inherit !important;             text-decoration: none !important;             font-size: inherit !important;             font-family: inherit !important;             font-weight: inherit !important;             line-height: inherit !important;         }          /* What it does: Prevents Gmail from changing the text color in conversation threads. */         .im {             color: inherit !important;         }          /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */         .a6S {            display: none !important;            opacity: 0.01 !important; 		} 		/* If the above doesn't work, add a .g-img class to any image in question. */ 		img.g-img + div { 		   display: none !important; 		}          /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */         /* Create one of these media queries for each additional viewport size you'd like to fix */          /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */         @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {             u ~ div .email-container {                 min-width: 320px !important;             }         }         /* iPhone 6, 6S, 7, 8, and X */         @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {             u ~ div .email-container {                 min-width: 375px !important;             }         }         /* iPhone 6+, 7+, and 8+ */         @media only screen and (min-device-width: 414px) {             u ~ div .email-container {                 min-width: 414px !important;             }         }      </style>     <style>          /* What it does: Hover styles for buttons */         .button-td,         .button-a {             transition: all 100ms ease-in;         } 	    .button-td-primary:hover, 	    .button-a-primary:hover { 	        background: #555555 !important; 	        border-color: #555555 !important; 	    }          /* Media Queries */         @media screen and (max-width: 600px) {              .email-container {                 width: 100% !important;                 margin: auto !important;             }              /* What it does: Forces table cells into full-width rows. */             .stack-column,             .stack-column-center {                 display: block !important;                 width: 100% !important;                 max-width: 100% !important;                 direction: ltr !important;             }             /* And center justify these ones. */             .stack-column-center {                 text-align: center !important;             }              /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */             .center-on-narrow {                 text-align: center !important;                 display: block !important;                 margin-left: auto !important;                 margin-right: auto !important;                 float: none !important;             }             table.center-on-narrow {                 display: inline-block !important;             }              /* What it does: Adjust typography on small screens to improve readability */             .email-container p {                 font-size: 17px !important;             }         }   .t-Details{border: solid thin #5c2568!important;    padding: 10px!important; } .t-header{ margin: 0px!important;     font-weight: bold;                text-align: center!important;                background-color: #5c2568!important;     color: #fff!important;         }  </style> </head> <body width='100%' style='margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f5f5f5;'> 	<center style='width: 100%; background-color: #f5f5f5;'> <table align='center' role='presentation' cellspacing='0' cellpadding='0' border='0' width='600' style='margin: auto;' class='email-container'> 	        <!-- Email Header : BEGIN -->             <tr>                 <td style='padding: 20px 0; text-align: center'>                     <img src='https://donate.habib.edu.pk/Content/HU-HUF.png' width='280' height='83' alt='alt_text' border='0' style='height: auto; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;'>                 </td>             </tr>             <tr>                 <td style='background-color: #ffffff;'>                     <table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'>                         <tr>                             <td style='padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;'>                                 <h1 style='margin: 0 0 10px; font-size: 25px; line-height: 30px; color: #5c2568; font-weight: normal;'><strong>Dear " + CamelCase(Name) + ",</strong></h1>                                 <p>Thank you so much for making  a contribution to Habib University Foundation.<br/><div style='border: solid thin #5c2568!important; padding: 10px!important;'><p style='margin: 0px!important; font-weight: bold; text-align: center!important; background-color: #5c2568!important; color: #fff!important;'>Transaction Details</p><p style='margin-bottom:2px;'><span class='t-field' >Amount: </span><span class='t-value'>" + (pledger.Amount.HasValue ? pledger.Amount.Value : 0) + "</span></p><p style='margin:2px;'><span class='t-field'>Transaction Date/Time: </span><span class='t-value'>" + (pledger.TransactionDateTime.HasValue ? pledger.TransactionDateTime.Value.ToString("dd/MM/yyyy HH:mm") : "") + "</span></p><p style='margin:2px;'><span class='t-field'>Transaction Id: </span><span class='t-value'>" + pledger.TransactionId + "</span></p><p style='margin:2px;'><span class='t-field'>Approval Code: </span><span class='t-value'>" + pledger.ApprovalNo + "</span></p></div><br/>                                   We truly appreciate your  generosity towards supporting education of talented young  women and men of Pakistan. </p>                                 <p>If you have any questions,  please contact Ms. Sukaina Bhagat, Senior Manager, Resource Development on <a href='mailto:sukaina.bhagat@habib.edu.pk'>sukaina.bhagat@habib.edu.pk</a> or call on +92 21 11 10 42242 (HABIB) to talk to our representative.</p>                               <p>We look forward to having you as a growing member of our community of Mohsineen.</p>                                 <p> Regards, <br>Wasif Rizvi<br/>President, Habib University<br/><a href='mailto:wasif.rizvi@habib.edu.pk'>wasif.rizvi@habib.edu.pk</a></p></td>                         </tr>                                            </table>                 </td>             </tr>             <tr>                 <td valign='middle' > 	                <div> 	                    <table role='presentation' border='0' cellpadding='0' cellspacing='0' width='100%'> 	                        <tr> 	                            <td valign='middle' style='text-align: left; padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #666;'> <p style='margin: 0;'>HUF is a not-for-profit organization under section 42 of the Companies Act 2017. HUF is also an approved non-profit organization u/s 2(36) of the Income Tax Ordinance, 2001 and any donation to the Foundation qualifies as a charitable donation in terms of section 61 of the Income Tax Ordinance.</p> 	                            </td> 	                        </tr> 	                    </table> 	                </div> 	            </td> 	        </tr> 	        <tr> 	            <td style='background-color: #ffffff;'> 	                <table role='presentation' cellspacing='0' cellpadding='0' border='0' width='100%'> 	                    <tr> 	                        <td style='padding: 20px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555; text-align: center;'> 	                            Stay Connected<p> <a href='https://twitter.com/habibuniversity' target='_blank'><img src='http://eapp.azurewebsites.net/Images/twitter.png' width='25' height='20' alt='Twitter' /></a> 	 	<a href='https://www.linkedin.com/company/habib-university' target='_blank'> <img src='http://eapp.azurewebsites.net/Images/linkedin.png' width='25' height='20' alt='Linkedin' /></a> 	 	<a href='https://www.facebook.com/HabibUniversity'><img src='http://eapp.azurewebsites.net/Images/fb.png' width='25' height='20' alt='Facebook' /> </a></p></td> 	                    </tr>  </table> 	            </td> 	        </tr> 	       	    </table>     </center> </body> </html>";
            CPD.Framework.Core.EmailService.SendEmailHUFUS(to, CCAdresses, BCCAdresses, "Habib University Foundation U.S. Inc. | Online Payment Confirmation!", Msgbody, null, "giving@hufus.org");

            AuditLogger.Log(to, "Index", "SendEmail", string.Format(" Name:{0}, Email:{1}, Email sent successfully.",
                    CamelCase(Name), to));
        }


        /*
      public ActionResult ThankYou(int id = 0, int amount = 0)
      {
          ViewBag.Id = id;
          Donation donation = new Donation() { Id = id, Donation1 = amount };
          return View(donation);
      }
      */
        #endregion
        //public ActionResult ViewToPDF()
        //{
        //    TransactionReceipt transactionReceipt = new TransactionReceipt()
        //    {
        //        Id = 1,
        //        ReceiptNo = "123456",
        //        Amount = "100,000",
        //        AmountInFigure = "One lac rupees only",
        //        Date = "10/01/2021",
        //        FullName = "Naseer Ahmed",
        //        NatureOfDoation = "Zakat",
        //        NationalTexNo = "3017275-6",
        //        TransactionId = "T122222223432"
        //    };
        //    return PartialView(transactionReceipt);
        //}
        public bool GenerateReceipt(TransactionReceipt transactionReceipt)
        {
            string strAdmitCard = RenderViewToString(ControllerContext, "ViewToPDF", transactionReceipt);

            return true;

        }
        public static string RenderViewToString(ControllerContext context, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = context.RouteData.GetRequiredString("action");

            ViewDataDictionary viewData = new ViewDataDictionary(model);
            string stringW = null;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                ViewContext viewContext = new ViewContext(context, viewResult.View, viewData, new TempDataDictionary(), sw);
                viewResult.View.Render(viewContext, sw);
                stringW = sw.GetStringBuilder().ToString();
                //return sw.GetStringBuilder().ToString();
            }
            return stringW;
        }
        public enum Category
        {
            visionaries = 1,
            benefactors = 2,
            supporters = 3,
            fellow = 4,
        }

        ////public static JsonResult GetDonorCategory(string firstName, string lastName, string cellPhone, string email)
        //public static HBL_Donors GetDonorCategory(string firstName, string lastName, string cellPhone, string email)
        //{
        //    bool flag = false;
        //    HBL_Donors _donor = new HBL_Donors();

        //    List<HBL_Donors> donors = new List<HBL_Donors>();
        //    List<HBL_Donors> supporters = new List<HBL_Donors>();
        //    List<HBL_Donors> benefactors = new List<HBL_Donors>();
        //    List<HBL_Donors> visionaries = new List<HBL_Donors>();
        //    List<HBL_Donors> fellows = new List<HBL_Donors>();
        //    List<HBL_Donors> tempDonors = new List<HBL_Donors>();
        //    List<HBL_Transactions> transactions = new List<HBL_Transactions>();

        //    bool isSupportor = false;
        //    bool isBenefactor = false;
        //    bool isVisionary = false;
        //    bool isFellow = false;
        //    string category = "fellow";

        //    if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(cellPhone) && !string.IsNullOrEmpty(email))
        //    {
        //        firstName = firstName.ToLowerInvariant();
        //        lastName = lastName.ToLowerInvariant();

        //        using (var db = new DMSEntitiesNew())
        //        {
        //            var donorsVM = db.VM_GetDonations.ToList();
        //            if (donorsVM != null)
        //            {

        //                var filteredDonors = donorsVM.Where(d => d.FIRSTNAME.ToLowerInvariant() == firstName && d.LastName.ToLowerInvariant() == lastName && d.CellNumber == cellPhone && d.Email == email).ToList();
        //                if (filteredDonors != null)
        //                {
        //                    foreach (var donorItem in filteredDonors)
        //                    {
        //                        if (donorItem.Category.ToLowerInvariant() == Category.visionaries.ToString().ToLowerInvariant())
        //                        {
        //                            isVisionary = true;
        //                            category = "Visionary";
        //                        }
        //                        else if (donorItem.Category.ToLowerInvariant() == Category.benefactors.ToString().ToLowerInvariant() && isVisionary == false)
        //                        {
        //                            isBenefactor = true;
        //                            category = "Benefactor";
        //                        }
        //                        else if (donorItem.Category.ToLowerInvariant() == Category.supporters.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false)
        //                        {
        //                            isSupportor = true;
        //                            category = "Supporter";
        //                        }
        //                        else if (donorItem.Category.ToLowerInvariant() == Category.fellow.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false && isSupportor == false)
        //                        {
        //                            isFellow = true;
        //                            category = "Fellow";
        //                        }
        //                    }
        //                    if (filteredDonors != null)
        //                    {
        //                        var currentDonor = filteredDonors.LastOrDefault();

        //                        _donor.CellNumber = currentDonor.CellNumber;
        //                        _donor.FirstName = currentDonor.FIRSTNAME;
        //                        _donor.LastName = currentDonor.LastName;
        //                        _donor.Email = currentDonor.Email;
        //                        _donor.TotalAmount = currentDonor.Amount;
        //                        _donor.Organization = currentDonor.Organization ?? "";
        //                        _donor.Type = category;
        //                        _donor.RecurringDonationFrequency = currentDonor.RecurringDonationFrequency;
        //                    }
        //                }

        //            }
        //        }

        //    }

        //    flag = true;
        //    return _donor;
        //    //return Json(new { Success = flag, Data = _donor }, JsonRequestBehavior.AllowGet);
        //}
        /// <summary>
        /// 23 march 2021
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public JsonResult Donors(decimal min, decimal max, int category, string name)
        {
            bool flag = false;
            List<Donor> donors = new List<Donor>();

            List<Donor> supporters = new List<Donor>();
            List<Donor> benefactors = new List<Donor>();
            List<Donor> visionaries = new List<Donor>();
            List<Donor> fellows = new List<Donor>();
            List<Donor> tempDonors = new List<Donor>();
            List<Transaction> transactions = new List<Transaction>();

            using (var db = new DMSEntitiesNew())
            {
                var donorList = db.VM_GetDonations.ToList();

                foreach (var donor in donorList)
                {
                    var currentDonor = donor;
                    bool isSupportor = false;
                    bool isBenefactor = false;
                    bool isVisionary = false;
                    bool isFellow = false;
                    bool isSecondItem = false;
                    foreach (var donorItem in donorList)
                    {
                        if (
                            currentDonor.Name == donorItem.Name &&
                            currentDonor.Email == donorItem.Email &&
                            currentDonor.CellNumber == donorItem.CellNumber)
                        {
                            if (currentDonor.Category.ToLowerInvariant() == Category.visionaries.ToString().ToLowerInvariant())
                            {
                                isVisionary = true;
                            }
                            else if (currentDonor.Category.ToLowerInvariant() == Category.benefactors.ToString().ToLowerInvariant() && isVisionary == false)
                            {
                                isBenefactor = true;
                            }
                            else if (currentDonor.Category.ToLowerInvariant() == Category.supporters.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false)
                            {
                                isSupportor = true;
                            }
                            else if (currentDonor.Category.ToLowerInvariant() == Category.fellow.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false && isSupportor == false)
                            {
                                isFellow = true;
                            }
                            if (isSecondItem)
                                currentDonor.Amount += donorItem.Amount;
                            isSecondItem = true;
                        }
                    }

                    Donor _donor = new Donor();
                    _donor.CellNumber = currentDonor.CellNumber;
                    _donor.FirstName = currentDonor.Name;
                    // _donor.LastName = currentDonor.LastName;
                    _donor.Email = currentDonor.Email;
                    _donor.TotalAmount = currentDonor.Amount;
                    _donor.Organization = currentDonor.Organization ?? "";
                    _donor.Type = currentDonor.Category;
                    _donor.RecurringDonationFrequency = currentDonor.RecurringDonationFrequency;

                    if (isVisionary)
                    {
                        if (visionaries.Count() == 0 || !visionaries.Exists(v => v.FirstName == _donor.FirstName && v.LastName == _donor.LastName && v.Email == _donor.Email && v.CellNumber == _donor.CellNumber))
                            visionaries.Add(_donor);
                    }
                    else if (isBenefactor)
                    {
                        if (benefactors.Count() == 0 || !benefactors.Exists(v => v.FirstName == _donor.FirstName && v.LastName == _donor.LastName && v.Email == _donor.Email && v.CellNumber == _donor.CellNumber))
                            benefactors.Add(_donor);
                    }
                    else if (isSupportor)
                    {
                        if (supporters.Count() == 0 || !supporters.Exists(v => v.FirstName == _donor.FirstName && v.LastName == _donor.LastName && v.Email == _donor.Email && v.CellNumber == _donor.CellNumber))
                            supporters.Add(_donor);
                    }
                    else if (isFellow)
                    {
                        if (fellows.Count() == 0 || !fellows.Exists(v => v.FirstName == _donor.FirstName && v.LastName == _donor.LastName && v.Email == _donor.Email && v.CellNumber == _donor.CellNumber))
                            fellows.Add(_donor);
                    }
                }
            }
            if (visionaries != null && visionaries.Count() > 0)
            {
                foreach (var visionary in visionaries)
                {

                    //check duplicates in benefacotrs, remove benefactor from visionaries
                    foreach (var benefactor in benefactors)
                    {
                        if (visionary.FirstName == benefactor.FirstName &&
                          visionary.LastName == benefactor.LastName &&
                          visionary.Email == benefactor.Email &&
                          visionary.CellNumber == benefactor.CellNumber)
                        {

                            visionary.TotalAmount += benefactor.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(benefactor);
                        }
                    }
                    benefactors = tempDonors;
                    tempDonors = new List<Donor>();

                    //check duplicates in supports, remove supports from visionaries
                    foreach (var supporter in supporters)
                    {
                        if (visionary.FirstName == supporter.FirstName &&
                          visionary.LastName == supporter.LastName &&
                          visionary.Email == supporter.Email &&
                          visionary.CellNumber == supporter.CellNumber)
                        {

                            visionary.TotalAmount += supporter.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(supporter);
                        }
                    }
                    supporters = tempDonors;
                    tempDonors = new List<Donor>();

                    //check duplicates in fellows, remove fellows from visionaries
                    foreach (var fellow in fellows)
                    {
                        if (visionary.FirstName == fellow.FirstName &&
                          visionary.LastName == fellow.LastName &&
                          visionary.Email == fellow.Email &&
                          visionary.CellNumber == fellow.CellNumber)
                        {

                            visionary.TotalAmount += fellow.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(fellow);
                        }
                    }
                    fellows = tempDonors;
                    tempDonors = new List<Donor>();
                }
            }
            if (benefactors != null && benefactors.Count() > 0)
            {
                foreach (var benefactor in benefactors)
                {
                    //check duplicates in supports, remove supports from visionaries
                    foreach (var supporter in supporters)
                    {
                        if (benefactor.FirstName == supporter.FirstName &&
                          benefactor.LastName == supporter.LastName &&
                          benefactor.Email == supporter.Email &&
                          benefactor.CellNumber == supporter.CellNumber)
                        {

                            benefactor.TotalAmount += supporter.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(supporter);
                        }
                    }
                    supporters = tempDonors;
                    tempDonors = new List<Donor>();

                    //check duplicates in fellows, remove fellows from visionaries
                    foreach (var fellow in fellows)
                    {
                        if (benefactor.FirstName == fellow.FirstName &&
                          benefactor.LastName == fellow.LastName &&
                          benefactor.Email == fellow.Email &&
                          benefactor.CellNumber == fellow.CellNumber)
                        {

                            benefactor.TotalAmount += fellow.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(fellow);
                        }
                    }
                    fellows = tempDonors;
                    tempDonors = new List<Donor>();
                }
            }
            if (supporters != null && supporters.Count() > 0)
            {
                foreach (var supporter in supporters)
                {
                    //check duplicates in fellows, remove fellows from visionaries
                    foreach (var fellow in fellows)
                    {
                        if (supporter.FirstName == fellow.FirstName &&
                          supporter.LastName == fellow.LastName &&
                          supporter.Email == fellow.Email &&
                          supporter.CellNumber == fellow.CellNumber)
                        {

                            supporter.TotalAmount += fellow.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(fellow);
                        }
                    }
                    fellows = tempDonors;
                    tempDonors = new List<Donor>();
                }
            }
            if (!string.IsNullOrEmpty(name))
            {
                name = name.ToLowerInvariant();
                if (category == Category.visionaries.GetHashCode())
                {
                    var finedDonorsByFName = visionaries.Where(d => ((d.FirstName.ToLowerInvariant().Trim()).Contains(name.Trim())) || d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim()) || (d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim()))).ToList();
                    //var finedDonorsByLName = visionaries.Where(d => d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim())).ToList();
                    //var finedDonorsByOrg = visionaries.Where(d => d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim())).ToList();
                    donors.AddRange(finedDonorsByFName);
                    //donors.AddRange(finedDonorsByLName);
                    //donors.AddRange(finedDonorsByOrg);
                }
                else if (category == Category.benefactors.GetHashCode())
                {
                    var finedDonorsByFName = benefactors.Where(d => ((d.FirstName.ToLowerInvariant().Trim()).Contains(name.Trim())) || d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim()) || (d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim()))).ToList();
                    //var finedDonorsByLName = benefactors.Where(d => d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim())).ToList();
                    //var finedDonorsByOrg = benefactors.Where(d => d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim())).ToList();
                    donors.AddRange(finedDonorsByFName);
                    //donors.AddRange(finedDonorsByLName);
                    //donors.AddRange(finedDonorsByOrg);
                }
                else if (category == Category.fellow.GetHashCode())
                {
                    var finedDonorsByName = fellows.Where(d => ((d.FirstName.ToLowerInvariant().Trim()).Contains(name.Trim())) || d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim()) || (d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim()))).ToList();
                    donors.AddRange(finedDonorsByName);
                }
                else if (category == Category.supporters.GetHashCode())
                {

                    var finedDonorsByFName = supporters.Where(d => ((d.FirstName.ToLowerInvariant().Trim()).Contains(name.Trim())) || d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim()) || (d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim()))).ToList();
                    //var finedDonorsByLName = supporters.Where(d => d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim())).ToList();
                    //var finedDonorsByOrg = supporters.Where(d => d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim())).ToList();
                    donors.AddRange(finedDonorsByFName);
                    //donors.AddRange(finedDonorsByLName);
                    //donors.AddRange(finedDonorsByOrg);
                }
            }
            else
            {
                if (category == Category.visionaries.GetHashCode())
                    donors.AddRange(visionaries);
                else if (category == Category.benefactors.GetHashCode())
                    donors.AddRange(benefactors);
                else if (category == Category.fellow.GetHashCode())
                    donors.AddRange(fellows);
                else if (category == Category.supporters.GetHashCode())
                    donors.AddRange(supporters);
            }


            flag = true;
            donors = donors.OrderByDescending(r => r.Id).ToList();
            return Json(new { Success = flag, Data = donors }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get donors all categories, no donors if name not passed or not matched
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Donors(string name)
        {
            bool flag = false;
            List<Donor> donors = new List<Donor>();

            List<Donor> supporters = new List<Donor>();
            List<Donor> benefactors = new List<Donor>();
            List<Donor> visionaries = new List<Donor>();
            List<Donor> fellows = new List<Donor>();
            List<Donor> tempDonors = new List<Donor>();
            List<Transaction> transactions = new List<Transaction>();

            using (var db = new DMSEntitiesNew())
            {
                var donorList = db.VM_GetDonations.ToList();

                foreach (var donor in donorList)
                {
                    var currentDonor = donor;
                    bool isSupportor = false;
                    bool isBenefactor = false;
                    bool isVisionary = false;
                    bool isFellow = false;
                    bool isSecondItem = false;
                    foreach (var donorItem in donorList)
                    {
                        if (
                            currentDonor.Name == donorItem.Name &&
                            currentDonor.Email == donorItem.Email &&
                            currentDonor.CellNumber == donorItem.CellNumber)
                        {
                            if (currentDonor.Category.ToLowerInvariant() == Category.visionaries.ToString().ToLowerInvariant())
                            {
                                isVisionary = true;
                            }
                            else if (currentDonor.Category.ToLowerInvariant() == Category.benefactors.ToString().ToLowerInvariant() && isVisionary == false)
                            {
                                isBenefactor = true;
                            }
                            else if (currentDonor.Category.ToLowerInvariant() == Category.supporters.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false)
                            {
                                isSupportor = true;
                            }
                            else if (currentDonor.Category.ToLowerInvariant() == Category.fellow.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false && isSupportor == false)
                            {
                                isFellow = true;
                            }
                            if (isSecondItem)
                                currentDonor.Amount += donorItem.Amount;
                            isSecondItem = true;
                        }
                    }

                    Donor _donor = new Donor();
                    _donor.CellNumber = currentDonor.CellNumber;
                    _donor.FirstName = currentDonor.Name;
                    // _donor.LastName = currentDonor.LastName;
                    _donor.Email = currentDonor.Email;
                    _donor.TotalAmount = currentDonor.Amount;
                    _donor.Organization = currentDonor.Organization ?? "";
                    _donor.Type = currentDonor.Category;
                    _donor.RecurringDonationFrequency = currentDonor.RecurringDonationFrequency;

                    if (isVisionary)
                    {
                        if (visionaries.Count() == 0 || !visionaries.Exists(v => v.FirstName == _donor.FirstName && v.LastName == _donor.LastName && v.Email == _donor.Email && v.CellNumber == _donor.CellNumber))
                        {
                            _donor.Type = "Visionary";
                            visionaries.Add(_donor);
                        }
                    }
                    else if (isBenefactor)
                    {
                        if (benefactors.Count() == 0 || !benefactors.Exists(v => v.FirstName == _donor.FirstName && v.LastName == _donor.LastName && v.Email == _donor.Email && v.CellNumber == _donor.CellNumber))
                        {
                            _donor.Type = "Benefactor";
                            benefactors.Add(_donor);
                        }
                    }
                    else if (isSupportor)
                    {
                        if (supporters.Count() == 0 || !supporters.Exists(v => v.FirstName == _donor.FirstName && v.LastName == _donor.LastName && v.Email == _donor.Email && v.CellNumber == _donor.CellNumber))
                        {
                            _donor.Type = "Supporter";
                            supporters.Add(_donor);
                        }
                    }
                    else if (isFellow)
                    {
                        if (fellows.Count() == 0 || !fellows.Exists(v => v.FirstName == _donor.FirstName && v.LastName == _donor.LastName && v.Email == _donor.Email && v.CellNumber == _donor.CellNumber))
                        {
                            _donor.Type = "Fellow";
                            fellows.Add(_donor);
                        }
                    }
                }
            }
            if (visionaries != null && visionaries.Count() > 0)
            {
                foreach (var visionary in visionaries)
                {

                    //check duplicates in benefacotrs, remove benefactor from visionaries
                    foreach (var benefactor in benefactors)
                    {
                        if (visionary.FirstName == benefactor.FirstName &&
                          visionary.LastName == benefactor.LastName &&
                          visionary.Email == benefactor.Email &&
                          visionary.CellNumber == benefactor.CellNumber)
                        {

                            visionary.TotalAmount += benefactor.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(benefactor);
                        }
                    }
                    benefactors = tempDonors;
                    tempDonors = new List<Donor>();

                    //check duplicates in supports, remove supports from visionaries
                    foreach (var supporter in supporters)
                    {
                        if (visionary.FirstName == supporter.FirstName &&
                          visionary.LastName == supporter.LastName &&
                          visionary.Email == supporter.Email &&
                          visionary.CellNumber == supporter.CellNumber)
                        {

                            visionary.TotalAmount += supporter.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(supporter);
                        }
                    }
                    supporters = tempDonors;
                    tempDonors = new List<Donor>();

                    //check duplicates in fellows, remove fellows from visionaries
                    foreach (var fellow in fellows)
                    {
                        if (visionary.FirstName == fellow.FirstName &&
                          visionary.LastName == fellow.LastName &&
                          visionary.Email == fellow.Email &&
                          visionary.CellNumber == fellow.CellNumber)
                        {

                            visionary.TotalAmount += fellow.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(fellow);
                        }
                    }
                    fellows = tempDonors;
                    tempDonors = new List<Donor>();
                }
            }
            if (benefactors != null && benefactors.Count() > 0)
            {
                foreach (var benefactor in benefactors)
                {
                    //check duplicates in supports, remove supports from visionaries
                    foreach (var supporter in supporters)
                    {
                        if (benefactor.FirstName == supporter.FirstName &&
                          benefactor.LastName == supporter.LastName &&
                          benefactor.Email == supporter.Email &&
                          benefactor.CellNumber == supporter.CellNumber)
                        {

                            benefactor.TotalAmount += supporter.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(supporter);
                        }
                    }
                    supporters = tempDonors;
                    tempDonors = new List<Donor>();

                    //check duplicates in fellows, remove fellows from visionaries
                    foreach (var fellow in fellows)
                    {
                        if (benefactor.FirstName == fellow.FirstName &&
                          benefactor.LastName == fellow.LastName &&
                          benefactor.Email == fellow.Email &&
                          benefactor.CellNumber == fellow.CellNumber)
                        {

                            benefactor.TotalAmount += fellow.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(fellow);
                        }
                    }
                    fellows = tempDonors;
                    tempDonors = new List<Donor>();
                }
            }
            if (supporters != null && supporters.Count() > 0)
            {
                foreach (var supporter in supporters)
                {
                    //check duplicates in fellows, remove fellows from visionaries
                    foreach (var fellow in fellows)
                    {
                        if (supporter.FirstName == fellow.FirstName &&
                          supporter.LastName == fellow.LastName &&
                          supporter.Email == fellow.Email &&
                          supporter.CellNumber == fellow.CellNumber)
                        {

                            supporter.TotalAmount += fellow.TotalAmount;
                        }
                        else
                        {
                            tempDonors.Add(fellow);
                        }
                    }
                    fellows = tempDonors;
                    tempDonors = new List<Donor>();
                }
            }
            if (!string.IsNullOrEmpty(name))
            {
                name = name.ToLowerInvariant();

                var finedDonorsByFName = visionaries.Where(d => ((d.FirstName.ToLowerInvariant().Trim()).Contains(name.Trim())) || d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim()) || (d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim()))).ToList();
                donors.AddRange(finedDonorsByFName);

                finedDonorsByFName = benefactors.Where(d => ((d.FirstName.ToLowerInvariant().Trim()).Contains(name.Trim())) || d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim()) || (d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim()))).ToList();
                donors.AddRange(finedDonorsByFName);

                var finedDonorsByName = fellows.Where(d => ((d.FirstName.ToLowerInvariant().Trim()).Contains(name.Trim())) || d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim()) || (d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim()))).ToList();
                donors.AddRange(finedDonorsByName);

                finedDonorsByFName = supporters.Where(d => ((d.FirstName.ToLowerInvariant().Trim()).Contains(name.Trim())) || d.LastName != null && (d.LastName.ToLowerInvariant().Trim()).Contains(name.Trim()) || (d.Organization != null && (d.Organization.ToLowerInvariant().Trim()).Contains(name.Trim()))).ToList();
                donors.AddRange(finedDonorsByFName);

            }

            flag = true;
            donors = donors.OrderByDescending(r => r.Id).ToList();
            return Json(new { Success = flag, Data = donors }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Report()
        {
            List<DONATION_VIEW> donationReport = new List<DONATION_VIEW>();//List<DonationReport> donationReport = new List<DonationReport>();
            using (var db = new DMSEntitiesNew())
            {
                donationReport = db.DONATION_VIEW.ToList();
            }
            ViewBag.Message = "Your application description page.";

            return View(donationReport);
        }

        public static bool SaveAdmitCardPDF(string admitCard, string fileName, string cssFile)
        {
            //Create a byte array that will eventually hold our final PDF
            Byte[] bytes;

            //Boilerplate iTextSharp setup here
            //Create a stream that we can write to, in this case a MemoryStream
            using (var ms = new MemoryStream())
            {

                //Create an iTextSharp Document which is an abstraction of a PDF but **NOT** a PDF
                var doc = new Document();
                {
                    //Create a writer that's bound to our PDF abstraction and our stream
                    var writer = PdfWriter.GetInstance(doc, ms);
                    {
                        //Open the document for writing
                        doc.Open();

                        //Our sample HTML and CSS
                        var example_html = admitCard; //@"<p>This <em>is </em><span class=""headline"" style=""text-decoration: underline;"">some</span> <strong>sample <em> text</em></strong><span style=""color: red;"">!!!</span></p>";
                        var example_css = cssFile;//".schedule_table td { padding: 5px;} body {font-family: Calibri; color: #000; font-size: 15px; } h1 {font-family: Times New Roman;font-size: 23px; line-height: 1px;color: #000;} h2 {font-size: 20px; font-weight: bold; color: #000; line-height: 18px; margin: 0 0 3px 0;}   h3 {color: #000; font-size: 18px;line-height: 20px; margin: 0;} ";// @".headline{font-size:200%}";

                        ///**************************************************
                        // * Example #1                                     *
                        // *                                                *
                        // * Use the built-in HTMLWorker to parse the HTML. *
                        // * Only inline CSS is supported.                  *
                        // * ************************************************/

                        ////Create a new HTMLWorker bound to our document
                        //var htmlWorker = new iTextSharp.text.html.simpleparser.HTMLWorker(doc);
                        //{

                        //    //HTMLWorker doesn't read a string directly but instead needs a TextReader (which StringReader subclasses)
                        //    using (var sr = new StringReader(example_html))
                        //    {

                        //        //Parse the HTML
                        //        htmlWorker.Parse(sr);
                        //    }
                        //}

                        ///**************************************************
                        // * Example #2                                     *
                        // *                                                *
                        // * Use the XMLWorker to parse the HTML.           *
                        // * Only inline CSS and absolutely linked          *
                        // * CSS is supported                               *
                        // * ************************************************/

                        //XMLWorker also reads from a TextReader and not directly from a string
                        //using (var srHtml = new StringReader(example_html))
                        //{

                        //    //Parse the HTML

                        //  // iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, srHtml);
                        //}

                        ///**************************************************
                        // * Example #3                                     *
                        // *                                                *
                        // * Use the XMLWorker to parse HTML and CSS        *
                        // * ***********************************************/

                        ////In order to read CSS as a string we need to switch to a different constructor
                        ////that takes Streams instead of TextReaders.
                        ////Below we convert the strings into UTF8 byte array and wrap those in MemoryStreams
                        using (var msCss = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(example_css)))
                        {
                            using (var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(example_html)))
                            {

                                //Parse the HTML
                                iTextSharp.tool.xml.XMLWorkerHelper.GetInstance().ParseXHtml(writer, doc, msHtml, msCss);
                            }
                        }


                        doc.Close();
                    }
                }

                //After all of the PDF "stuff" above is done and closed but **before** we
                //close the MemoryStream, grab all of the active bytes from the stream
                bytes = ms.ToArray();
            }

            //Now we just need to do something with those bytes.
            //Here I'm writing them to disk but if you were in ASP.Net you might Response.BinaryWrite() them.
            //You could also write the bytes to a database in a varbinary() column (but please don't) or you
            //could pass them to another function for further PDF processing.
            //var testFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "test.pdf");
            System.IO.File.WriteAllBytes(fileName, bytes);

            return true;
        }


        //[HttpPost]
        //public JsonResult getpaymentstatus()
        //{
        //    string flag;
        //    IDictionary<string, string> parameters = new Dictionary<string, string>();
        //    foreach (string property in Request.Form.AllKeys)
        //    {
        //        parameters.Add(property, Request.Form[property]);
        //    }
        //    string x_trans_id = parameters["x_trans_id"].ToString();
        //    int x_invoice_num = int.Parse(parameters["x_invoice_num"].ToString());
        //    string x_response_reason_text = parameters["x_response_reason_text"].ToString();
        //    string x_auth_code = parameters["x_auth_code"].ToString();
        //    string x_amount = parameters["x_amount"].ToString();
        //    if (Request.Form.AllKeys.Contains("x_recurring_billing_id"))
        //    { string x_recurring_billing_id = parameters["x_recurring_billing_id"].ToString(); }
        //    string x_currency_code = parameters["x_currency_code"].ToString();
        //    string TransactionCardType = parameters["TransactionCardType"].ToString();
        //    string Retrieval_Ref_No = parameters["Retrieval_Ref_No"].ToString();
        //    string Bank_Resp_Code = parameters["Bank_Resp_Code"].ToString();
        //    string Bank_Message = parameters["Bank_Message"].ToString();
        //    string Reference_No = parameters["Reference_No"].ToString();
        //    string exact_ctr = parameters["exact_ctr"].ToString();

        //    DMSEntitiesNew context = new DMSEntitiesNew();
        //    Transaction transaction = (from t in context.Transactions
        //                               where t.DonorId == x_invoice_num
        //                               select t).SingleOrDefault();

        //    transaction.TransactionId = x_trans_id;
        //    transaction.ReferenceNumber = Reference_No;
        //    transaction.AuthorizationNumber = Retrieval_Ref_No;
        //    transaction.ReasonCode = Bank_Resp_Code;
        //    transaction.TransactionStatus = Bank_Message;
        //    transaction.Description = x_response_reason_text;
        //    transaction.Receipt = exact_ctr;
        //    transaction.CardType = TransactionCardType;

        //    context.SaveChanges();


        //    if (Bank_Resp_Code == "100")
        //    {
        //        flag = "success" + Bank_Message;
        //    }
        //    else
        //    {
        //        flag = "Failed" + Bank_Message;
        //    }
        //    return Json(flag, JsonRequestBehavior.AllowGet);
        //}

        /// <summary>
        /// Pass donor name and its category name, category name must be same as of view name 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="category">view name for certificate</param>
        /// <returns></returns>
        public string GenerateCertificate(string name, string category)
        {
            var path = Server.MapPath("/Receipts" + "//" + DateTime.UtcNow.Ticks.ToString() + "_Certificate.pdf");
            var viewString = RenderViewToString(ControllerContext, category, name);

            using (MemoryStream htmlSource = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(viewString)))
            using (FileStream pdfDest = System.IO.File.Open(path, FileMode.OpenOrCreate))
            {
                ConverterProperties converterProperties = new ConverterProperties();
                HtmlConverter.ConvertToPdf(htmlSource, pdfDest, converterProperties);
            }

            return path;
        }
        public static Donor GetDonorCategory(string firstName, string lastName, string cellPhone, string email, bool naseer = true)
        {
            bool flag = false;
            Donor _donor = new Donor();

            List<Donor> donors = new List<Donor>();
            List<Donor> supporters = new List<Donor>();
            List<Donor> benefactors = new List<Donor>();
            List<Donor> visionaries = new List<Donor>();
            List<Donor> fellows = new List<Donor>();
            List<Donor> tempDonors = new List<Donor>();
            List<Transaction> transactions = new List<Transaction>();

            bool isSupportor = false;
            bool isBenefactor = false;
            bool isVisionary = false;
            bool isFellow = false;
            string category = "fellow";


            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(cellPhone) && !string.IsNullOrEmpty(email))
            {
                string Name = firstName + " " + lastName;

                using (var db = new DMSEntitiesNew())
                {
                    var donorsVM = db.VM_GetDonations.ToList();
                    if (donorsVM != null)
                    {

                        var filteredDonors = donorsVM.Where(d => d.Name == Name && d.CellNumber == cellPhone && d.Email == email).ToList();
                        if (filteredDonors != null)
                        {
                            foreach (var donorItem in filteredDonors)
                            {
                                if (donorItem.Category.ToLowerInvariant() == Category.visionaries.ToString().ToLowerInvariant())
                                {
                                    isVisionary = true;
                                    category = "Visionary";
                                }
                                else if (donorItem.Category.ToLowerInvariant() == Category.benefactors.ToString().ToLowerInvariant() && isVisionary == false)
                                {
                                    isBenefactor = true;
                                    category = "Benefactor";
                                }
                                else if (donorItem.Category.ToLowerInvariant() == Category.supporters.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false)
                                {
                                    isSupportor = true;
                                    category = "Supporter";
                                }
                                else if (donorItem.Category.ToLowerInvariant() == Category.fellow.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false && isSupportor == false)
                                {
                                    isFellow = true;
                                    category = "Fellow";
                                }
                            }
                            if (filteredDonors != null)
                            {
                                var currentDonor = filteredDonors.LastOrDefault();

                                _donor.CellNumber = currentDonor.CellNumber;
                                _donor.FirstName = firstName;
                                _donor.LastName = lastName;
                                _donor.Email = currentDonor.Email;
                                _donor.TotalAmount = currentDonor.Amount;
                                _donor.Organization = currentDonor.Organization ?? "";
                                _donor.Type = category;
                                _donor.RecurringDonationFrequency = currentDonor.RecurringDonationFrequency;
                            }
                        }

                    }
                }

            }

            flag = true;
            return _donor;
            //return Json(new { Success = flag, Data = _donor }, JsonRequestBehavior.AllowGet);
        }
        public static Donor GetDonorCategory(string firstName, string lastName, string cellPhone, string email)
        {
            bool flag = false;
            Donor _donor = new Donor();

            List<Donor> donors = new List<Donor>();
            List<Donor> supporters = new List<Donor>();
            List<Donor> benefactors = new List<Donor>();
            List<Donor> visionaries = new List<Donor>();
            List<Donor> fellows = new List<Donor>();
            List<Donor> tempDonors = new List<Donor>();
            List<Transaction> transactions = new List<Transaction>();
            helper help = new helper();
            bool isSupportor = false;
            bool isBenefactor = false;
            bool isVisionary = false;
            bool isFellow = false;
            string category = "fellow";


            if (!string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(cellPhone) && !string.IsNullOrEmpty(email))
            {
                string Name = firstName + " " + lastName;
                List<VM_GetDonations> filteredDonor = new List<VM_GetDonations>();
                VM_GetDonations DonorItem = new VM_GetDonations();
                using (var db = new DMSEntitiesNew())
                {
                    //VM_GetDonations donorsVM = db.VM_GetDonations.ToList();
                    // if (donorsVM != null)
                    {
                        List<VM_GetDonations> allDonors = new List<VM_GetDonations>();
                        DataTable filteredDonorsList = help.SelectDBPSCS("SELECT *  FROM [dbo].[VM_GetDonations] WHERE Name ='" + Name + "' AND CellNumber='" + cellPhone + "' AND Email='" + email + "'");//donorsVM.Where(d => d.Name == Name && d.CellNumber == cellPhone && d.Email == email).ToList();
                        if (filteredDonorsList != null)
                        {
                            for (int i = 0; i < filteredDonorsList.Rows.Count; i++)
                            {

                                DonorItem.Id = 0;
                                DonorItem.Name = filteredDonorsList.Rows[i][1].ToString();//item[1].ToString();
                                DonorItem.Email = filteredDonorsList.Rows[i][2].ToString();
                                DonorItem.CellNumber = filteredDonorsList.Rows[i][3].ToString();
                                DonorItem.RecurringDonationFrequency = filteredDonorsList.Rows[i][4].ToString();
                                DonorItem.Organization = filteredDonorsList.Rows[i][5].ToString();
                                DonorItem.Amount = Convert.ToDecimal(filteredDonorsList.Rows[i][6].ToString());
                                DonorItem.Category = filteredDonorsList.Rows[i][7].ToString();
                                allDonors.Add(DonorItem);
                            }
                            //filteredDonor = allDonors.Where(d => d.Name == Name && d.CellNumber == cellPhone && d.Email == email).ToList();
                            //foreach (var item in filteredDonorsList) {
                            //    VM_GetDonations filteredDonor = new VM_GetDonations();
                            //    filteredDonor.Id = 0;
                            //    filteredDonor.Name = item[1].ToString();
                            //    filteredDonor.Email = item[2].ToString();
                            //    filteredDonor.CellNumber = item[3].ToString();
                            //    filteredDonor.RecurringDonationFrequency= item[4].ToString();
                            //    filteredDonor.Organization= item[5].ToString();
                            //    filteredDonor.Amount = Convert.ToDecimal(item[6].ToString());
                            //    filteredDonor.Category = item[7].ToString();
                            //    filteredDonors.Add(filteredDonor);
                            //}
                            foreach (var donorItem in allDonors)
                            {

                                if (donorItem.Category.ToLowerInvariant() == Category.visionaries.ToString().ToLowerInvariant())
                                {
                                    isVisionary = true;
                                    category = "Visionary";
                                }
                                else if (donorItem.Category.ToLowerInvariant() == Category.benefactors.ToString().ToLowerInvariant() && isVisionary == false)
                                {
                                    isBenefactor = true;
                                    category = "Benefactor";
                                }
                                else if (donorItem.Category.ToLowerInvariant() == Category.supporters.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false)
                                {
                                    isSupportor = true;
                                    category = "Supporter";
                                }
                                else if (donorItem.Category.ToLowerInvariant() == Category.fellow.ToString().ToLowerInvariant() && isVisionary == false && isBenefactor == false && isSupportor == false)
                                {
                                    isFellow = true;
                                    category = "Fellow";
                                }
                            }
                            if (allDonors != null)
                            {
                                var currentDonor = allDonors.LastOrDefault();

                                _donor.CellNumber = currentDonor.CellNumber;
                                _donor.FirstName = firstName;
                                _donor.LastName = lastName;
                                _donor.Email = currentDonor.Email;
                                _donor.TotalAmount = currentDonor.Amount;
                                _donor.Organization = currentDonor.Organization ?? "";
                                _donor.Type = category;
                                _donor.RecurringDonationFrequency = currentDonor.RecurringDonationFrequency;
                            }
                        }

                    }
                }

            }

            flag = true;
            return _donor;
            //return Json(new { Success = flag, Data = _donor }, JsonRequestBehavior.AllowGet);
        }
    }
}