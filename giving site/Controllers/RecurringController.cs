﻿using DonorManagementApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DonorManagementApp.Controllers
{
    public class RecurringController : Controller
    {
        // GET: Recurring
        public ActionResult Index()
        {
            Transaction hbl_transactions = new Transaction();
            HBLRecurringTransactionRequest hblRecurringTransactionRequest = new HBLRecurringTransactionRequest();
            Donor hBL_Donors = new Donor();
            using (var db = new DMSEntitiesNew())
            {
                hbl_transactions = db.Transactions.Where(t => t.SubscriptionId != null).ToList().LastOrDefault();
                hBL_Donors = db.Donors.Where(d => d.Id == hbl_transactions.DonorId).ToList().LastOrDefault();
            }
            if (hbl_transactions != null && hBL_Donors != null) {
                hblRecurringTransactionRequest.payment_token = hbl_transactions.SubscriptionId;
                hblRecurringTransactionRequest.recurring_amount = hbl_transactions.Amount.ToString();
                //hblRecurringTransactionRequest.recurring_frequency = hbl_transactions.TransactionType.ToLowerInvariant();
                //hblRecurringTransactionRequest.recurring_start_date = DateTime.UtcNow.AddHours(5).ToString("yyyyMMdd");
                hblRecurringTransactionRequest.consumer_id = hBL_Donors.Id.ToString();

                //hblRecurringTransactionRequest.bill_to_phone = hBL_Donors.CellNumber;
                hblRecurringTransactionRequest.bill_to_forename = hBL_Donors.FirstName;
                hblRecurringTransactionRequest.bill_to_surname = hBL_Donors.LastName;
                hblRecurringTransactionRequest.bill_to_email = hBL_Donors.Email;
            }
            return View(hblRecurringTransactionRequest);
        }
    }
}