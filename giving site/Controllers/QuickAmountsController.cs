﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DonorManagementApp;

namespace DonorManagementApp.Controllers
{
    public class QuickAmountsController : Controller
    {
        private DMSEntitiesNew db = new DMSEntitiesNew();

        // GET: QuickAmounts
        public async Task<ActionResult> Index()
        {
            return View(await db.QuickAmounts.ToListAsync());
        }

        // GET: QuickAmounts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuickAmount quickAmount = await db.QuickAmounts.FindAsync(id);
            if (quickAmount == null)
            {
                return HttpNotFound();
            }
            return View(quickAmount);
        }

        // GET: QuickAmounts/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QuickAmounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Text,Value,Indexing,Visibility,CreatedOn,GroupId")] QuickAmount quickAmount)
        {
            if (ModelState.IsValid)
            {
                db.QuickAmounts.Add(quickAmount);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(quickAmount);
        }

        // GET: QuickAmounts/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuickAmount quickAmount = await db.QuickAmounts.FindAsync(id);
            if (quickAmount == null)
            {
                return HttpNotFound();
            }
            return View(quickAmount);
        }

        // POST: QuickAmounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Text,Value,Indexing,Visibility,CreatedOn,GroupId")] QuickAmount quickAmount)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quickAmount).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(quickAmount);
        }

        // GET: QuickAmounts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuickAmount quickAmount = await db.QuickAmounts.FindAsync(id);
            if (quickAmount == null)
            {
                return HttpNotFound();
            }
            return View(quickAmount);
        }

        // POST: QuickAmounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            QuickAmount quickAmount = await db.QuickAmounts.FindAsync(id);
            db.QuickAmounts.Remove(quickAmount);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
