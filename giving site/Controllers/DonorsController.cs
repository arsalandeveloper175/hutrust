﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DonorManagementApp;

namespace DonorManagementApp.Controllers
{
    public class DonorsController : Controller
    {
        private DMSEntitiesNew db = new DMSEntitiesNew();

        // GET: Donors
        public async Task<ActionResult> Index()
        {
            return View(await db.Donors.ToListAsync());
        }

        // GET: Donors
        public async Task<ActionResult> Profile()
        {
            return PartialView(await db.Donors.ToListAsync());
        }

        // GET: Donors/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donor donor = await db.Donors.FindAsync(id);
            if (donor == null)
            {
                return HttpNotFound();
            }
            return View(donor);
        }

        // GET: Donors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Donors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,FirstName,LastName,CellNumber,Email,Password,Organization,Type,TypeId,Currency,Recognition,RecurringDonationStatus,RecurringDonationAmount,RecurringDonationFrequency,RecurringTransactionEnabledDateTime,RecurringDonationStartDate,RecurringDonationEndDate,LastRecurringTransactionDateTime,NextRecurringTransactionDateTime,CreatedOn,UpdatedOn,TotalAmount")] Donor donor)
        {
            if (ModelState.IsValid)
            {
                db.Donors.Add(donor);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(donor);
        }

        // GET: Donors/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donor donor = await db.Donors.FindAsync(id);
            if (donor == null)
            {
                return HttpNotFound();
            }
            return View(donor);
        }

        // POST: Donors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,FirstName,LastName,CellNumber,Email,Password,Organization,Type,TypeId,Currency,Recognition,RecurringDonationStatus,RecurringDonationAmount,RecurringDonationFrequency,RecurringTransactionEnabledDateTime,RecurringDonationStartDate,RecurringDonationEndDate,LastRecurringTransactionDateTime,NextRecurringTransactionDateTime,CreatedOn,UpdatedOn")] Donor donor)
        {
            if (ModelState.IsValid)
            {
                donor.EnableRecurringDonationNotificationStatus = 0;
                donor.EnableRecurringDonationNotificationSentOn = DateTime.UtcNow.AddHours(5);
                donor.DisableRecurringDonationNotificationStatus = 0;
                donor.DisableRecurringDonationNotificationSentOn = null;
                donor.LastRecurringTransactionDateTime = DateTime.UtcNow.AddHours(5);
                donor.NextRecurringTransactionDateTime = DateTime.UtcNow.AddHours(5);

                db.Entry(donor).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(donor);
        }

        // GET: Donors/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Donor donor = await db.Donors.FindAsync(id);
            if (donor == null)
            {
                return HttpNotFound();
            }
            return View(donor);
        }

        // POST: Donors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Donor donor = await db.Donors.FindAsync(id);
            db.Donors.Remove(donor);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
