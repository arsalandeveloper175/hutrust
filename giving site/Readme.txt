﻿

#1: When enable recurring doantion set "EnableRecurringDonationNotificationStatus" to 1 
	and "RecurringDonationStatus" to 1 in email notification service check if "EnableRecurringDonationNotificationStatus" is 1 
	than send notification email and update "EnableRecurringDonationNotificationStatus" to 0 and added date time of notification 
	in "EnableRecurringDonationNotificationSentOn" field

#2: When disable recurring doantion set "DisableRecurringDonationNotificationStatus" to 1 
	and "RecurringDonationStatus" to 0 in email notification service check if "DisableRecurringDonationNotificationStatus" is 1 
	than send notification email and update "DisableRecurringDonationNotificationStatus" to 0 and added date time of notification 
	in "DisableRecurringDonationNotificationSentOn" field

