﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DonorManagementApp.Models
{
    public class TransactionReceipt
    {
        public int Id { get; set; }
        public string ReceiptNo { get; set; }
        public string NationalTexNo { get; set; }
        public string Date { get; set; }
        public string FullName { get; set; }
        public string Amount { get; set; }
        public string AmountInFigure { get; set; }
        public string NatureOfDoation { get; set; }
        public string TransactionId { get; set; }
        public string Plan { get; set; }
        public string NextTransactionDate { get; set; }
        public string ContributionRecognition { get; set; }
        public string Organization { get; set; }
        public string Address { get; set; }
    }
}