﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace DonorManagementApp.Models
{
    public class Security
    {
        private const String SECRET_KEY = "b5ff1299c7d541d3bf68adb533012ebaeb1a859707604ce4b07ca3e14b976dcc649d4dae6a1647508a60ddb2da9ec4928e666b5e545e4bd5a974ef58cb4e18bd259e6c368e0248b79805184f5474a75c407f312571d8498c91adbb0832e2281c4b80535f54ff4398b7c9a22f24f67a903984adb9304a49698611f108a7ec27bb";//test 
        public static String sign(IDictionary<string, string> paramsArray)
        {
            return sign(buildDataToSign(paramsArray), SECRET_KEY);
        }

        private static String sign(String data, String secretKey)
        {
            UTF8Encoding encoding = new System.Text.UTF8Encoding();
            byte[] keyByte = encoding.GetBytes(secretKey);

            HMACSHA256 hmacsha256 = new HMACSHA256(keyByte);
            byte[] messageBytes = encoding.GetBytes(data);
            return Convert.ToBase64String(hmacsha256.ComputeHash(messageBytes));
        }

        private static String buildDataToSign(IDictionary<string, string> paramsArray)
        {
            String[] signedFieldNames = paramsArray["signed_field_names"].Split(',');
            IList<string> dataToSign = new List<string>();

            foreach (String signedFieldName in signedFieldNames)
            {
                try
                {
                    dataToSign.Add(signedFieldName + "=" + paramsArray[signedFieldName]);
                }
                catch (Exception ex) { }
            }

            return commaSeparate(dataToSign);
        }

        private static String commaSeparate(IList<string> dataToSign)
        {
            return String.Join(",", dataToSign);
        }
    }
}