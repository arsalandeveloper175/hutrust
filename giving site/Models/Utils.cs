﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;

namespace DonorManagementApp.Models
{
    public class Util
    {
        public static string OrderId = "OrderId";
        public static string TId = "TId";
        public static string Name = "PledgerName";
        public static string Email = "PledgerEmail";
        public static string Amount = "PledgeAmount";
        public static string TransactionDateTime = "TransactionDateTime";
        public static string CampaignId = "HUSelectedCampaignId";
        /// <summary>
        /// Utility class which holds various static methods to Log user activities on Application.
        /// <remarks>Created by Naseer Ahmed Dated: Feb 16, 2016</remarks>
        /// </summary>
        public class AuditLogger
        {
            /// <summary>
            ///Asynchronously Log user activities on pages to txt file.
            /// later should be moved to DB
            /// </summary>
            /// <param name="email"></param>
            /// <param name="page"></param>
            /// <param name="action"></param>
            /// <param name="data"></param>
            public static void Log(string email, string page, string action, string data)
            {
                Task.Factory.StartNew(() => WriteLog(email, page, action, data));
            }
            public static void LogToDB(string userId, int transactionId, string action, string data)
            {
                Task.Factory.StartNew(() => WriteDBLog(userId, transactionId, action, data));
            }
            static void WriteLog(string email, string page, string action, string data)
            {
                Trace.WriteLine(
                     string.Format(
                         "\n-------------------------------------------------------------------\n {0}, {1}: page access by {2} for action {3}.\n Data:{4}"
                     , DateTime.UtcNow.AddHours(5), page, email, action, data));
                Trace.Flush();
            }

            static void WriteDBLog(string userId, int transactionId, string action, string description)
            {
                using (var DBLog = new DMSEntitiesNew())
                {
                    DBLog.Logs.Add(new Log()
                    {
                        UserId = userId,
                        TransactionId = transactionId,
                        Action = action,
                        Description = description,
                        DateTime = DateTime.UtcNow.AddHours(5)
                    });
                    DBLog.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Utility class which holds various static methods to asynchronously Log Exception of Application.
        /// <remarks>Created by Naseer Ahmed Dated: Feb 16, 2016</remarks>
        /// </summary>
        public class ExceptionLogger
        {
            /// <summary>
            /// Log user Exception on pages to txt file.
            /// later should be moved to DB
            /// </summary>
            /// <param name="email"></param>
            /// <param name="page"></param>
            /// <param name="action"></param>
            /// <param name="date"></param>
            public static void Log(string email, string page, string message, string detail, string innerMsg, string stactTrace)
            {
                Task.Factory.StartNew(() => WriteLog(email, page, message, detail, innerMsg, stactTrace));
            }

            static void WriteLog(string email, string page, string message, string detail, string innerMsg, string stactTrace)
            {
                Trace.WriteLine(
                    string.Format(
                        "\n-------------------------------------------------------------------\n{0}, {1}: page access by {2}, exception Msg: {3}.\n Detail:{4}\n Inner Msg:{5}\nStackTrace:{6}"
                    , DateTime.UtcNow.AddHours(5), page, email, message, detail, innerMsg, stactTrace));
                Trace.Flush();
            }
        }
        
        public static bool validateAccess(string email, Pages page)
        {
            bool result = false;
            switch (email.ToLowerInvariant())
            {
                case "naseer.ahmed@habib.edu.pk":
                case "naseer.ahmed":
                case "waqar.naqvi@habib.edu.pk":
                case "waqar.naqvi":
                case "wasif.rizvi@habib.edu.pk":
                case "wasif.rizvi":
                case "shahnoor.sultan@habib.edu.pk":
                case "shahnoor.sultan":
                case "tatheer.hamdani@habib.edu.pk":
                case "tatheer.hamdani":
                case "sukaina.bhagat@habib.edu.pk":
                case "sukaina.bhagat":
                    {
                        switch (page)
                        {
                            case Pages.ListPledges:
                                {
                                    result = true;
                                }
                                break;
                            case Pages.ListCampaignPledges:
                                {
                                    result = true;
                                }
                                break;
                            default:
                                result = false;
                                break;
                        }
                        break;
                    }

                default:
                    result = false;
                    break;
            }

            return result;
        }
        public enum Pages
        {
            ListPledges = 1,
            ListCampaignPledges = 2,
            CampaignScale = 3
        }

        public static void WriteCSV<T>(IEnumerable<T> items, string path)
        {
            Type itemType = typeof(T);
            var props = itemType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            //                    .OrderBy(p => p.Name);

            using (var writer = new StreamWriter(path))
            {

                writer.WriteLine(string.Join(", ", props.Select(p => p.Name)));

                foreach (var item in items)
                {
                    writer.WriteLine(string.Join(", ", props.Select(p => p.GetValue(item, null))));
                }
            }
        }

        public static string CamelCase(string s)
        {
            var x = s.Replace("_", "");
            if (x.Length == 0) return "Null";
            x = System.Text.RegularExpressions.Regex.Replace(x, "([A-Z])([A-Z]+)($|[A-Z])",
                m => m.Groups[1].Value + m.Groups[2].Value.ToLower() + m.Groups[3].Value);
            return char.ToUpper(x[0]) + x.Substring(1);
        }
        /// <summary>
        /// UBL gateway error codes
        /// </summary>
        public enum ErrorCode
        {
            GeneralException = 1,
            TransactionFailed = 2,
            ErrorInEpaymenAPI = 3,
            UploadVoucherPageError = 4,
            UndertakingPageError = 5
        }

        public enum Recognition
        {
            Self = 0,
            Anonymous = 1,
            InNameOf = 2

        }
    }
    public class WebConstants
    {
        public const string HU_Current_User = "HUCurrentUser";
        public const string HU_Current_User_Info = "HUCurrentUserInfo";
        public const string InvalidAccess = "Invalid Access";

    }
}