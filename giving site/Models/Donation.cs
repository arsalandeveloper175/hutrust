﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace DonorManagementApp.Models
{
    public class Donation
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CellNumber { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Organization { get; set; }
        public string TreatAsZakat { get; set; }
        public decimal Amount { get; set; }
        public string Plan { get; set; }
        public string IP { get; set; }
        public int Recognition { get; set; }
        public string InTheNameOf { get; set; }
        public string HearFrom { get; set; }
        public string HearFromField { get; set; }
    }
    public class HBLTransactionRequest
    {
        public string access_key { get; set; } = "3f5ce24117083524866b613e46c6db5b";//test 
        public string profile_id { get; set; } = "4B3414B9-6DBB-4F92-AEF5-CE40FCC95C11";//test 
        public string transaction_uuid { get; set; } = System.Guid.NewGuid().ToString();

        #region fields for recurring token
        public string consumer_id { get; set; }
        //public string recurring_frequency { get; set; }
        //public string recurring_amount { get; set; }
        //public string recurring_start_date { get; set; }
        #endregion
        #region Merchant Defined Data Fields
        public string merchant_defined_data1 { get; set; } = "WC";
        public string merchant_defined_data2 { get; set; } = "Donation";//"Yes";
        //public string merchant_defined_data3 { get; set; } = "Donation";
        //public string merchant_defined_data20 { get; set; } = "NO";
        #endregion
        //for recurring transactions
        public string signed_field_names { get; set; } = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,merchant_defined_data1,merchant_defined_data2,consumer_id";
        // for normal transaction
        //public string signed_field_names { get; set; } = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,merchant_defined_data1";

        public string unsigned_field_names { get; set; } = "card_type,card_number,card_expiry_date";
        public string signed_date_time { get; set; } = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
        public string locale { get; set; } = "en";

        public string transaction_type { get; set; } = "sale";//"authorization";
        public string reference_number { get; set; } = (DateTime.UtcNow.Ticks.ToString());//any unique no like invoice id
        public string amount { get; set; } = "500";
        public string currency { get; set; } = "PKR";
        public string payment_method { get; set; } = "card";
        public string bill_to_forename { get; set; } = "John";//first name
        public string bill_to_surname { get; set; } = "Doe";//last name
        public string bill_to_email { get; set; } = "null@cybersource.com";//last name
        public string bill_to_phone { get; set; } = "02890888888";//last name
        public string bill_to_address_line1 { get; set; } = "Gulistan e jauhar";//last name
        public string bill_to_address_city { get; set; } = "Karachi";//last name
        public string bill_to_address_state { get; set; } = "SN";//last name
        public string bill_to_address_country { get; set; } = "PK";//last name
        public string bill_to_address_postal_code { get; set; } = "75290";//last name
        //public string customer_ip_address { get; set; }// = "10.100.0.87";//last name

        public string signature { get; set; } = "";//last name
        public string GetSignature(IDictionary<string, string> parameters)
        {
            if (parameters != null)
            {
                this.signature = Security.sign(parameters);
            }
            return this.signature;
        }
        public IDictionary<string, string> GetProperties()
        {
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            PropertyInfo[] properties = typeof(HBLTransactionRequest).GetProperties();

            foreach (PropertyInfo property in properties)
            {
                try
                {
                    parameters.Add(property.Name, property.GetValue(this).ToString());
                }
                catch (Exception ex) { }
            }
            return parameters;

        }
    }
    public class FirstdataCreateTokenTransactionRequest
    {
        public string x_login { get; set; } = ConfigurationManager.AppSettings["x_login"].ToString(); 
        public double x_fp_sequence { get; set; }
        public string x_currency_code { get; set; }
        public string x_type { get; set; }

        public string x_first_name { get; set; }
        public string x_last_name { get; set; }
        public string x_phone { get; set; }
        public string x_email { get; set; }
        public string x_company { get; set; }
        public string x_invoice_num { get; set; }

        public string x_recurring_billing { get; set; }
        public string x_recurring_billing_id { get; set; }
        public string x_recurring_billing_amount { get; set; }
        public string x_recurring_billing_start_date { get; set; }
        public string x_recurring_billing_end_date { get; set; }

        public string x_address { get; set; }

        public string x_amount { get; set; }
        public string x_fp_hash { get; set; }
        public Int32 x_fp_timestamp { get; set; }

        public string x_merchant_email { get; set; } = ConfigurationManager.AppSettings["x_merchant_email"].ToString();

        public List<string> GetSignature()
        {
            List<string> temp = new List<string>();
            temp = GetHmac(x_login, x_amount);

            return temp;
        }
        public IDictionary<string, string> GetProperties()
        {
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            PropertyInfo[] properties = typeof(FirstdataCreateTokenTransactionRequest).GetProperties();

            foreach (PropertyInfo property in properties)
            {
                try
                {
                    parameters.Add(property.Name, property.GetValue(this).ToString());
                }
                catch (Exception ex) { }
            }
            return parameters;

        }
        public List<string> GetHmac(string x_login, string x_amount)
        {
            string key = "eAa71qshOWKtDPMSAHyy";//production eAa71qshOWKtDPMSAHyy           test AFu41ZEnaGGu0WMwGg7_
            List<string> Hmac = new List<string>();
            Random rand = new Random();
            x_fp_sequence = rand.Next(1000, 100000) + 123456;

            Hmac.Add(x_fp_sequence.ToString());//"x_fp_sequence"

            x_fp_timestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

            Hmac.Add(x_fp_timestamp.ToString());//"x_fp_timestamp"
            string hmac_data = x_login + "^" + x_fp_sequence + "^" + x_fp_timestamp + "^" + x_amount + "^" + "USD";
            x_fp_hash = HashHmac(hmac_data, key);

            Hmac.Add(x_fp_hash);//"x_fp_hash"

            return Hmac;
        }
        private static string HashHmac(string message, string key)
        {
            Encoding encoding = Encoding.UTF8;
            using (HMACSHA1 hmac = new HMACSHA1(encoding.GetBytes(key)))
            {
                var msg = encoding.GetBytes(message);
                var hash = hmac.ComputeHash(msg);
                return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
            }
        }
    }
    public class HBLCreateTokenTransactionRequest
    {

        public string access_key { get; set; } = "3f5ce24117083524866b613e46c6db5b";//test 
        public string profile_id { get; set; } = "4B3414B9-6DBB-4F92-AEF5-CE40FCC95C11";//test 

        public string transaction_uuid { get; set; } = System.Guid.NewGuid().ToString();

        #region fields for recurring token
        public string consumer_id { get; set; }
        //public string recurring_frequency { get; set; }
        //public string recurring_amount { get; set; }
        //public string recurring_start_date { get; set; }
        #endregion
        #region Merchant Defined Data Fields
        public string merchant_defined_data1 { get; set; } = "WC";
        public string merchant_defined_data2 { get; set; } = "Donation";//= "Yes";
        //public string merchant_defined_data3 { get; set; } = "Donation";
        //public string merchant_defined_data20 { get; set; } = "NO";
        #endregion
        //for recurring transactions
        public string signed_field_names { get; set; } = "recurring_number_of_installments,recurring_start_date,recurring_amount,recurring_frequency,access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,merchant_defined_data1,merchant_defined_data2,consumer_id";
        // for normal transaction
        //public string signed_field_names { get; set; } = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,merchant_defined_data1";

        public string unsigned_field_names { get; set; } = "card_type,card_number,card_expiry_date";
        public string signed_date_time { get; set; } = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
        public string locale { get; set; } = "en";

        public string transaction_type { get; set; } = "sale,create_payment_token";//"authorization";
        public string reference_number { get; set; } = (DateTime.UtcNow.Ticks.ToString());//any unique no like invoice id
        public string amount { get; set; } = "500";
        public string currency { get; set; } = "PKR";
        public string payment_method { get; set; } = "card";
        public string bill_to_forename { get; set; } = "John";//first name
        public string bill_to_surname { get; set; } = "Doe";//last name
        public string bill_to_email { get; set; } = "null@cybersource.com";//last name
        public string bill_to_phone { get; set; } = "02890888888";//last name
        public string bill_to_address_line1 { get; set; } = "Gulistan e jauhar";//last name
        public string bill_to_address_city { get; set; } = "Karachi";//last name
        public string bill_to_address_state { get; set; } = "SN";//last name
        public string bill_to_address_country { get; set; } = "PK";//last name
        public string bill_to_address_postal_code { get; set; } = "75290";//last name
        public string signature { get; set; } = "";//last name
        //public string customer_ip_address {get;set;}

        public string recurring_frequency { get; set; }
        public string recurring_amount { get; set; }
        public string recurring_start_date { get; set; }
        public string recurring_number_of_installments { get; set; }

        public string GetSignature(IDictionary<string, string> parameters)
        {
            if (parameters != null)
            {
                this.signature = Security.sign(parameters);
            }
            return this.signature;
        }
        public IDictionary<string, string> GetProperties()
        {
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            PropertyInfo[] properties = typeof(HBLCreateTokenTransactionRequest).GetProperties();

            foreach (PropertyInfo property in properties)
            {
                try
                {
                    parameters.Add(property.Name, property.GetValue(this).ToString());
                }
                catch (Exception ex) { }
            }
            return parameters;

        }
        /*
        public string profile_id { get; set; } = "4B3414B9-6DBB-4F92-AEF5-CE40FCC95C11";//any unique no like invoice id
        public string access_key { get; set; } = "3f5ce24117083524866b613e46c6db5b";
        public string transaction_type { get; set; } = "sale,create_payment_token";// "authorization,create_payment_token";//"authorization";//"sale,create_payment_token";//
        public string locale { get; set; } = "en";
        public string amount { get; set; } = "500";
        public string transaction_uuid { get; set; } = System.Guid.NewGuid().ToString();
        public string signed_date_time { get; set; } = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
        public string signed_field_names { get; set; } = "profile_id,access_key,transaction_type,locale,amount,transaction_uuid,signed_date_time,signed_field_names,signature,consumer_id,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_line1,bill_to_address_state,bill_to_address_country,recurring_frequency,recurring_amount,recurring_start_date,payment_method,merchant_defined_data1,unsigned_field_names,reference_number";
            //"access_key,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_line1,bill_to_address_state,bill_to_address_country,merchant_defined_data1,consumer_id,recurring_frequency,recurring_amount,recurring_start_date";
        public string signature { get; set; } = "";
        public string consumer_id { get; set; }
        public string bill_to_forename { get; set; } = "John";//first name
        public string bill_to_surname { get; set; } = "Doe";//last name
        public string bill_to_email { get; set; } = "null@cybersource.com";//last name
        public string bill_to_address_line1 { get; set; } = "Gulistan e jauhar";//last 
        public string bill_to_address_state { get; set; } = "SN";//last name
        public string bill_to_address_country { get; set; } = "PK";//last name
        // no card information to pass
        public string recurring_frequency { get; set; }
        public string recurring_amount { get; set; }
        public string recurring_start_date { get; set; }
        public string payment_method { get; set; } = "card";

        #region Merchant Defined Data Fields
        public string merchant_defined_data1 { get; set; } = "WC";
        #endregion
     
        public string unsigned_field_names { get; set; } = "card_type,card_number,card_expiry_date";
                
        public string reference_number { get; set; } = (DateTime.UtcNow.Ticks.ToString());//any unique no like invoice id
        
       // public string currency { get; set; } = "PKR";
       
        
       //last name
                                                   //public string profile_id { get; set; } = "4B3414B9-6DBB-4F92-AEF5-CE40FCC95C11";//any unique no like invoice id
                                                   //public string bill_to_address_city { get; set; } = "Karachi";//last name
                                                   // public string bill_to_address_postal_code { get; set; } = "75290";//last name
                                                   //public string bill_to_phone { get; set; } = "02890888888";//last name
        public string GetSignature(IDictionary<string, string> parameters) {
            if (parameters != null) {
                this.signature = Security.sign(parameters);
            }
            return this.signature;
        }
        public IDictionary<string, string> GetProperties()
        {
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            PropertyInfo[] properties = typeof(HBLCreateTokenTransactionRequest).GetProperties();

            foreach (PropertyInfo property in properties)
            {
                try
                {
                    parameters.Add(property.Name, property.GetValue(this).ToString());
                }
                catch (Exception ex) { }
            }
            return parameters;

        }
        */
    }

    public class HBLRecurringTransactionRequest
    {
        public string access_key { get; set; } = "3f5ce24117083524866b613e46c6db5b";
        public string profile_id { get; set; } = "4B3414B9-6DBB-4F92-AEF5-CE40FCC95C11";
        public string transaction_uuid { get; set; } = System.Guid.NewGuid().ToString();

        #region Merchant Defined Data Fields
        public string merchant_defined_data1 { get; set; } = "WC";
        #endregion
        public string signed_field_names { get; set; } = "consumer_id,amount,access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code,merchant_defined_data1,recurring_amount,payment_token";

        public string unsigned_field_names { get; set; } = "card_type,card_number,card_expiry_date";
        public string signed_date_time { get; set; } = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-dd'T'HH:mm:ss'Z'");
        public string locale { get; set; } = "en";

        public string transaction_type { get; set; } = "sale";//,update_payment_token";//"authorization";
        public string reference_number { get; set; } = (DateTime.UtcNow.Ticks.ToString());//any unique no like invoice id

        public string currency { get; set; } = "PKR";
        public string payment_method { get; set; } = "card";
        public string bill_to_forename { get; set; } = "John";//first name
        public string bill_to_surname { get; set; } = "Doe";//last name
        public string bill_to_email { get; set; } = "null@cybersource.com";//last name
        //public string bill_to_phone { get; set; } = "02890888888";//last name
        public string bill_to_address_line1 { get; set; } = "Gulistan e jauhar";//last name
        public string bill_to_address_city { get; set; } = "Karachi";//last name
        public string bill_to_address_state { get; set; } = "SN";//last name
        public string bill_to_address_country { get; set; } = "PK";//last name
        public string bill_to_address_postal_code { get; set; } = "75290";//last name
        public string signature { get; set; } = "";//last name
        public string amount { get; set; } = "500";

        public string payment_token { get; set; }
        public string recurring_amount { get; set; } = "500";
        // public string recurring_frequency { get; set; }
        // public string recurring_start_date { get; set; }
        public string consumer_id { get; set; }
        public string GetSignature(IDictionary<string, string> parameters)
        {
            if (parameters != null)
            {
                this.signature = Security.sign(parameters);
            }
            return this.signature;
        }
        public IDictionary<string, string> GetProperties()
        {
            IDictionary<string, string> parameters = new Dictionary<string, string>();
            PropertyInfo[] properties = typeof(HBLRecurringTransactionRequest).GetProperties();

            foreach (PropertyInfo property in properties)
            {
                parameters.Add(property.Name, property.GetValue(this).ToString());
            }
            return parameters;

        }
    }

    //public class DonationReport : Donation {
    //    public string TransactionDateTime { get; set; }
    //    public string TransactionUUID { get; set; }
    //}
}