﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace DonorManagementApp.Models
{
    public class helper
    {
        string PSSrcConstr = ConfigurationManager.ConnectionStrings["DBConnection"].ToString();//PeopleSoft Source Connection string for fetching data

        public List<string> SelectListPSCS(string Query)
        {
            
           SqlCommand cmd;
            SqlDataReader readr;
            List<string> temp = new List<string>();
            try
            {
                using (SqlConnection con = new SqlConnection(PSSrcConstr))
                {
                    if (con.State == ConnectionState.Open) con.Close();

                    con.Open();
                    cmd = new SqlCommand(Query, con);

                    readr = cmd.ExecuteReader();


                    while (readr.Read())
                    {
                        for (int i = 0; i < readr.FieldCount; i++)
                            temp.Add(readr[i].ToString());
                    }

                    return temp;
                }
            }
            catch (Exception)
            {
                return null;
            }

        }
        public DataTable SelectDBPSCS(String Query)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(PSSrcConstr))
                {
                    if (con.State == ConnectionState.Open) con.Close();


                    con.Open();
                    SqlDataAdapter oraAdapt = new SqlDataAdapter(Query, con);
                    DataTable dt = new DataTable();
                    oraAdapt.Fill(dt);
                    con.Close();
                    return dt;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}