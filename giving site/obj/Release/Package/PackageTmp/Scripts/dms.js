﻿(function () {
    try {
        $("body").on('hidden.bs.modal', function (e) {
            var $iframes = $(e.target).find("iframe");
            $iframes.each(function (index, iframe) {
                $(iframe).attr("src", $(iframe).attr("src"));
            });
        });
    } catch (e) { console.log(e); console.log('above error is from close youtube video model.'); }
    $('.group-2').css('display', 'none');
    $('.freq-select').on('change', function (e) {
        $('.amnt-select').prop('checked', false);
        $('#onetimeonly').prop('checked', false);
        $('.amnt-select').prop('required','required');
        if (this.value.toLocaleLowerCase() === "monthly") {
            $('.group-1').css('display', 'inline-block');
            $('.group-2').css('display', 'none');
        }
        if (this.value.toLocaleLowerCase() === "yearly") {
            $('.group-1').css('display', 'none');
            $('.group-2').css('display', 'inline-block');
        }
    });
    $('#onetimeonly').on('change', function (e) {
        if ($(this).is(':checked')) {
            $('.freq-select').prop('checked', false);
            $('.amnt-select').prop('checked', false);
            $('.amnt-select').removeProp('required');
        }
    });
//    $('#mainPage').addClass('show');

    $('.joinUs').click(function () {
        $(".landingContent").addClass('hideContentWithanimate');//.show();
        $('.formContent').removeClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        return false;
    });

    $('.mohsineen').click(function () {
        $(".mohsineenContent").removeClass('hideContentWithanimate');//.show();
        $('.formContent').addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        return false;
    });

    $('.home').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').removeClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');//.show();
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        return false;
    });

    $('.visionariesLink').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').removeClass('hideContentWithanimate');
        return false;
    });
    $('.benefactorsLink').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').removeClass('hideContentWithanimate');
        
        return false;
    });
    $('.supportersLink').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').removeClass('hideContentWithanimate');
        return false;
    });
    

    jQuery('.numbersOnly').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });
    function validateEmail(email) {
        //var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(email)) {
            return false;
        } else {
            return true;
        }
    }

    $('#btnSubscribe').on("click", function (e) {

        var link = '';
        var url = '';
        var flag = true;
        var plan = '';
        var amount = 0;
        var firstName = '';
        var lastName = '';
        var mobile = '';
        var organization = '';
        var treatAsZakat = '';
        var requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();

        if ($('#onetimeonly').is(':checked')) {
            plan = 'One Time';
        } else {
            plan = $('input[name="RecurringDonationFrequency"]:checked').val();
        }
        $('#Plan').val(plan);

        if ($('input[name="Amount"]').is(':checked')) {
            amount = $('input[name="Amount"]:checked').val();
        } else {
            amount = $('#inputOtherAmount').val();
            if (amount) {
                amount = amount.replace(",", "");
                amount = parseInt(amount);
            }
            $('#Amount').val(amount);
            if ($('input[name="TreatAsZakat"]').is(':checked')) {
                treatAsZakat = $('input[name="TreatAsZakat"]:checked').val();
            } else {
                treatAsZakat = '';
            }

            firstName = $('#firstname').val();
            lastName = $('#lastname').val();
            mobile = $('#mobilenumber').val();
            email = $('#email').val();
            organization = $('#organization').val();


            if (amount >= 500) {
                $('#error_Amount').addClass('text-hide');
            } else {
                $('#error_Amount').removeClass('text-hide');
                flag = false;
            }
            if (firstName) {
                if (firstName.trim().length === 0) {
                    $('#error_firstname').html('Please enter your first name');
                    $('#error_firstname').removeClass('text-hide');
                    document.getElementById('firstname').scrollIntoView(true);
                    flag = false;
                } else if (firstName.trim().length > 120) {
                    $('#error_firstname').html('First name shuold be less than 120 characters');
                    $('#error_firstname').removeClass('text-hide');
                    document.getElementById('firstname').scrollIntoView(true);
                    flag = false;
                }
                else {
                    $('#error_firstname').addClass('text-hide');
                }
            } else {
                $('#error_firstname').html('Please enter your first name');
                $('#error_firstname').removeClass('text-hide');
                document.getElementById('firstname').scrollIntoView(true);
                flag = false;
            }
            if (lastName) {
                if (lastName.trim().length === 0) {
                    $('#error_lastname').html('Please enter your last name');
                    $('#error_lastname').removeClass('text-hide');
                    document.getElementById('lastname').scrollIntoView(true);
                    flag = false;
                } else if (lastName.trim().length > 120) {
                    $('#error_lastname').html('Last name shuold be less than 120 characters');
                    $('#error_lastname').removeClass('text-hide');
                    document.getElementById('lastname').scrollIntoView(true);
                    flag = false;
                }
                else {
                    $('#error_lastname').addClass('text-hide');
                }
            } else {
                $('#error_lastname').html('Please enter your last name');
                $('#error_lastname').removeClass('text-hide');
                document.getElementById('lastname').scrollIntoView(true);
                flag = false;
            }

            if (mobile) {
                if (mobile.trim().length === 0) {
                    $('#error_mobilenumber').html('Please enter your mobile');
                    $('#error_mobilenumber').removeClass('text-hide');
                    document.getElementById('mobilenumber').scrollIntoView(true);
                    flag = false;
                } else if (mobile.trim().length > 15) {
                    $('#error_mobilenumber').html('Mobile number length should be less than or equal to 15 numbers');
                    $('#error_mobilenumber').removeClass('text-hide');
                    document.getElementById('mobilenumber').scrollIntoView(true);
                    flag = false;
                } else {
                    //$('#error_mobilenumber').html('');
                    $('#error_mobilenumber').addClass('text-hide');
                }
            } else {
                $('#error_mobilenumber').html('Please enter your mobile');
                $('#error_mobilenumber').removeClass('text-hide');
                document.getElementById('mobilenumber').scrollIntoView(true);
                flag = false;
            }

            if (email) {
                if (email.trim().length > 0) {
                    if (validateEmail(email)) {
                        $('#error_email').html('');
                        $('#error_email').addClass('text-hide');
                        if (email.trim().length > 180) {
                            $('#error_email').html('Email length should be less than or equal to 180 characters');
                            $('#error_email').removeClass('text-hide');
                            document.getElementById('email').scrollIntoView(true);
                            flag = false;
                        }
                        else {
                            $('#error_email').addClass('text-hide');
                        }
                    } else {
                        $('#error_email').html('Please enter a valid email');
                        $('#error_email').removeClass('text-hide');
                        document.getElementById('email').scrollIntoView(true);
                        flag = false;
                    }
                } else {
                    $('#error_email').html('Please enter an email');
                    $('#error_email').removeClass('text-hide');
                    document.getElementById('email').scrollIntoView(true);
                    flag = false;
                }
            } else {
                $('#error_email').html('Please enter your email');
                $('#error_email').removeClass('text-hide');
                document.getElementById('email').scrollIntoView(true);
                flag = false;
            }

            // file case
            //var totalPledgedAmount = $("#spn-summary").html();
            //totalPledgedAmount = totalPledgedAmount.replace(/,/g, '');
            if (flag) {
                document.forms[0].submit();
            }

            /* this is to impletment async
            if (amount) {
    
                var donation = {
                    Id: 0, 'FirstName': capitalize(firstName), 'LastName': capitalize(lastName), 'CellNumber': mobile, 'Email': email, 'Organization': capitalize(organization),
                    'TreatAsZakat': treatAsZakat, 'Amount': amount, 'Plan': plan, __RequestVerificationToken: requestVerificationToken
                };
                console.log(donation);
    
                if (donation.length <= 0) {
                    alert('Please select an option to pledge.');
                    flag = false;
                }
                if (flag) {
                    //showLoader();
                    $.post('/Home/Donate', donation, function (resp) {
                        console.log(resp);
                        if (resp) {
                            if (resp.Success) {
                                if (resp.TypeId === 0) {
                                    var link = $('#ThankYouLink');
                                    if (link) {
                                        url = (document.location.origin + link.val());
                                        redirect(url);
                                        return false;
                                    }
                                    else
                                        alert('Thank you for your pledge');
                                } else {     // donor case
                                    if (resp.PledgerId) {
                                        if (resp.TypeId === 1) {
                                            if (resp.PledgerId > 0) {
                                                link = $('#Pay').val();
                                                //}
                                                //var link = $('#DonationStatus');
                                                //if (link) {
                                                url = (document.location.origin + link);
                                                redirectToGateway(url, resp.PledgerId, resp.Name, resp.TotalPledgeAmount);
                                                //redirectToGateway(url, totalPledgedAmount, resp.PledgerId, resp.Name, resp.HashKey, resp.TimeStamp, resp.X_login, resp.Email);
                                                //return false;
                                            }
                                            else {
                                                alert('Faild to save record, please try again.');
                                                document.location.href = document.location.href;
                                            }
                                        }
                                    }
                                }
                            }
                            else {
                                alert(resp.Msg);
                                return false;
                            }
                        }
                    });
                }
                return false;
            }
            */
        }
        e.preventDefault();
    });
    function capitalize(text) {
        return text.replace(/\b\w/g, function (m) { return m.toUpperCase(); });
    }
    function addCommas(amount) {
        if (amount) {
            amount = amount.toString();
            var result = "";
            amount = amount.split("").reverse().join("");
            for (var i = 0; i < amount.length; i++) {
                result = result + amount[i];
                if (i === 2)
                    result = result + ',';
                if (i === 5)
                    result = result + ',';
                if (i === 8)
                    result = result + ',';
                if (i === 11)
                    result = result + ',';
                if (i === 14)
                    result = result + ',';

                // console.log(result);
            }
            result = result.split("").reverse().join("");
            if (result.charAt(0) === ',')
                result = result.slice(1);

            return result;
        }
    }

    $('.amnt-select').change(function (e) {
        $('#inputOtherAmount').val('');
    });
    $('#inputOtherAmount').on('keyup',function (e) {
        $('.amnt-select').prop('checked', false);
        if (e) {
            var value = this.value;
            if (value) {
                value = value.replace(",", "");
                 value = parseInt(value);
                if (value > 0) {
                    this.value = addCommas(value);
                    return false;
                }
            }
        }
    });
    $('#inputOtherAmount').on('change', function (e) {
        $('.amnt-select').prop('checked', false);
        if (e) {
            var value = this.value;
            if (value) {
                value = value.replace(",", "");
                value = parseInt(value);
                if (value > 1000000) {
                    alert('Amount should be between 500 - 1,000,000');
                    this.value = 0;
                    return false;
                }
                //extra check 
                else if (value < 500) {
                    alert('Minimum amount should be 500 PKR');
                    this.value = 0;
                    return false;
                } else {
                    this.value = addCommas(value);
                    return false;
                }
            }
        }
    });
     function redirectToGateway(url, id, name, amount) {

        var form = $(document.createElement('form'));
        $(form).attr("action", url);
        $(form).attr("method", "POST");


        var input = $("<input>")
            .attr("type", "hidden")
            .attr("name", "totalAmount")
            .val(amount);
        $(form).append($(input));

        var input1 = $("<input>")
            .attr("type", "hidden")
            .attr("name", "pledgerId")
            .val(id);
        $(form).append($(input1));
        var input2 = $("<input>")
            .attr("type", "hidden")
            .attr("name", "name")
            .val(name);
        $(form).append($(input2));

        form.appendTo(document.body);

        $(form).submit();
        //  var url = (document.location.origin + $('#ThankYouLink').val());
        //  document.location.href = url;
    }

    getDonors('donors-Visonaries-Content', 99999, 999999999999, this.value);
    //document.getElementById(data.id).scrollIntoView(true);
})();

function getDonors(container, min, max, name) {
    //showLoader();
    $.post('/Home/Donors', { Min: min, Max: max, Name: name }, function (resp) {
        console.log(resp);
        if (resp) {
            if (resp.Success) {
                console.log(resp.Data);
                if (resp.Data !== null && resp.Data.length > 0) {
                    $('.' + container).html('');
                    var newContent = '';
                    $.each(resp.Data, function (index, data) {
                        if (data !== null) {
                            var name = data.FirstName + ' ' + data.LastName;
                            var org = data.Organization;
                            $('.' + container).append('<div class="list-design"><label class= "list-name">' + name + '</label><label class="list-org">' + org + '</label></div>');
                            newContent += '<div class="list-design">' +
                                +'<label class= "list-name">' + name + '</label>' +
                                +'<label class="list-org">' + org + '</label>' +
                                +' </div>';
                        }
                    });
                    //$('.donorsContent').html(newContent);
                } else {
                    $('.' + container).html('<div class="list-design" ><label class="list-name">Records not found...</label></div>');
                }
            }
            else {
                alert(resp.Msg);
                return false;
            }
        }
    });
    return false;
}
//function close(id) {
//    $('#' + id).removeClass('show');
//}
//$('.detailsLink').click(function () {
//    $("#container").load(this.href).addClass('show');//.show();
//    $('#mainPage').removeClass('show');
//    return false;
//});
/*
function getTotalAmount() {
    var totalPledged = 0;
    var selectedFunds = $('input[type="checkbox"]:checked');
    if (selectedFunds) {
        $.each(selectedFunds, function (index, data) {
            if (data) {
                if (data) {
                    if (data.dataset) {
                        if (data.dataset.amount) {
                            var value = parseInt(data.dataset.amount);

                            if (value === 0) {
                                try {
                                    //alert('Please select amount in ' + $(data).parent().text().trim());
                                    _popUpMsg = 'Please select amount in ' + $(data).parent().text().trim();
                                    
                                } catch (ex) { console.debug(ex); }
                                flag = false;
                                return false;
                            }
                            else if (value === -1) {
                                try {
                                    try {
                                        //    alert('Please enter numeric value in ' + $(data).parent().text().trim());
                                        _popUpMsg = 'Please enter numeric value in ' + $(data).parent().text().trim();
                                        document.getElementById(data.id).scrollIntoView(true);
                                    } catch (ex) { console.debug(ex); }
                                    flag = false;
                                    return false;
                                }
                                catch (ex) { console.debug(ex); }
                            }
                            //else if (value > 50000000) {
                            //    try {
                            //        alert('Amount to pledge should be equal to or less than USD 50,000,000 in ' + $(data).parent().text().trim());
                            //    } catch (ex) { console.debug(ex); }
                            //    flag = false;
                            //    return false;
                            //}

                            totalPledged += value;
                        }
                    }
                }
            }
        });
    }
    return totalPledged;
}
*/
