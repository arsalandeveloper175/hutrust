﻿(function () {
    try {
        $("body").on('hidden.bs.modal', function (e) {
            var $iframes = $(e.target).find("video");
            $iframes.each(function (index, iframe) {
                $(iframe).attr("src", $(iframe).attr("src"));
            });
        });
    } catch (e) { console.log(e); console.log('above error is from close youtube video model.'); }
    $('.group-2').css('display', 'none');

    $('input[name="recognition"]').change(function () {
        $('#error_InTheNameOf').addClass('text-hide');
        if ($(this).val() === "2") {
            $("#InTheNameOfField").show();
        }
        else {
            $("#InTheNameOfField").hide();
        }
    });

    $("#HearFrom").change(function () {
        if ($(this).val() === "other" || $(this).val() === "Friend/Colleague") { $('#HearFromDiv').show(); $("#HearFromField").show(); }
        else { $('#HearFromDiv').hide(); $("#HearFromField").hide(); $('#error_HearFrom').addClass('text-hide'); }
    });


    $('.freq-select').on('change', function (e) {
        $('.amnt-select').prop('checked', false);//needed
        $('#onetimeonly').prop('checked', false);
        $('.amnt-select').prop('required', 'required');
        if (this.value.toLocaleLowerCase() === "monthly") {
            $('.group-1').css('display', 'inline-block');
            $('.group-2').css('display', 'none');
            try {
                if ($('#inputOtherAmount').val().trim().length === 0) {
                    $($('.amnt-select.group-2')[0]).prop('checked', false);
                    $($('.amnt-select.group-1')[0]).prop('checked', true);
                }
            } catch (e) { console.log(e); }
        }
        if (this.value.toLocaleLowerCase() === "yearly") {
            $('.group-1').css('display', 'none');
            $('.group-2').css('display', 'inline-block');
            try {
                if ($('#inputOtherAmount').val().trim().length === 0) {
                    $($('.amnt-select.group-1')[0]).prop('checked', false);
                    $($('.amnt-select.group-2')[0]).prop('checked', true);
                }
            } catch (e) { console.log(e); }
        }
        document.getElementById('error_plan').classList.add('text-hide');
    });
    $('#onetimeonly').on('change', function (e) {
        if ($(this).is(':checked')) {
            $('.group-1').css('display', 'none');
            $('.group-2').css('display', 'inline-block');
            $('.freq-select').prop('checked', false);
            $('.amnt-select').prop('checked', false);//needed
            //$('.amnt-select').removeProp('required');//needed
            try {
                if ($('#inputOtherAmount').val().trim().length === 0) {
                    $($('.amnt-select.group-1')[0]).prop('checked', false);
                    $($('.amnt-select.group-2')[0]).prop('checked', true);
                }
            } catch (e) { console.log(e); }
        }
        document.getElementById('error_plan').classList.add('text-hide');
    });
    //    $('#mainPage').addClass('show');

    $('.joinUs').click(function () {
        $(".landingContent").addClass('hideContentWithanimate');//.show();
        $('.formContent').removeClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        $('.mohsineenFellowsContent').addClass('hideContentWithanimate');
        return false;
    });

    $('.mohsineen').click(function () {
        $(".mohsineenContent").removeClass('hideContentWithanimate');//.show();
        $('.formContent').addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $('.mohsineenFellowsContent').addClass('hideContentWithanimate');
        return false;
    });

    $('.home').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').removeClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');//.show();
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $('.mohsineenFellowsContent').addClass('hideContentWithanimate');
        document.getElementById('mainPage').scrollIntoView(true);
        return false;
    });

    $('.visionariesLink').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenFellowsContent').addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').removeClass('hideContentWithanimate');
        document.getElementById('mohsineenVisionariesContent').scrollIntoView(true);
        return false;
    });
    $('.benefactorsLink').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenFellowsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').removeClass('hideContentWithanimate');
        document.getElementById('mohsineenBenefactorsContent').scrollIntoView(true);
        return false;
    });
    $('.supportersLink').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').removeClass('hideContentWithanimate');
        $('.mohsineenFellowsContent').addClass('hideContentWithanimate');
        document.getElementById('mohsineenSupportsContent').scrollIntoView(true);
        return false;
    });
    $('.fellowsLink').click(function () {
        //$(".formContent").load(this.href).addClass('hideContentWithanimate');
        $(".formContent").addClass('hideContentWithanimate');
        $('.landingContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenFellowsContent').removeClass('hideContentWithanimate');
        document.getElementById('mohsineenFellowsContent').scrollIntoView(true);
        return false;
    });


    jQuery('.numbersOnly').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    jQuery('.alphabateOnly').keyup(function () {
        this.value = this.value.replace(/[^a-zA-Z\ ]/g, '');
    });

    function validateEmail(email) {
        //var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(email)) {
            return false;
        } else {
            return true;
        }
    }

    $('#btnSubscribe').on("click", function (e) {

        var link = '';
        var url = '';
        var flag = true;
        var plan = '';
        var amount = 0;
        var firstName = '';
        var lastName = '';
        var mobile = '';
        var address = '';
        var organization = '';
        //var address = '';
        var treatAsZakat = '';
        var InTheNameOf = '';
        var recognition = '';
        var isRecognitionSelected = false;
        var hearFrom = '';
        var requestVerificationToken = $('input[name="__RequestVerificationToken"]').val();

        if ($('#onetimeonly').is(':checked')) {
            plan = 'One Time';
        } else {
            plan = $('input[name="RecurringDonationFrequency"]:checked').val();
        }
        $('#Plan').val(plan);

        if ($('input[name="Figure"]').is(':checked')) {
            amount = $('input[name="Figure"]:checked').val();
        } else {
            amount = $('#inputOtherAmount').val();
            if (amount) {
                amount = amount.replace(",", "");
                amount = parseInt(amount);
            }
            if ($('input[name="TreatAsZakat"]').is(':checked')) {
                treatAsZakat = $('input[name="TreatAsZakat"]:checked').val();
            } else {
                treatAsZakat = '';
            }
        }
        $('#Amount').val(amount);
        firstName = $('#firstname').val();
        lastName = $('#lastname').val();
        mobile = $('#mobilenumber').val();
        email = $('#email').val();
        organization = $('#organization').val();
        address = $('#address').val();
        hearFrom = $('#HearFrom').val();
        HearFromField = $('#HearFromField').val();
        isRecognitionSelected = $('input[name="recognition"]').is(':checked');
        recognition = $('input[name="recognition"]:checked').val();

        if (isRecognitionSelected === null || isRecognitionSelected === '' || isRecognitionSelected === undefined || isRecognitionSelected === false) {
            document.getElementById('recognitionContainer').scrollIntoView(true);
            $('#error_Recognition').removeClass('text-hide');
            flag = false;
        } else {
            $('#error_Recognition').addClass('text-hide');
        }

        InTheNameOf = $('#InTheNameOfField').val();
        if (isRecognitionSelected === true && recognition === "2") {
            if (InTheNameOf) {
                if (InTheNameOf.trim().length === 0) {
                    $('#error_InTheNameOf').html('Please enter name for recognition');
                    $('#error_InTheNameOf').removeClass('text-hide');
                    document.getElementById('InTheNameOfField').scrollIntoView(true);
                    flag = false;
                } else if (InTheNameOf.trim().length > 60) {
                    $('#error_InTheNameOf').html('Name of recognition shuold be less than 30 characters');
                    $('#error_InTheNameOf').removeClass('text-hide');
                    document.getElementById('InTheNameOfField').scrollIntoView(true);
                    flag = false;
                }
                else {
                    $('#error_InTheNameOf').addClass('text-hide');
                }
            } else {
                $('#error_InTheNameOf').html('Please enter the name');
                $('#error_InTheNameOf').removeClass('text-hide');
                document.getElementById('InTheNameOfField').scrollIntoView(true);
                flag = false;
            }
        } else {
            //  $('#error_Recognition').addClass('text-hide');
            $('#error_InTheNameOf').addClass('text-hide');
        }

        if (plan === null || plan === '' || plan === undefined) {
            $('#error_plan').removeClass('text-hide');
            document.getElementById('planContainer').scrollIntoView(true);
            flag = false;
        } else {
            $('#error_plan').addClass('text-hide');
        }
        if (amount > 0) {
            $('#error_Amount').addClass('text-hide');
        } else {
            $('#error_Amount').removeClass('text-hide');
            document.getElementById('amountContainer').scrollIntoView(true);
            flag = false;
        }
        if (address) {

            if (address.trim().length === 0) {
                $('#error_address').html('Please enter your address');
                $('#error_address').removeClass('text-hide');
                document.getElementById('address').scrollIntoView(true);
                flag = false;
            } else if (address.trim().length > 100) {
                $('#error_address').html('address shuold be less than 100 characters');
                $('#error_address').removeClass('text-hide');
                document.getElementById('address').scrollIntoView(true);
                flag = false;
            }
            else {
                $('#error_address').addClass('text-hide');
            }
        } else {
            $('#error_address').html('Please enter your address ');
            $('#error_address').removeClass('text-hide');
            document.getElementById('address').scrollIntoView(true);
            flag = false;
        }
        if (hearFrom) {

            if (hearFrom === "0") {
                $('#error_HearFrom').html('Please select how did you hear about us?');
                $('#error_HearFrom').removeClass('text-hide');
                document.getElementById('HearFrom').scrollIntoView(true);
                flag = false;
            } else if (hearFrom === 'other' || hearFrom === 'Friend/Colleague') {
                if (HearFromField != null) {
                    if (HearFromField.trim().length === 0) {
                        $('#error_HearFromField').html('Please specify ' + hearFrom.toLowerCase());
                        $('#error_HearFromField').removeClass('text-hide');
                        document.getElementById('HearFromField').scrollIntoView(true);
                        flag = false;
                    } else {
                        $('#error_HearFromField').addClass('text-hide');
                        $('#error_HearFrom').addClass('text-hide');
                    }
                } else {
                    $('#error_HearFromField').html('Please specify ' + hearFrom.toLowerCase());
                    $('#error_HearFromField').removeClass('text-hide');

                }
            }
            else {
                $('#error_HearFromField').addClass('text-hide');
                $('#error_HearFrom').addClass('text-hide');
            }
        } else {
            $('#error_address').html('Please enter your address');
            $('#error_address').removeClass('text-hide');
            document.getElementById('address').scrollIntoView(true);
            flag = false;
        }


        if (firstName) {
            if (firstName.trim().length === 0) {
                $('#error_firstname').html('Please enter your first name');
                $('#error_firstname').removeClass('text-hide');
                document.getElementById('firstname').scrollIntoView(true);
                flag = false;
            } else if (firstName.trim().length > 15) {
                $('#error_firstname').html('First name shuold be less than 14 characters');
                $('#error_firstname').removeClass('text-hide');
                document.getElementById('firstname').scrollIntoView(true);
                flag = false;
            }
            else {
                $('#error_firstname').addClass('text-hide');
            }
        } else {
            $('#error_firstname').html('Please enter your first name');
            $('#error_firstname').removeClass('text-hide');
            document.getElementById('firstname').scrollIntoView(true);
            flag = false;
        }
        if (lastName) {
            if (lastName.trim().length === 0) {
                $('#error_lastname').html('Please enter your last name');
                $('#error_lastname').removeClass('text-hide');
                document.getElementById('lastname').scrollIntoView(true);
                flag = false;
            } else if (lastName.trim().length > 15) {
                $('#error_lastname').html('Last name shuold be less than 14 characters');
                $('#error_lastname').removeClass('text-hide');
                document.getElementById('lastname').scrollIntoView(true);
                flag = false;
            }
            else {
                $('#error_lastname').addClass('text-hide');
            }
        } else {
            $('#error_lastname').html('Please enter your last name');
            $('#error_lastname').removeClass('text-hide');
            document.getElementById('lastname').scrollIntoView(true);
            flag = false;
        }

        if (mobile) {
            if (mobile.trim().length === 0) {
                $('#error_mobilenumber').html('Please enter your mobile');
                $('#error_mobilenumber').removeClass('text-hide');
                document.getElementById('mobilenumber').scrollIntoView(true);
                flag = false;
            } else if (mobile.trim().length < 5) {
                $('#error_mobilenumber').html('Cell must be 5 digit number');
                $('#error_mobilenumber').removeClass('text-hide');
                document.getElementById('mobilenumber').scrollIntoView(true);
                flag = false;
            }
            else if (mobile.trim().length > 15) {
                $('#error_mobilenumber').html('Cell number length should be less than or equal to 15 numbers');
                $('#error_mobilenumber').removeClass('text-hide');
                document.getElementById('mobilenumber').scrollIntoView(true);
                flag = false;
            } else {
                //$('#error_mobilenumber').html('');
                $('#error_mobilenumber').addClass('text-hide');
            }
        } else {
            $('#error_mobilenumber').html('Please enter your mobile');
            $('#error_mobilenumber').removeClass('text-hide');
            document.getElementById('mobilenumber').scrollIntoView(true);
            flag = false;
        }

        if (email) {
            if (email.trim().length > 0) {
                if (validateEmail((email.trim()))) {
                    $('#error_email').html('');
                    $('#error_email').addClass('text-hide');
                    if (email.trim().length > 180) {
                        $('#error_email').html('Email length should be less than or equal to 180 characters');
                        $('#error_email').removeClass('text-hide');
                        document.getElementById('email').scrollIntoView(true);
                        flag = false;
                    }
                    else {
                        $('#error_email').addClass('text-hide');
                    }
                } else {
                    $('#error_email').html('Please enter a valid email');
                    $('#error_email').removeClass('text-hide');
                    document.getElementById('email').scrollIntoView(true);
                    flag = false;
                }
            } else {
                $('#error_email').html('Please enter an email');
                $('#error_email').removeClass('text-hide');
                document.getElementById('email').scrollIntoView(true);
                flag = false;
            }
        } else {
            $('#error_email').html('Please enter your email');
            $('#error_email').removeClass('text-hide');
            document.getElementById('email').scrollIntoView(true);
            flag = false;
        }

        // file case
        //var totalPledgedAmount = $("#spn-summary").html();
        //totalPledgedAmount = totalPledgedAmount.replace(/,/g, '');
        if (flag) {
            showLoader();
            document.forms[0].submit();
        }

        /* this is to impletment async
        if (amount) {
 
            var donation = {
                Id: 0, 'FirstName': capitalize(firstName), 'LastName': capitalize(lastName), 'CellNumber': mobile, 'Email': email, 'Organization': capitalize(organization),
                'TreatAsZakat': treatAsZakat, 'Amount': amount, 'Plan': plan, __RequestVerificationToken: requestVerificationToken
            };
            console.log(donation);
 
            if (donation.length <= 0) {
                alert('Please select an option to pledge.');
                flag = false;
            }
            if (flag) {
                //showLoader();
                $.post('/Home/Donate', donation, function (resp) {
                    console.log(resp);
                    if (resp) {
                        if (resp.Success) {
                            if (resp.TypeId === 0) {
                                var link = $('#ThankYouLink');
                                if (link) {
                                    url = (document.location.origin + link.val());
                                    redirect(url);
                                    return false;
                                }
                                else
                                    alert('Thank you for your pledge');
                            } else {     // donor case
                                if (resp.PledgerId) {
                                    if (resp.TypeId === 1) {
                                        if (resp.PledgerId > 0) {
                                            link = $('#Pay').val();
                                            //}
                                            //var link = $('#DonationStatus');
                                            //if (link) {
                                            url = (document.location.origin + link);
                                            redirectToGateway(url, resp.PledgerId, resp.Name, resp.TotalPledgeAmount);
                                            //redirectToGateway(url, totalPledgedAmount, resp.PledgerId, resp.Name, resp.HashKey, resp.TimeStamp, resp.X_login, resp.Email);
                                            //return false;
                                        }
                                        else {
                                            alert('Faild to save record, please try again.');
                                            document.location.href = document.location.href;
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            alert(resp.Msg);
                            return false;
                        }
                    }
                });
            }
            return false;
        }
        */
        e.preventDefault();
    });
    function capitalize(text) {
        return text.replace(/\b\w/g, function (m) { return m.toUpperCase(); });
    }
    function addCommas(amount) {
        if (amount) {
            amount = amount.toString();
            var result = "";
            amount = amount.split("").reverse().join("");
            for (var i = 0; i < amount.length; i++) {
                result = result + amount[i];
                if (i === 2)
                    result = result + ',';
                if (i === 5)
                    result = result + ',';
                if (i === 8)
                    result = result + ',';
                if (i === 11)
                    result = result + ',';
                if (i === 14)
                    result = result + ',';

                // console.log(result);
            }
            result = result.split("").reverse().join("");
            if (result.charAt(0) === ',')
                result = result.slice(1);

            return result;
        }
    }

    $('.amnt-select').change(function (e) {
        $('#inputOtherAmount').val('');
        document.getElementById('error_Amount').classList.add('text-hide');
    });
    $('#inputOtherAmount').on('keyup', function (e) {
        $('.amnt-select').prop('checked', false);
        if (e) {
            var value = this.value;
            if (value) {
                value = value.replace(",", "");
                value = parseInt(value);
                if (value > 0) {
                    this.value = addCommas(value);
                    return false;
                }
            }
        }
    });
    $('#inputOtherAmount').on('change', function (e) {
        //$('.amnt-select').prop('checked', false);
        $('.amnt-select').removeProp('required');
        if (e) {
            var value = this.value;
            if (value) {
                value = value.replaceAll(",", "");
                value = parseInt(value);
                if (value > 900000) {
                    alert('Amount should be less than 900,000');
                    this.value = 0;
                    return false;
                }
                //extra check 
                else if (value < 1) {
                    alert('Amount must be greater than 0');
                    this.value = 0;
                    return false;
                } else {
                    this.value = addCommas(value);
                    return false;
                }
            }
        }
    });
    function redirectToGateway(url, id, name, amount) {

        var form = $(document.createElement('form'));
        $(form).attr("action", url);
        $(form).attr("method", "POST");


        var input = $("<input>")
            .attr("type", "hidden")
            .attr("name", "totalAmount")
            .val(amount);
        $(form).append($(input));

        var input1 = $("<input>")
            .attr("type", "hidden")
            .attr("name", "pledgerId")
            .val(id);
        $(form).append($(input1));
        var input2 = $("<input>")
            .attr("type", "hidden")
            .attr("name", "name")
            .val(name);
        $(form).append($(input2));

        form.appendTo(document.body);

        $(form).submit();
        //  var url = (document.location.origin + $('#ThankYouLink').val());
        //  document.location.href = url;
    }

    //getDonors('donors-Visonaries-Content', 10000, 999999999999, 1, this.value);
    //getDonors('donors-Benefactors-Content', 5000, 9999, 2, this.value);
    //getDonors('donors-Supporters-Content', 1, 4999, 3, this.value);
    //getDonors('donors-Fellows-Content', 5000, 9999, 4, this.value);

    var mFlag = getParameterByName("sm");
    if (mFlag) {
        $(".landingContent").addClass('hideContentWithanimate');
        $('.formContent').addClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").removeClass('hideContentWithanimate');
        return false;
    }
    var msgFlag = getParameterByName("msg");
    if (msgFlag) {
        $(".landingContent").addClass('hideContentWithanimate');
        $('.formContent').removeClass('hideContentWithanimate');
        $('.mohsineenSupportsContent').addClass('hideContentWithanimate');
        $('.mohsineenBenefactorsContent').addClass('hideContentWithanimate');
        $('.mohsineenVisionariesContent').addClass('hideContentWithanimate');
        $(".mohsineenContent").addClass('hideContentWithanimate');
        return false;
    }
    //document.getElementById(data.id).scrollIntoView(true);
})();

function getDonors(container, min, max, category, name) {

    var data = {};
    if (min === null && max === null && category === null) {
        data = { Name: name };
    } else {
        data = { Min: min, Max: max, category, Name: name };
    }
    //showLoader();
    $.post('/Home/Donors', data, function (resp) {
        console.log(resp);
        if (resp) {
            if (resp.Success) {
                console.log(resp.Data);
                if (resp.Data !== null && resp.Data.length > 0) {
                    $('.' + container).html('');
                    var newContent = '';
                    $.each(resp.Data, function (index, data) {
                        if (data !== null) {
                            var name = data.FirstName; //+ ' ' + data.LastName;
                            var org = data.Organization;
                            $('.' + container).append('<div class="list-design"><div class="div-data"><label class= "list-name">' + name + '</label><label class="list-org">' + org + '</label></div><div class="div-type"><label class="list-type">' + data.Type + '</label></div></div>');
                            newContent += '<div class="list-design type-' + data.Type + '">' +
                                +'<label class= "list-name">' + name + '</label>' +
                                +'<label class="list-org">' + org + '</label>' +
                                +'<label class="list-type">' + data.Type + '</label>' +
                                +' </div>';
                        }
                    });
                    //$('.donorsContent').html(newContent);
                } else {
                    $('.' + container).html('<div class="list-design" ><label class="list-name"></label></div>');
                }
            }
            else {
                alert(resp.Msg);
                return false;
            }
        }
    });
    return false;
}
function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}
function showLoader() {
    try {
        $("#mask").show();
        $("#loader").show();
    } catch (e) {
        console.log(e);
    }
}

function hideLoader() {
    try {
        $("#mask").hide();
        $("#loader").hide();
    } catch (e) {
        console.log(e);
    }
}
//function close(id) {
//    $('#' + id).removeClass('show');
//}
//$('.detailsLink').click(function () {
//    $("#container").load(this.href).addClass('show');//.show();
//    $('#mainPage').removeClass('show');
//    return false;
//});
/*
function getTotalAmount() {
    var totalPledged = 0;
    var selectedFunds = $('input[type="checkbox"]:checked');
    if (selectedFunds) {
        $.each(selectedFunds, function (index, data) {
            if (data) {
                if (data) {
                    if (data.dataset) {
                        if (data.dataset.amount) {
                            var value = parseInt(data.dataset.amount);

                            if (value === 0) {
                                try {
                                    //alert('Please select amount in ' + $(data).parent().text().trim());
                                    _popUpMsg = 'Please select amount in ' + $(data).parent().text().trim();
                                    
                                } catch (ex) { console.debug(ex); }
                                flag = false;
                                return false;
                            }
                            else if (value === -1) {
                                try {
                                    try {
                                        //    alert('Please enter numeric value in ' + $(data).parent().text().trim());
                                        _popUpMsg = 'Please enter numeric value in ' + $(data).parent().text().trim();
                                        document.getElementById(data.id).scrollIntoView(true);
                                    } catch (ex) { console.debug(ex); }
                                    flag = false;
                                    return false;
                                }                                catch (ex) { console.debug(ex); }
                            }
                            //else if (value > 50000000) {
                            //    try {
                            //        alert('Amount to pledge should be equal to or less than USD 50,000,000 in ' + $(data).parent().text().trim());
                            //    } catch (ex) { console.debug(ex); }
                            //    flag = false;
                            //    return false;
                            //}

                            totalPledged += value;
                        }
                    }
                }
            }
        });
    }
    return totalPledged;
}
*/
